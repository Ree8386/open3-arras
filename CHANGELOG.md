POISON_TO_APPLY: 0,
  POISON: true,
  SHOWPOISON: true,

fire: 2
plauge: 7

this.plaugeed = false
        this.plauge = false
        this.plaugeedBy = -1
        this.plaugeLevel = 0
        this.plaugeToApply = 0
        this.showplauge = false
       	this.plaugeTimer = 0
        
        if (set.PLAUGE != null) {
          this.plauge = set.BURN
        }
        if (set.PLAUGEED != null) {
          this.plaugeed = set.BURNED
        }
        if (set.PLAUGE_TO_APPLY != null) {
          this.plaugeToApply = set.BURN_TO_APPLY
        }
        if (set.SHOWPLAUGE != null) {
          this.showplauge = set.SHOWBURN
        }
        
         /*************   PLAUGE  ***********/
                      if (n.plauge) {
                        my.plaugeed = true
                        my.plaugeedLevel = n.poisionToApply
                        my.plaugeTime = 100
                        my.plaugeedBy = n.master
                      }
                      if (my.plauge) {
                        n.plaugeed = true
                        n.plaugeedLevel = my.poisionToApply
                        n.plaugeTime = 100
                        n.plaugeedBy = my.master
                      }
                    }
                    
                    var plaugeLoop = (() => {
    // Fun stuff, like RAINBOWS :D
    function plauge(my) {
      entities.forEach(function(element) {
        if (element.showplauge) {
            let x = element.size + 10
            let y = element.size + 10
            Math.random() < 0.5 ? x *= -0.2 : x
            Math.random() < 0.5 ? y *= -0.2 : y
            Math.random() < 0.5 ? x *= Math.random() + 5 : x
            Math.random() < 0.5 ? y *= Math.random() + 5 : y
            var o = new Entity({
            x: element.x + x,
            y: element.y + y
            })
            o.define(Class['plaugeEffect'])
        }
        if (element.plauged && element.type == 'tank') {
            let x = element.size + 10
            let y = element.size + 10
            Math.random() < 0.5 ? x *= -0.2 : x
            Math.random() < 0.5 ? y *= -0.2 : y
            Math.random() < 0.5 ? x *= Math.random() + 5 : x
            Math.random() < 0.5 ? y *= Math.random() + 5 : y
            var o = new Entity({
            x: element.x + x,
            y: element.y + y
            })
            o.define(Class['plaugeEffect'])
            
            if (!element.invuln) {
              element.health.amount -= element.health.max / (55 - element.plaugeLevel)
              element.shield.amount -= element.shield.max / (35 - element.plaugeLevel)
            }
            
            element.plaugeTime -= 1
            if (element.plaugeTime <= 0) element.plaugeed = false
            
            if (element.health.amount <= 0 && element.plaugeedBy != undefined && element.plaugeedBy.skill != undefined) {
              element.plaugeedBy.skill.score += Math.ceil(util.getJackpot(element.plaugeedBy.skill.score));
              element.plaugeedBy.sendMessage('You infected ' + element.name + ' with the plague.'); 
              element.sendMessage('You have died because ' + element.burnedBy.name + ' is spreding the plague.')
            }
          }
      }
    )}
       return () => {
        // run the plauge
        plauge()
    };
})();
// Bring it to life
setInterval(plaugeLoop, room.cycleSpeed * 1);
setInterval(gameloop, room.cycleSpeed);
setInterval(maintainloop, 200);
setInterval(speedcheckloop, 1000);