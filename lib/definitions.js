// GUN DEFINITIONS
const combineStats = function(arr) {
  try {
    // Build a blank array of the appropiate length
    let data = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
    arr.forEach(function(component) {
      for (let i = 0; i < data.length; i++) {
        data[i] = data[i] * component[i];
      }
    });
    return {
      reload: data[0],
      recoil: data[1],
      shudder: data[2],
      size: data[3],
      health: data[4],
      damage: data[5],
      pen: data[6],
      speed: data[7],
      maxSpeed: data[8],
      range: data[9],
      density: data[10],
      spray: data[11],
      resist: data[12]
    };
  } catch (err) {
    console.log(err);
    console.log(JSON.stringify(arr));
  }
};
const skillSet = (() => {
  let config = require("../config.json");
  let skcnv = {
    rld: 0,
    pen: 1,
    str: 2,
    dam: 3,
    spd: 4,
    shi: 5,
    atk: 6,
    hlt: 7,
    rgn: 8,
    mob: 9
  };
  return args => {
    let skills = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    for (let s in args) {
      if (!args.hasOwnProperty(s)) continue;
      skills[skcnv[s]] = Math.round(config.MAX_SKILL * args[s]);
    }
    return skills;
  };
})();

const g = {
  // Gun info here
  trap: [36, 1, 0.25, 0.6, 1, 0.75, 1, 5, 1, 1, 1, 15, 3],
  swarm: [18, 0.25, 0.05, 0.4, 1, 0.75, 1, 4, 1, 1, 1, 5, 1],
  drone: [50, 0.25, 0.1, 0.6, 1, 1, 1, 2, 1, 1, 1, 0.1, 1],
  factory: [60, 1, 0.1, 0.7, 1, 0.75, 1, 3, 1, 1, 1, 0.1, 1],
  basic: [18, 1.4, 0.1, 1, 1, 0.75, 1, 4.5, 1, 1, 1, 15, 1],
  small: [1, 1, 1, 0.5, 1, 1, 1, 1, 1, 1, 1,1, 1,],
  /***************** RELOAD RECOIL SHUDDER  SIZE   HEALTH  DAMAGE   PEN    SPEED    MAX    RANGE  DENSITY  SPRAY   RESIST  */
  blank: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  spam: [1.1, 1, 1, 1.05, 1, 1.1, 1, 0.9, 0.7, 1, 1, 1, 1.05],
  minion: [1, 1, 2, 1, 0.4, 0.4, 1.2, 1, 1, 0.75, 1, 2, 1],
  single: [1.05, 1, 1, 1, 1, 1, 1, 1.05, 1, 1, 1, 1, 1],
  sniper: [1.35, 1, 0.25, 1, 1, 0.8, 1.1, 1.5, 1.5, 1, 1.5, 0.2, 1.15],
  rifle: [0.8, 0.8, 1.5, 1, 0.8, 0.8, 0.9, 1, 1, 1, 1, 2, 1],
  assass: [1.65, 1, 0.25, 1, 1.15, 1, 1.1, 1.18, 1.18, 1, 3, 1, 1.3],
  hunter: [1.5, 0.7, 1, 0.95, 1, 0.9, 1, 1.1, 0.8, 1, 1.2, 1, 1.15],
  hunter2: [1, 1, 1, 0.9, 2, 0.5, 1.5, 1, 1, 1, 1.2, 1, 1.1],
  preda: [1.4, 1, 1, 0.8, 1.5, 0.9, 1.2, 0.9, 0.9, 1, 1, 1, 1],
  snake: [0.4, 1, 4, 1, 1.5, 0.9, 1.2, 0.2, 0.35, 1, 3, 6, 0.5],
  sidewind: [1.5, 2, 1, 1, 1.5, 0.9, 1, 0.15, 0.5, 1, 1, 1, 1],
  snakeskin: [0.6, 1, 2, 1, 0.5, 0.5, 1, 1, 0.2, 0.4, 1, 5, 1],
  mach: [0.5, 0.8, 1.7, 1, 0.7, 0.7, 1, 1, 0.8, 1, 1, 2.5, 1],
  blaster: [1, 1.2, 1.25, 1.1, 1.5, 1, 0.6, 0.8, 0.33, 0.6, 0.5, 1.5, 0.8],
  chain: [1.25, 1.33, 0.8, 1, 0.8, 1, 1.1, 1.25, 1.25, 1.1, 1.25, 0.5, 1.1],
  mini: [1.25, 0.6, 1, 0.8, 0.55, 0.45, 1.25, 1.33, 1, 1, 1.25, 0.5, 1.1],
  stream: [1.1, 0.6, 1, 1, 1, 0.65, 1, 1.24, 1, 1, 1, 1, 1],
  shotgun: [8, 0.4, 1, 1.5, 1, 0.4, 0.8, 1.8, 0.6, 1, 1.2, 1.2, 1],
  burts: [4, 0.2, 1, 1, 1, 0.4, 0.8, 2, 0.6, 1.5, 1.2, 1.2, 1],
  flank: [1, 1.2, 1, 1, 1.02, 0.81, 0.9, 1, 0.85, 1, 1.2, 1, 1],
  tri: [1, 0.9, 1, 1, 0.9, 1, 1, 0.8, 0.8, 0.6, 1, 1, 1],
  trifront: [1, 0.2, 1, 1, 1, 1, 1, 1.3, 1.1, 1.5, 1, 1, 1],
  thruster: [1, 1.5, 2, 1, 0.5, 0.5, 0.7, 1, 1, 1, 1, 0.5, 0.7],
  auto: /*pure*/ [
    1.8,
    0.75,
    0.5,
    0.8,
    0.9,
    0.6,
    1.2,
    1.1,
    1,
    0.8,
    1.3,
    1,
    1.25
  ],
  five: [1.15, 1, 1, 1, 1, 1, 1, 1.05, 1.05, 1.1, 2, 1, 1],
  autosnipe: [1, 1, 1, 1.4, 2, 1, 1, 1, 1, 1, 1, 1, 1],
  /***************** RELOAD RECOIL SHUDDER  SIZE   HEALTH  DAMAGE   PEN    SPEED    MAX    RANGE  DENSITY  SPRAY   RESIST  */

  pound: [2, 1.6, 1, 1, 1, 2, 1, 0.85, 0.8, 1, 1.5, 1, 1.15],
  destroy: [2.2, 1.8, 0.5, 1, 2, 2, 1.2, 1, 0.5, 1, 2, 1, 3],
  jump: [10, 15, 1, 1, 2, 1, 1.5, 0.65, 1, 1, 2.5, 1, 6],
    anni: [5, 2.4, 1, 1, 2, 1, 1.5, 0.65, 1, 1, 2.5, 1, 6],
  hive: [1.5, 0.8, 1, 0.8, 0.7, 0.3, 1, 1, 0.6, 1, 1, 1, 1],
  arty: [1.2, 0.7, 1, 0.9, 1, 1, 1, 1.15, 1.1, 1, 1.5, 1, 1],
  mortar: [1.2, 1, 1, 1, 1.1, 1, 1, 0.8, 0.8, 1, 1, 1, 1],
  spreadmain: [
    0.78125,
    0.25,
    0.5,
    1,
    0.5,
    1,
    1,
    1.5 / 0.78,
    0.9 / 0.78,
    1,
    1,
    1,
    1
  ],
  spread: [1.5, 1, 0.25, 1, 1, 1, 1, 0.7, 0.7, 1, 1, 0.25, 1],
  skim: [1.33, 0.8, 0.8, 0.9, 1.35, 0.8, 2, 0.3, 0.3, 1, 1, 1, 1.1],
  twin: [1, 0.5, 0.9, 1, 0.9, 0.7, 1, 1, 1, 1, 1, 1.2, 1],
  bent: [1.1, 1, 0.8, 1, 0.9, 1, 0.8, 1, 1, 1, 0.8, 0.5, 1],
  triple: [1.2, 0.667, 0.9, 1, 0.85, 0.85, 0.9, 1, 1, 1, 1.1, 0.9, 0.95],
  quint: [1.5, 0.667, 0.9, 1, 1, 1, 0.9, 1, 1, 1, 1.1, 0.9, 0.95],
  dual: [2, 1, 0.8, 1, 1.5, 1, 1, 1.3, 1.1, 1, 1, 1, 1.25],
  double: [1, 1, 1, 1, 1, 0.9, 1, 1, 1, 1, 1, 1, 1],
  hewn: [1.25, 1.5, 1, 1, 0.9, 0.85, 1, 1, 0.9, 1, 1, 1, 1],
  puregunner: [
    1,
    0.25,
    1.5,
    1.2,
    1.35,
    0.25,
    1.25,
    0.8,
    0.65,
    1,
    1.5,
    1.5,
    1.2
  ],
  machgun: [0.66, 0.8, 2, 1, 1, 0.75, 1, 1.2, 0.8, 1, 1, 2.5, 1],
  gunner: [1.25, 0.25, 1.5, 1.1, 1, 0.35, 1.35, 0.9, 0.8, 1, 1.5, 1.5, 1.2],
  power: [1, 1, 0.6, 1.2, 1, 1, 1.25, 2, 1.7, 1, 2, 0.5, 1.5],
  nail: [0.85, 2.5, 1, 0.8, 1, 0.7, 1, 1, 1, 1, 2, 1, 1],
  fast: [1, 1, 1, 1, 1, 1, 1, 1.2, 1, 1, 1, 1, 1],
  turret: [2, 1, 1, 1, 0.8, 0.6, 0.7, 1, 1, 1, 0.1, 1, 1],
  /***************** RELOAD RECOIL SHUDDER  SIZE   HEALTH  DAMAGE   PEN    SPEED    MAX    RANGE  DENSITY  SPRAY   RESIST  */
  battle: [1, 1, 1, 1, 1.25, 1.15, 1, 1, 0.85, 1, 1, 1, 1.1],
  bees: [1.3, 1, 1, 1.4, 1, 1.5, 0.5, 3, 1.5, 1, 0.25, 1, 1],
  carrier: [1.5, 1, 1, 1, 1, 0.8, 1, 1.3, 1.2, 1.2, 1, 1, 1],
  hexatrap: [1.3, 1, 1.25, 1, 1, 1, 1, 0.8, 1, 0.5, 1, 1, 1],
  block: [1.1, 2, 0.1, 1.5, 2, 1, 1.25, 1.5, 2.5, 1.25, 1, 1, 1.25],
  construct: [1.3, 1, 1, 0.9, 1, 1, 1, 1, 1.1, 1, 1, 1, 1],
  boomerang: [0.8, 1, 1, 1, 0.5, 0.5, 1, 0.75, 0.75, 1.333, 1, 1, 1],
  over: [1.25, 1, 1, 0.85, 0.7, 0.8, 1, 1, 0.9, 1, 2, 1, 1],
  meta: [1.333, 1, 1, 1, 1, 0.667, 1, 1, 1, 1, 1, 1, 1],
  weak: [2, 1, 1, 1, 0.6, 0.6, 0.8, 0.5, 0.7, 0.25, 0.3, 1, 1],
  master: [3, 1, 1, 0.7, 0.4, 0.7, 1, 1, 1, 0.1, 0.5, 1, 1],
  sunchip: [5, 1, 1, 1.4, 0.5, 0.4, 0.6, 1, 1, 1, 0.8, 1, 1],
  babyfactory: [1.5, 1, 1, 1, 1, 1, 1, 1, 1.35, 1, 1, 1, 1],
  lowpower: [1, 1, 2, 1, 0.5, 0.5, 0.7, 1, 1, 1, 1, 0.5, 0.7],
  halfrecoil: [1, 0.5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  morerecoil: [1, 1.15, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  muchmorerecoil: [1, 1.35, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  lotsmorrecoil: [1, 1.8, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  tonsmorrecoil: [1, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  doublereload: [0.5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  morereload: [0.75, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  halfreload: [2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  lessreload: [1.5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  threequartersrof: [1.333, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  morespeed: [1, 1, 1, 1, 1, 1, 1, 1.3, 1.3, 1, 1, 1, 1],
  bitlessspeed: [1, 1, 1, 1, 1, 1, 1, 0.93, 0.93, 1, 1, 1, 1],
  slow: [1, 1, 1, 1, 1, 1, 1, 0.7, 0.7, 1, 1, 1, 1],
  halfspeed: [1, 1, 1, 1, 1, 1, 1, 0.5, 0.5, 1, 1, 1, 1],
  notdense: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0.1, 1, 1],
  fake: [1, 1, 1, 0.00001, 0.0001, 1, 1, 0.00001, 2, 0, 1, 1, 1],
  norecoil: [1, 0, 1, 1, 1, 1, 1, 1.5, 1, 1, 1, 2, 1],
  doublerecoil: [1, 2, 1, 1, 1, 1, 1, 1.5, 1, 1, 1, 2, 1],
  electron: [0.5, 0, 1, 0.75, 1, 1, 1, 1.5, 1, 0.8, 0.9, 360, 1],
  electron2: [0.5, 0, 1, 0.75, 1, 1, 1, 1.5, 1, 0.8, 0.9, 15, 1],
  firebreath: [0.05, 0, 1, 0.75, 0.5, 0.5, 1, 3, 1, 2, 0.5, 1.5, 1],
  weakfirebreath: [0.1, 0, 1, 0.75, 0.25, 0.25, 1, 1.5, 1, 1, 0.25, 3, 1],
  focusedbeam: [0.1, 0.001, 1, 0.5, 0.25, 0.1, 10, 2, 1, 1, 0.25, 1, 1],
  chargerbeam: [0.1, 0.001, 1, 0.5, 0.005, 0.1, 10, 0, 1, 1, 0.25, 1, 1],
  bigbertha: [3, 0.1, 1, 1.5, 2, 1, 10, 2, 1, 1, 1.5, 1, 5],
  lazer: [0.2, 0.001, 1, 0.5, 0.25, 0.05, 10, 1, 1, 1, 0.25, 20, 1],
  smokescreen: [0.025, 0, 1, 0.75, 2.5, 0.25, 2.5, 1, 1, 10, 1, 7.5, 1],
  fireball: [5, 1.25, 1, 1.5, 2.5, 5, 1, 2, 1, 1.5, 5, 1, 10],
  weakfireball: [7.5, 1.25, 1, 1, 1, 2.5, 0.75, 1, 1, 0.75, 2.5, 1.5, 5],
  arenaclose: [0.5, 0, 1, 1.5, 10, 999, 999, 5, 1, 1.5, 100, 1, 999],
  doubledamage: [1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1],
  halfrange: [1, 1, 1, 1, 1, 1, 1, 1, 1, 0.5, 1, 1, 1],
  doublepen: [1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1],
  halfdamage: [1, 1, 1, 1, 1, 0.5, 1, 1, 1, 1, 1, 1, 1],
  doublerange: [1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1],
  tempdrone: [1, 1, 1, 1, 1, 0.25, 1, 1, 1, 10, 1, 1, 1],
  tastetherainbow: [0.1, 1, 1, 1, 1, 0.5, 1, 1, 1, 1, 1, 10, 1],
  charger: [5, 5, 1, 0.5, 0.1, 0, 0, 0.1, 1, 1, 1, 30, 1],
  smokerside: [1, 1, 1, 1, 1, 0.75, 1, 1, 1, 0.5, 1, 1, 1],
  fire: [0.1, 1, 1, 1, 0.5, 0.25, 0.75, 1, 1, 0.1, 1, 1, 1],
  knocker: [1, 1, 1, 1, 1, 1, 0.1, 1, 1, 1, 250, 1, 1],
  KO: [1, 1, 1, 1, 1, 1, 0.1, 1, 1, 1, 10000, 1, 1],
  magnum: [1, 1, 1, 1, 1, 0.75, 0.01, 1, 1, 1, 50, 1, 1],
  sound: [1, 1, 1, 1, 1, 0.5, 0.01, 1, 1, 1, 50, 1, 1],
  doublesize: [1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  nospeed: [1, 1, 1, 1, 1, 1, 1, 0.1, 1, 1, 1, 1, 1],
  freeze: [1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1],
  wind: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1000, 1, 1],
  infihealth: [1, 1, 1, 1, 9999, 1, 1, 1, 1, 1, 1, 1, 1],
  noshudder: [1, 1, 0.0001, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  nospray: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0.0001, 1],
  halfspray: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0.5, 1],
  doublerange: [1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1],
  lancerblade: [0.1, 0, 1, 0.5, 1, 1.95, 1, 0.5, 1, 0.05, 0, 1, 1],
    mace: [0.025, 0, 1, 0.5, 1, 2, 2, 0.1, 1, 0.05, 0, 1, 1],
   swordblade: [0.1, 0, 1, 0.5, 1, 1.95, 1, 1.4, 1, 0.05, 0, 1, 1],
  hammerbash: [0.1, 0, 1, 1, 1, 0.3, 0.1, 1, 1, 0.05, 25, 1, 1],
  /***************** RELOAD RECOIL SHUDDER  SIZE   HEALTH  DAMAGE   PEN    SPEED    MAX    RANGE  DENSITY  SPRAY   RESIST  */
  op: [0.5, 1.3, 1, 1, 4, 4, 4, 3, 2, 1, 5, 2, 1],
  protectorswarm: [5, 0, 1, 1, 100, 1, 1, 1, 1, 0.5, 5, 1, 10],
   GunnerDom: [0.7, 0.0001, 1, 0.6, 1.5, 1.1, 0.5, 0.5, 0.9, 1.4, 1.2, 1, 1.2],
    TrapperDom: [1.2, 0.0001, 0.2, 0.9, 1, 2, 0.5, 0.7, 1.1, 1.3, 2, 1, 2],
   DestroyDom: [6, 0.0001, 1, 0.7, 10, 8, 0.5, 0.3, 0.8, 1.5, 2, 1, 2],
};

const dfltskl = 9;

// NAMES
const statnames = {
  smasher: 1,
  drone: 2,
  necro: 3,
  swarm: 4,
  trap: 5,
  generic: 6
};
const gunCalcNames = {
  default: 0,
  bullet: 1,
  drone: 2,
  swarm: 3,
  fixedReload: 4,
  thruster: 5,
  sustained: 6,
  necro: 7,
  trap: 8
};

// ENTITY DEFINITIONS
exports.genericEntity = {
  NAME: "",
  LABEL: "Unknown Entity",
  TYPE: "unknown",
  DAMAGE_CLASS: 0, // 0: def, 1: food, 2: tanks, 3: obstacles
  DANGER: 0,
  VALUE: 0,
  SHAPE: 0,
  COLOR: 16,
  INDEPENDENT: false,
  CONTROLLERS: ["doNothing"],
  HAS_NO_MASTER: false,
  MOTION_TYPE: "glide", // motor, swarm, chase
  FACING_TYPE: "toTarget", // turnWithSpeed, withMotion, looseWithMotion, toTarget, looseToTarget
  DRAW_HEALTH: false,
  DRAW_SELF: true,
  DAMAGE_EFFECTS: true,
  RATEFFECTS: true,
  MOTION_EFFECTS: true,
  INTANGIBLE: false,
  ACCEPTS_SCORE: true,
  GIVE_KILL_MESSAGE: false,
  CAN_GO_OUTSIDE_ROOM: false,
  HITS_OWN_TYPE: "normal", // hard, repel, never, hardWithBuffer
  DIE_AT_LOW_SPEED: false,
  DIE_AT_RANGE: false,
  CLEAR_ON_MASTER_UPGRADE: false,
  PERSISTS_AFTER_DEATH: false,
  VARIES_IN_SIZE: false,
  HEALTH_WITH_LEVEL: true,
  CAN_BE_ON_LEADERBOARD: true,
  HAS_NO_RECOIL: false,
  AUTO_UPGRADE: "none",
  BUFF_VS_FOOD: false,
  OBSTACLE: false,
  CRAVES_ATTENTION: false,
  NECRO: false,
  UPGRADES_TIER_1: [],
  UPGRADES_TIER_2: [],
  UPGRADES_TIER_3: [],
  UPGRADES_TIER_4: [],
  SKILL: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  LEVEL: 0,
  SKILL_CAP: [
    dfltskl,
    dfltskl,
    dfltskl,
    dfltskl,
    dfltskl,
    dfltskl,
    dfltskl,
    dfltskl,
    dfltskl,
    dfltskl
  ],
  GUNS: [],
  MAX_CHILDREN: 0,
  BODY: {
    ACCELERATION: 1,
    SPEED: 0,
    HEALTH: 1,
    RESIST: 1,
    SHIELD: 0,
    REGEN: 0,
    DAMAGE: 1,
    PENETRATION: 1,

    RANGE: 0,
    FOV: 1,
    DENSITY: 1,
    STEALTH: 1,
    PUSHABILITY: 1,
    HETERO: 2
  },
  FOOD: {
    LEVEL: -1
  }
};

// FOOD
exports.food = {
  TYPE: "food",
  DAMAGE_CLASS: 1,
  CONTROLLERS: ["moveInCircles"],
  HITS_OWN_TYPE: "repel",
  MOTION_TYPE: "drift",
  FACING_TYPE: "turnWithSpeed",
  VARIES_IN_SIZE: true,
  BODY: {
    STEALTH: 30,
    PUSHABILITY: 1
  },
  DAMAGE_EFFECTS: false,
  RATEFFECTS: false,
  HEALTH_WITH_LEVEL: false
};

const basePolygonDamage = 1;
const basePolygonHealth = 2;
exports.hugePentagon = {
  PARENT: [exports.food],
  FOOD: {
    LEVEL: 5
  },
  LABEL: "Alpha Pentagon",
  VALUE: 15000,
  SHAPE: -5,
  SIZE: 58,
  COLOR: 14,
  BODY: {
    DAMAGE: 2 * basePolygonDamage,
    DENSITY: 80,
    HEALTH: 300 * basePolygonHealth,
    RESIST: Math.pow(1.25, 3),
    SHIELD: 40 * basePolygonHealth,
    REGEN: 0.6
  },
  DRAW_HEALTH: true,
  GIVE_KILL_MESSAGE: true
};
exports.bigPentagon = {
  PARENT: [exports.food],
  FOOD: {
    LEVEL: 4
  },
  LABEL: "Beta Pentagon",
  VALUE: 2500,
  SHAPE: 5,
  SIZE: 30,
  COLOR: 14,
  BODY: {
    DAMAGE: 2 * basePolygonDamage,
    DENSITY: 30,
    HEALTH: 50 * basePolygonHealth,
    RESIST: Math.pow(1.25, 2),
    SHIELD: 20 * basePolygonHealth,
    REGEN: 0.2
  },
  DRAW_HEALTH: true,
  GIVE_KILL_MESSAGE: true
};

exports.pentagon = {
  PARENT: [exports.food],
  FOOD: {
    LEVEL: 3
  },
  LABEL: "Pentagon",
  VALUE: 400,
  SHAPE: 5,
  SIZE: 16,
  COLOR: 14,
  BODY: {
    DAMAGE: 1.5 * basePolygonDamage,
    DENSITY: 8,
    HEALTH: 10 * basePolygonHealth,
    RESIST: 1.25,
    PENETRATION: 1.1
  },
  DRAW_HEALTH: true
};
exports.triangle = {
  PARENT: [exports.food],
  FOOD: {
    LEVEL: 2
  },
  LABEL: "Triangle",
  VALUE: 120,
  SHAPE: 3,
  SIZE: 9,
  COLOR: 2,
  BODY: {
    DAMAGE: basePolygonDamage,
    DENSITY: 6,
    HEALTH: 3 * basePolygonHealth,
    RESIST: 1.15,
    PENETRATION: 1.5
  },
  DRAW_HEALTH: true
};
exports.square = {
  PARENT: [exports.food],
  FOOD: {
    LEVEL: 1
  },
  LABEL: "Square",
  VALUE: 30,
  SHAPE: 4,
  SIZE: 10,
  COLOR: 13,
  BODY: {
    DAMAGE: basePolygonDamage,
    DENSITY: 4,
    HEALTH: basePolygonHealth,
    PENETRATION: 2
  },
  DRAW_HEALTH: true,
  INTANGIBLE: false
};
exports.egg = {
  PARENT: [exports.food],
  FOOD: {
    LEVEL: 0
  },
  LABEL: "Egg",
  VALUE: 10,
  SHAPE: 0,
  SIZE: 5,
  COLOR: 6,
  INTANGIBLE: true,
  BODY: {
    DAMAGE: 0,
    DENSITY: 2,
    HEALTH: 0.0011,
    PUSHABILITY: 0
  },
  DRAW_HEALTH: false
};

exports.greenpentagon = {
  PARENT: [exports.food],
  LABEL: "Pentagon",
  VALUE: 30000,
  SHAPE: 5,
  SIZE: 16,
  COLOR: 1,
  BODY: {
    DAMAGE: 3,
    DENSITY: 8,
    HEALTH: 200,
    RESIST: 1.25,
    PENETRATION: 1.1
  },
  DRAW_HEALTH: true
};
exports.greentriangle = {
  PARENT: [exports.food],
  LABEL: "Triangle",
  VALUE: 7000,
  SHAPE: 3,
  SIZE: 9,
  COLOR: 1,
  BODY: {
    DAMAGE: 1,
    DENSITY: 6,
    HEALTH: 60,
    RESIST: 1.15,
    PENETRATION: 1.5
  },
  DRAW_HEALTH: true
};
exports.greensquare = {
  PARENT: [exports.food],
  LABEL: "Square",
  VALUE: 2000,
  SHAPE: 4,
  SIZE: 10,
  COLOR: 1,
  BODY: {
    DAMAGE: 0.5,
    DENSITY: 4,
    HEALTH: 20,
    PENETRATION: 2
  },
  DRAW_HEALTH: true,
  INTANGIBLE: false
};

exports.gem = {
  PARENT: [exports.food],
  LABEL: "Gem",
  VALUE: 2000,
  SHAPE: 6,
  SIZE: 5,
  COLOR: 0,
  BODY: {
    DAMAGE: basePolygonDamage / 4,
    DENSITY: 4,
    HEALTH: 10,
    PENETRATION: 2,
    RESIST: 2,
    PUSHABILITY: 0.25
  },
  DRAW_HEALTH: true,
  INTANGIBLE: false
};
exports.obstacle = {
  TYPE: "wall",
  DAMAGE_CLASS: 1,
  LABEL: "Rock",
  FACING_TYPE: "turnWithSpeed",
  SHAPE: -9,
  BODY: {
    PUSHABILITY: 0,
    HEALTH: 10000,
    SHIELD: 10000,
    REGEN: 1000,
    DAMAGE: 1,
    RESIST: 100,
    STEALTH: 1
  },
  VALUE: 0,
  SIZE: 60,
  COLOR: 16,
  VARIES_IN_SIZE: true,
  GIVE_KILL_MESSAGE: true,
  ACCEPTS_SCORE: false
};
exports.babyObstacle = {
  PARENT: [exports.obstacle],
  SIZE: 25,
  SHAPE: -7,
  LABEL: "Gravel"
};

// WEAPONS
const wepHealthFactor = 0.5;
const wepDamageFactor = 1.5;
exports.bullet = {
  LABEL: "Bullet",
  TYPE: "bullet",
  ACCEPTS_SCORE: false,
  BODY: {
    PENETRATION: 1,
    SPEED: 3.75,
    RANGE: 90,
    DENSITY: 1.25,
    HEALTH: 0.33 * wepHealthFactor,
    DAMAGE: 4 * wepDamageFactor,
    PUSHABILITY: 0.3
  },
  FACING_TYPE: "smoothWithMotion",
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "never",
  // DIE_AT_LOW_SPEED: true,
  DIE_AT_RANGE: true
};
exports.lazerbeam = {
  LABEL: "Lazer",
  TYPE: "bullet",
  ACCEPTS_SCORE: false,
  SHAPE: [[-1.007,0.136],[1.02,0.13],[1.02,-0.09],[-1.007,-0.104]],
  BODY: {
    PENETRATION: 5,
    SPEED: 5,
    RANGE: 120,
    DENSITY: 0.25,
    HEALTH: 1 * wepHealthFactor,
    DAMAGE: 6 * wepDamageFactor,
    PUSHABILITY: 0.3
  },
  FACING_TYPE: "smoothWithMotion",
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "never",
  // DIE_AT_LOW_SPEED: true,
  DIE_AT_RANGE: true
};
exports.acidpuddle = {
  LABEL: "Acid Puddle",
  TYPE: "bullet",
  ACCEPTS_SCORE: false,
  SHAPE: [[0.24,-0.26],[1.04,-0.02],[0.77,0.62],[0.53,0.57],[-0.03,0.83],[-0.427,0.56],[-0.86,0.32],[-0.827,-0.384],[-0.01,-0.84]],
  POISON_TO_APPLY: 0,
  POISON: true,
  SHOWPOISON: true,
  BODY: {
    PENETRATION: 1,
    SPEED: 0,
    RANGE: 270,
    DENSITY: 0,
    HEALTH: 999 * wepHealthFactor,
    DAMAGE: 4 * wepDamageFactor,
    PUSHABILITY: 0
  },
  FACING_TYPE: "smoothWithMotion",
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "never",
  // DIE_AT_LOW_SPEED: true,
  DIE_AT_RANGE: true
};
exports.dart = {
  LABEL: "Dart",
  TYPE: "bullet",
  ACCEPTS_SCORE: false,
  SHAPE: [[-0.75,-0.404],[-0.41,-0.224],[0.993,-0.2],[1.9,-0.004],[0.993,0.2],[-0.407,0.21],[-0.8,0.39],[-1.4,0.39],[-1.56,0.216],[-0.83,-0.004],[-1.56,-0.264],[-1.36,-0.404]],
  BODY: {
    PENETRATION: 1,
    SPEED: 7,
    RANGE: 180,
    DENSITY: 1.25,
    HEALTH: 0.33 * wepHealthFactor,
    DAMAGE: 6 * wepDamageFactor,
    PUSHABILITY: 0.3
  },
  FACING_TYPE: "smoothWithMotion",
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "never",
  // DIE_AT_LOW_SPEED: true,
  DIE_AT_RANGE: true
};
exports.sbomb = {
  LABEL: "Spectre Bomb",
  TYPE: "bullet",
  ACCEPTS_SCORE: false,
  SHAPE: [[-0.75,-0.404],[-0.41,-0.224],[0.993,-0.2],[1.9,-0.004],[0.993,0.2],[-0.407,0.21],[-0.8,0.39],[-1.4,0.39],[-1.56,0.216],[-0.83,-0.004],[-1.56,-0.264],[-1.36,-0.404]],
  BODY: {
    PENETRATION: 1,
    SPEED: 7,
    RANGE: 90,
    DENSITY: 1.25,
    HEALTH: 0.1 * wepHealthFactor,
    DAMAGE: 6 * wepDamageFactor,
    PUSHABILITY: 0.3
  },
  FRAG: 'spectrefrag',
  FACING_TYPE: "smoothWithMotion",
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "never",
  // DIE_AT_LOW_SPEED: true,
  DIE_AT_RANGE: true
};
exports.pointer = {
  LABEL: "Lazer Pointer",
  TYPE: "bullet",
  ACCEPTS_SCORE: false,
  BODY: {
    PENETRATION: 100,
    SPEED: 10,
    RANGE: 11,
    DENSITY: 0.0001,
    HEALTH: 999 * wepHealthFactor,
    DAMAGE: 0.001 * wepDamageFactor,
    PUSHABILITY: 0.3
  },
  FACING_TYPE: "smoothWithMotion",
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "never",
  // DIE_AT_LOW_SPEED: true,
  DIE_AT_RANGE: true
};
exports.healcrate = {
  LABEL: "Supply Crate",
  TYPE: "bullet",
  ACCEPTS_SCORE: false,
  FRAG: 'Healaura',
  SHAPE: 4,
  SIZE: 2,
  BODY: {
    PENETRATION: 1,
    SPEED: 3.75,
    RANGE: 90,
    DENSITY: 1.25,
    HEALTH: 0.33 * wepHealthFactor,
    DAMAGE: 4 * wepDamageFactor,
    PUSHABILITY: 0.3
  },
  FACING_TYPE: "smoothWithMotion",
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "never",
  // DIE_AT_LOW_SPEED: true,
  DIE_AT_RANGE: true
};
exports.poisonEffect = {
    LABEL: 'Poison',
    TYPE: 'bullet',
    ACCEPTS_SCORE: false,
  COLOR: 11,
  SIZE: 5,
    BODY: {
        PENETRATION: 1,
        SPEED: 3.75,
        RANGE: 10,
        DENSITY: 1.25,
        HEALTH: 1,
        DAMAGE: 0,
        PUSHABILITY: 0.3,
    },
    FACING_TYPE: 'smoothWithMotion',
    CAN_GO_OUTSIDE_ROOM: true,
    HITS_OWN_TYPE: 'never',
    // DIE_AT_LOW_SPEED: true,
    DIE_AT_RANGE: true,
};
exports.plaugeEffect = {
    LABEL: 'Plauge',
    TYPE: 'bullet',
    ACCEPTS_SCORE: false,
  COLOR: 7,
  PLAUGE_TO_APPLY: 0,
  PLAUGE: true,
  SHOWPLAUGE: false,
  SIZE: 5,
    BODY: {
        PENETRATION: 1,
        SPEED: 3.75,
        RANGE: 30,
        DENSITY: 1.25,
        HEALTH: 1,
        DAMAGE: 0,
        PUSHABILITY: 0.3,
    },
    FACING_TYPE: 'smoothWithMotion',
    CAN_GO_OUTSIDE_ROOM: true,
    HITS_OWN_TYPE: 'never',
    // DIE_AT_LOW_SPEED: true,
    DIE_AT_RANGE: true,
};
exports.burnEffect = {
    LABEL: 'Fire',
    TYPE: 'bullet',
    ACCEPTS_SCORE: false,
  COLOR: 2,
  SIZE: 5,
    BODY: {
        PENETRATION: 1,
        SPEED: 3.75,
        RANGE: 20,
        DENSITY: 1.25,
        HEALTH: 1,
        DAMAGE: 0,
        PUSHABILITY: 0.3,
    },
    FACING_TYPE: 'smoothWithMotion',
    CAN_GO_OUTSIDE_ROOM: true,
    HITS_OWN_TYPE: 'never',
    // DIE_AT_LOW_SPEED: true,
    DIE_AT_RANGE: true,
};
exports.freezeEffect = {
    LABEL: 'Freeze',
    TYPE: 'bullet',
    ACCEPTS_SCORE: false,
  COLOR: 10,
  SIZE: 5,
    BODY: {
        PENETRATION: 1,
        SPEED: 3.75,
        RANGE: 10,
        DENSITY: 1.25,
        HEALTH: 1,
        DAMAGE: 0,
        PUSHABILITY: 0.3,
    },
    FACING_TYPE: 'smoothWithMotion',
    CAN_GO_OUTSIDE_ROOM: true,
    HITS_OWN_TYPE: 'never',
    // DIE_AT_LOW_SPEED: true,
    DIE_AT_RANGE: true,
};
exports.snakespit = {
  LABEL: "Poison",
  TYPE: "bullet",
  ACCEPTS_SCORE: false,
  POISON_TO_APPLY: 0,
  POISON: true,
  SHOWPOISON: true,
  BODY: {
    PENETRATION: 1,
    SPEED: 3.75,
    RANGE: 90,
    DENSITY: 1.25,
    HEALTH: 0.33 * wepHealthFactor,
    DAMAGE: 4 * wepDamageFactor,
    PUSHABILITY: 0.3
  },
  FACING_TYPE: "smoothWithMotion",
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "never",
  // DIE_AT_LOW_SPEED: true,
  DIE_AT_RANGE: true
};
exports.plaugebullet = {
  LABEL: "Plauge",
  TYPE: "bullet",
  ACCEPTS_SCORE: false,
  PLAUGE_TO_APPLY: 0,
  PLAUGE: true,
  SHOWPLAUGE: true,
  BODY: {
    PENETRATION: 1,
    SPEED: 3.75,
    RANGE: 90,
    DENSITY: 1.25,
    HEALTH: 0.33 * wepHealthFactor,
    DAMAGE: 4 * wepDamageFactor,
    PUSHABILITY: 0.3
  },
  FACING_TYPE: "smoothWithMotion",
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "never",
  // DIE_AT_LOW_SPEED: true,
  DIE_AT_RANGE: true
};
exports.snowball = {
  LABEL: "Snowball",
  TYPE: "bullet",
  ACCEPTS_SCORE: false,
  FREEZE_TO_APPLY: 0.6,
  FREEZE: true,
  SHOWFREEZE: true,
  BODY: {
    PENETRATION: 1,
    SPEED: 3.75,
    RANGE: 90,
    DENSITY: 1.25,
    HEALTH: 0.33 * wepHealthFactor,
    DAMAGE: 4 * wepDamageFactor,
    PUSHABILITY: 0.3
  },
  FACING_TYPE: "smoothWithMotion",
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "never",
  // DIE_AT_LOW_SPEED: true,
  DIE_AT_RANGE: true
};
exports.snowfireball = {
  LABEL: "Freezerburn Bullet",
  TYPE: "bullet",
  ACCEPTS_SCORE: false,
  FREEZE_TO_APPLY: 0.4,
  FREEZE: true,
  SHOWFREEZE: true,
  BURN_TO_APPLY: 0,
  BURN: true,
  SHOWBURN: true,
  BODY: {
    PENETRATION: 1,
    SPEED: 3.75,
    RANGE: 90,
    DENSITY: 1.25,
    HEALTH: 0.33 * wepHealthFactor,
    DAMAGE: 4 * wepDamageFactor,
    PUSHABILITY: 0.3
  },
  FACING_TYPE: "smoothWithMotion",
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "never",
  // DIE_AT_LOW_SPEED: true,
  DIE_AT_RANGE: true
};
exports.frostbiteball = {
  LABEL: "Frostbite Bullet",
  TYPE: "bullet",
  ACCEPTS_SCORE: false,
  FREEZE_TO_APPLY: 0.4,
  FREEZE: true,
  SHOWFREEZE: true,
  POISON_TO_APPLY: 0,
  POISON: true,
  SHOWPOISON: true,
  BODY: {
    PENETRATION: 1,
    SPEED: 3.75,
    RANGE: 90,
    DENSITY: 1.25,
    HEALTH: 0.33 * wepHealthFactor,
    DAMAGE: 4 * wepDamageFactor,
    PUSHABILITY: 0.3
  },
  FACING_TYPE: "smoothWithMotion",
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "never",
  // DIE_AT_LOW_SPEED: true,
  DIE_AT_RANGE: true
};
exports.fire = {
  LABEL: "Burning Bullet",
  TYPE: "bullet",
  ACCEPTS_SCORE: false,
  BURN_TO_APPLY: 0,
  BURN: true,
  SHOWBURN: true,
  BODY: {
    PENETRATION: 1,
    SPEED: 3.75,
    RANGE: 90,
    DENSITY: 1.25,
    HEALTH: 0.33 * wepHealthFactor,
    DAMAGE: 4 * wepDamageFactor,
    PUSHABILITY: 0.3
  },
  FACING_TYPE: "smoothWithMotion",
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "never",
  // DIE_AT_LOW_SPEED: true,
  DIE_AT_RANGE: true
};
exports.hexbul = {
  LABEL: "Hex Bullet",
  TYPE: "bullet",
  ACCEPTS_SCORE: false,
  BURN_TO_APPLY: 0,
  BURN: true,
  SHOWBURN: true,
  FREEZE_TO_APPLY: 0.8,
  FREEZE: true,
  SHOWFREEZE: true,
  PLAUGE_TO_APPLY: 0,
  PLAUGE: true,
  SHOWPLAUGE: true,
  POISON_TO_APPLY: 0,
  POISON: true,
  SHOWPOISON: true,
  BODY: {
    PENETRATION: 1,
    SPEED: 3.75,
    RANGE: 90,
    DENSITY: 1.25,
    HEALTH: 0.33 * wepHealthFactor,
    DAMAGE: 4 * wepDamageFactor,
    PUSHABILITY: 0.3
  },
  FACING_TYPE: "smoothWithMotion",
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "never",
  // DIE_AT_LOW_SPEED: true,
  DIE_AT_RANGE: true
};
exports.healbullet = {
  LABEL: "Healing Bullet",
  TYPE: "bullet",
  ACCEPTS_SCORE: false,
  BODY: {
    PENETRATION: 1,
    SPEED: 3.75,
    RANGE: 90,
    DENSITY: 1.25,
    HEALTH: 0.33 * wepHealthFactor,
    DAMAGE: -4 * wepDamageFactor,
    PUSHABILITY: 0.3
  },
  FACING_TYPE: "smoothWithMotion",
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "never",
  // DIE_AT_LOW_SPEED: true,
  DIE_AT_RANGE: true
};
exports.soundwave = {
  LABEL: "Sound Wave",
  TYPE: "bullet",
  SHAPE: [[0.01,-1.14],[0.473,-0.63],[0.66,-0.204],[0.65,0.36],[0.47,0.8],[0.18,1.156],[-0.28,1.51],[-0.287,1.31],[-0.207,1.136],[-0.07,0.95],[0.073,0.59],[0.113,0.276],[0.09,-0.09],[0.03,-0.41],[-0.107,-0.704],[-0.3,-1.004],[-0.38,-1.24],[-0.25,-1.304]],
  ACCEPTS_SCORE: false,
  BODY: {
    PENETRATION: 0.1,
    SPEED: 5,
    RANGE: 90,
    DENSITY: 1,
    HEALTH: 9999 * wepHealthFactor,
    DAMAGE: 2.4 * wepDamageFactor,
    PUSHABILITY: 0.1
  },
  FACING_TYPE: "smoothWithMotion",
  MOTION_TYPE: 'grow',
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "never",
  // DIE_AT_LOW_SPEED: true,
  DIE_AT_RANGE: true
};
exports.soundwave2 = {
  LABEL: "Sound Wave",
  TYPE: "bullet",
  SHAPE: [[0.01,-1.14],[0.473,-0.63],[0.66,-0.204],[0.65,0.36],[0.47,0.8],[0.18,1.156],[-0.28,1.51],[-0.287,1.31],[-0.207,1.136],[-0.07,0.95],[0.073,0.59],[0.113,0.276],[0.09,-0.09],[0.03,-0.41],[-0.107,-0.704],[-0.3,-1.004],[-0.38,-1.24],[-0.25,-1.304]],
  ACCEPTS_SCORE: false,
  BODY: {
    PENETRATION: 0.1,
    SPEED: 5,
    RANGE: 90,
    DENSITY: 1,
    HEALTH: 9999 * wepHealthFactor,
    DAMAGE: 2.4 * wepDamageFactor,
    PUSHABILITY: 0.1
  },
  FACING_TYPE: "smoothWithMotion",
  MOTION_TYPE: 'swell',
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "never",
  // DIE_AT_LOW_SPEED: true,
  DIE_AT_RANGE: true
};
exports.soundwave3 = {
  LABEL: "Sound Wave",
  TYPE: "bullet",
  SHAPE: [[0.01,-1.14],[0.473,-0.63],[0.66,-0.204],[0.65,0.36],[0.47,0.8],[0.18,1.156],[-0.28,1.51],[-0.287,1.31],[-0.207,1.136],[-0.07,0.95],[0.073,0.59],[0.113,0.276],[0.09,-0.09],[0.03,-0.41],[-0.107,-0.704],[-0.3,-1.004],[-0.38,-1.24],[-0.25,-1.304]],
  ACCEPTS_SCORE: false,
  BODY: {
    PENETRATION: 0.1,
    SPEED: 5,
    RANGE: 90,
    DENSITY: 1,
    HEALTH: 9999 * wepHealthFactor,
    DAMAGE: 2.4 * wepDamageFactor,
    PUSHABILITY: 0.1
  },
  FACING_TYPE: "smoothWithMotion",
  MOTION_TYPE: 'expand',
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "never",
  // DIE_AT_LOW_SPEED: true,
  DIE_AT_RANGE: true
};
exports.lazer = {
  LABEL: "Lazer",
  SHAPE: [[-2,-0.01],[0,-0.184],[2,0.01],[0.01,0.216]],
  TYPE: "bullet",
  ACCEPTS_SCORE: false,
  BODY: {
    PENETRATION: 1,
    SPEED: 3.75,
    RANGE: 90,
    DENSITY: 1.25,
    HEALTH: 0.33 * wepHealthFactor,
    DAMAGE: 0.2 * wepDamageFactor,
    PUSHABILITY: 0.3
  },
  FACING_TYPE: "smoothWithMotion",
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "never",
  // DIE_AT_LOW_SPEED: true,
  DIE_AT_RANGE: true
};
exports.firebal = {
  LABEL: "Fireball",
  TYPE: "bullet",
  ACCEPTS_SCORE: false,
  BODY: {
    PENETRATION: 1,
    SPEED: 3.75,
    RANGE: 90,
    DENSITY: 1.25,
    HEALTH: 0.33 * wepHealthFactor,
    DAMAGE: 4 * wepDamageFactor,
    PUSHABILITY: 0.3
  },
  FACING_TYPE: "smoothWithMotion",
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "never",
  // DIE_AT_LOW_SPEED: true,
  DIE_AT_RANGE: true
};
exports.casing = {
  PARENT: [exports.bullet],
  LABEL: "Shell",
  TYPE: "swarm"
};
exports.flare1 = {
  LABEL: "Flare",
  ACCEPTS_SCORE: false,
  SHAPE: 4,
  BODY: {
    PENETRATION: 1,
    SPEED: 0,
    RANGE: 90,
    SIZE: 1,
    DENSITY: 1.25,
    HEALTH: 1 * wepHealthFactor,
    DAMAGE: 1.5 * wepDamageFactor,
    PUSHABILITY: 0
  },
  MOTION_TYPE: "grow",
  FACING_TYPE: "smoothWithMotion",
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "never",
  DIE_AT_RANGE: true
};
exports.flare1p = {
  LABEL: "Vented Poison Gas",
  ACCEPTS_SCORE: false,
  SHAPE: 4,
  BODY: {
    PENETRATION: 1,
    SPEED: 0,
    RANGE: 90,
    SIZE: 1,
    DENSITY: 1.25,
    HEALTH: 1 * wepHealthFactor,
    DAMAGE: 0.5 * wepDamageFactor,
    PUSHABILITY: 0
  },
  MOTION_TYPE: "grow",
  FACING_TYPE: "smoothWithMotion",
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "never",
  DIE_AT_RANGE: true
};
exports.bomb = {
  LABEL: "Bomb",
  ACCEPTS_SCORE: false,
  SHAPE: 0,
  BODY: {
    PENETRATION: 1,
    SPEED: 0,
    RANGE: 45,
    SIZE: 0.1,
    DENSITY: 1.25,
    HEALTH: 999 * wepHealthFactor,
    DAMAGE: 5 * wepDamageFactor,
    PUSHABILITY: 0
  },
  MOTION_TYPE: "bomb",
  FACING_TYPE: "smoothWithMotion",
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "never",
  DIE_AT_RANGE: true
};
exports.tsar = {
  LABEL: "Tsar Bomba",
  ACCEPTS_SCORE: false,
  FRAG: 'Tsar Bomba',
  SHAPE: [[-1.393,-0.98],[-0.6,-0.41],[0.49,-0.58],[1.31,-0.39],[1.4,-0.004],[1.32,0.39],[0.45,0.64],[-0.627,0.396],[-1.407,1.016],[-1.607,0.2],[-0.79,0.01],[-1.62,-0.184]],
  BODY: {
    PENETRATION: 1,
    SPEED: 0,
    RANGE: 45,
    SIZE: 0.1,
    DENSITY: 1.25,
    HEALTH: 999 * wepHealthFactor,
    DAMAGE: 5 * wepDamageFactor,
    PUSHABILITY: 0
  },
  MOTION_TYPE: "motor",
  FACING_TYPE: "smoothWithMotion",
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "never",
  DIE_AT_RANGE: true
};
exports.acebomb = {
  LABEL: "Bomber Ace Bomb",
  ACCEPTS_SCORE: false,
  FRAG: 'bomberacefrag',
  SHAPE: [[-1.393,-0.98],[-0.6,-0.41],[0.49,-0.58],[1.31,-0.39],[1.4,-0.004],[1.32,0.39],[0.45,0.64],[-0.627,0.396],[-1.407,1.016],[-1.607,0.2],[-0.79,0.01],[-1.62,-0.184]],
  BODY: {
    PENETRATION: 1,
    SPEED: 0,
    RANGE: 45,
    SIZE: 0.1,
    DENSITY: 1.25,
    HEALTH: 999 * wepHealthFactor,
    DAMAGE: 5 * wepDamageFactor,
    PUSHABILITY: 0
  },
  MOTION_TYPE: "motor",
  FACING_TYPE: "smoothWithMotion",
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "never",
  DIE_AT_RANGE: true
};
exports.ringoffire = {
  LABEL: "Ring of Fire",
  ACCEPTS_SCORE: false,
  SHAPE: 0,
  BODY: {
    PENETRATION: 1,
    SPEED: 0,
    RANGE: 70,
    SIZE: 0.1,
    DENSITY: 1.25,
    HEALTH: 999 * wepHealthFactor,
    DAMAGE: 5 * wepDamageFactor,
    PUSHABILITY: 0
  },
  MOTION_TYPE: "bomb",
  FACING_TYPE: "smoothWithMotion",
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "never",
  DIE_AT_RANGE: true
};
exports.infernado = {
  LABEL: "Infernado",
  TYPE: "bullet",
  ACCEPTS_SCORE: false,
  SHAPE: [[0,1],[0.19,0.98],[0.413,0.91],[0.6,0.796],[0.77,0.63],[0.93,0.416],[0.98,0.2],[1,0.01],[0.96,-0.27],[0.81,-0.524],[0.573,-0.68],
          [0.3,-0.77],[-0.007,-0.77],[-0.28,-0.66],[-0.49,-0.464],[-0.607,-0.19],[-0.54,0.116],[-0.38,0.34],[-0.167,0.48],[0.02,0.53],[0.23,0.416],
          [0.35,0.196],[0.29,0],[0.113,-0.14],[-0.01,-0.04],[0.1,0.016],[0.06,0.15],[-0.14,0.19],[-0.23,-0.02],[-0.18,-0.25],[0.113,-0.41],
          [0.44,-0.36],[0.65,-0.104],[0.65,0.24],[0.42,0.576],[0.133,0.75],[-0.17,0.76],[-0.47,0.616],[-0.75,0.296],[-0.82,-0.11],[-0.75,-0.504],
          [-0.51,-0.804],[-0.16,-0.984],[0.18,-1],[0.573,-0.84],[0.84,-0.58],[0.97,-0.24],[0.85,-0.584],[0.57,-0.824],[0.18,-1],[-0.153,-0.99],
          [-0.52,-0.87],[-0.82,-0.6],[-0.98,-0.23],[-1.013,0.16],[-0.85,0.55],[-0.593,0.81],[-0.33,0.976]],
  BODY: {
    PENETRATION: 1,
    SPEED: 0,
    RANGE: 250,
    DENSITY: 1.25,
    HEALTH: 50 * wepHealthFactor,
    DAMAGE: 2 * wepDamageFactor,
    PUSHABILITY: 1
  },
  GUNS: [
    {
      /***    LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [14,    6,      1,      0,     -2,     90,      0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.slow, g.halfreload]),
        TYPE: [exports.flare1, { PERSISTS_AFTER_DEATH: true }]
      }
    },
    {
      POSITION: [14, 6, 1, 0, -2, 270, 0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.slow, g.halfreload]),
        TYPE: [exports.flare1, { PERSISTS_AFTER_DEATH: true }]
      }
    }, {
      POSITION: [14, 6, 1, 0, -2, 0, 0.5],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.slow, g.halfreload]),
        TYPE: [exports.flare1, { PERSISTS_AFTER_DEATH: true }]
      }
    }, {
      POSITION: [14, 6, 1, 0, -2, 180, 0.5],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.slow, g.halfreload]),
        TYPE: [exports.flare1, { PERSISTS_AFTER_DEATH: true }]
      }
    }
  ],
  FACING_TYPE: "hyperspin",
  MOTION_TYPE: "grow",
  CAN_GO_OUTSIDE_ROOM: false,
  HITS_OWN_TYPE: "never",
  DRAW_HEALTH: true,
  DIE_AT_RANGE: true
};
exports.flare4 = {
  LABEL: "Infernis Flare",
  ACCEPTS_SCORE: false,
  SHAPE: 12,
  BODY: {
    PENETRATION: 1,
    SPEED: 1,
    RANGE: 200,
    SIZE: 1,
    DENSITY: 1.25,
    HEALTH: 999 * wepHealthFactor,
    DAMAGE: 5 * wepDamageFactor,
    PUSHABILITY: 0
  },
  MOTION_TYPE: "swell",
  FACING_TYPE: "autospin",
  CAN_GO_OUTSIDE_ROOM: false,
  HITS_OWN_TYPE: "never",
  DIE_AT_RANGE: true
};
exports.flare3 = {
  LABEL: "Smokescreen",
  ACCEPTS_SCORE: false,
  SHAPE: 4,
  BODY: {
    PENETRATION: 1,
    SPEED: 0,
    RANGE: 90,
    SIZE: 1,
    DENSITY: 1.25,
    HEALTH: 1 * wepHealthFactor,
    DAMAGE: 0 * wepDamageFactor,
    PUSHABILITY: 0
  },
  MOTION_TYPE: "grow",
  FACING_TYPE: "smoothWithMotion",
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "hard",
  DIE_AT_RANGE: true
};
exports.flare2 = {
  LABEL: "Bunsen",
  ACCEPTS_SCORE: false,
  SHAPE: -4,
  BODY: {
    PENETRATION: 1,
    SPEED: 0,
    RANGE: 10,
    SIZE: 1,
    DENSITY: 1.25,
    HEALTH: 1 * wepHealthFactor,
    DAMAGE: 1 * wepDamageFactor,
    PUSHABILITY: 0
  },
  MOTION_TYPE: "grow",
  FACING_TYPE: "smoothWithMotion",
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "never",
  DIE_AT_RANGE: true
};
exports.fshield = {
  LABEL: "Flare Shield",
  ACCEPTS_SCORE: false,
  SHAPE: 4,
  BODY: {
    PENETRATION: 1,
    SPEED: 0,
    RANGE: 500,
    SIZE: 1.5,
    DENSITY: 1.25,
    HEALTH: 100 * wepHealthFactor,
    DAMAGE: 2 * wepDamageFactor,
    PUSHABILITY: 0
  },
  MOTION_TYPE: "motor",
  FACING_TYPE: "smoothWithMotion",
  CONTROLLERS: [
    "hangOutNearMaster"
  ],
  AI: { BLIND: true },
  FACING_TYPE: "smoothWithMotion",
  CAN_GO_OUTSIDE_ROOM: true,
  HITS_OWN_TYPE: "never",
  DIE_AT_RANGE: true
};
exports.swarm = {
  LABEL: "Swarm Drone",
  TYPE: "swarm",
  ACCEPTS_SCORE: false,
  SHAPE: 3,
  MOTION_TYPE: "swarm",
  FACING_TYPE: "smoothWithMotion",
  CONTROLLERS: ["nearestDifferentMaster", "mapTargetToGoal"],
  CRAVES_ATTENTION: true,
  BODY: {
    ACCELERATION: 3,
    PENETRATION: 1.5,
    HEALTH: 0.35 * wepHealthFactor,
    DAMAGE: 1.5 * wepDamageFactor,
    SPEED: 4.5,
    RESIST: 1.6,
    RANGE: 225,
    DENSITY: 12,
    PUSHABILITY: 0.5,
    FOV: 1.5
  },
  DIE_AT_RANGE: true,
  BUFF_VS_FOOD: true
};
exports.exswarm = {
  LABEL: "Exploding Swarm Drone",
  TYPE: "swarm",
  ACCEPTS_SCORE: false,
  SHAPE: [[-0.75,-0.404],[-0.41,-0.224],[0.993,-0.2],[1.9,-0.004],[0.993,0.2],[-0.407,0.21],[-0.8,0.39],[-1.4,0.39],[-1.56,0.216],[-0.83,-0.004],[-1.56,-0.264],[-1.36,-0.404]],
  FRAG: 'firebal2frag',
  MOTION_TYPE: "swarm",
  FACING_TYPE: "smoothWithMotion",
  CONTROLLERS: ["nearestDifferentMaster", "mapTargetToGoal"],
  CRAVES_ATTENTION: true,
  BODY: {
    ACCELERATION: 3,
    PENETRATION: 1.5,
    HEALTH: 0.35 * wepHealthFactor,
    DAMAGE: 1.5 * wepDamageFactor,
    SPEED: 4.5,
    RESIST: 1.6,
    RANGE: 225,
    DENSITY: 12,
    PUSHABILITY: 0.5,
    FOV: 1.5
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [12, 5, 1.4, 8, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.muchmorerecoil, g.lowpower, g.halfdamage, g.halfdamage, g.halfdamage, g.halfdamage, g.halfdamage, g.halfdamage]),
        TYPE: exports.bullet,
        AUTOFIRE: true
      }
    }
  ],
  DIE_AT_RANGE: true,
  BUFF_VS_FOOD: true
};
exports.explosion = {
    PARENT: [exports.bullet, ],
    MOTION_TYPE: 'bomb',
    LABEL: 'Explosion',
    ACCEPTS_SCORE: false,
    BODY: {
        PENETRATION: 1.25,
        SPEED: 4.5,
        RANGE: 125,
        DENSITY: 1.15,
        SHIELD: 125,
        HEALTH: 5 * wepHealthFactor,
        DAMAGE: 5 * wepDamageFactor,
        PUSHABILITY: 0.1,
    },
    FACING_TYPE: 'smoothWithMotion',
    CAN_GO_OUTSIDE_ROOM: true,
    HITS_OWN_TYPE: 'never',
    PERSISTS_AFTER_DEATH: true,
};
exports.groundz = {
    PARENT: [exports.bullet, ],
    MOTION_TYPE: 'bomb',
    LABEL: 'Explosion',
    ACCEPTS_SCORE: false,
    BODY: {
        PENETRATION: 1.25,
        SPEED: 4.5,
        RANGE: 125,
        DENSITY: 1.15,
        SHIELD: 125,
        HEALTH: 9999999 * wepHealthFactor,
        DAMAGE: 2.5 * wepDamageFactor,
        PUSHABILITY: 0.1,
    },
    FACING_TYPE: 'smoothWithMotion',
    CAN_GO_OUTSIDE_ROOM: true,
    HITS_OWN_TYPE: 'never',
    PERSISTS_AFTER_DEATH: true,
};
exports.tsarb = {
    PARENT: [exports.bullet, ],
    MOTION_TYPE: 'bomb',
    LABEL: 'Explosion',
    ACCEPTS_SCORE: false,
    BODY: {
        PENETRATION: 0.25,
        SPEED: 4.5,
        RANGE: 125,
        DENSITY: 15,
        SHIELD: 125,
        HEALTH: 9999999 * wepHealthFactor,
        DAMAGE: 5 * wepDamageFactor,
        PUSHABILITY: 0.1,
    },
    FACING_TYPE: 'smoothWithMotion',
    CAN_GO_OUTSIDE_ROOM: true,
    HITS_OWN_TYPE: 'never',
    PERSISTS_AFTER_DEATH: true,
};
exports.downdraft = {
    PARENT: [exports.bullet, ],
    MOTION_TYPE: 'expand',
    LABEL: 'Downdraft',
    ACCEPTS_SCORE: false,
    BODY: {
        PENETRATION: 0.1,
        SPEED: 1,
        RANGE: 40,
        DENSITY: 999,
        SHIELD: 0.0001,
        HEALTH: 999* wepHealthFactor,
        DAMAGE: 4 * wepDamageFactor,
        PUSHABILITY: 0.001,
    },
    FACING_TYPE: 'smoothWithMotion',
    CAN_GO_OUTSIDE_ROOM: true,
    HITS_OWN_TYPE: 'never',
    PERSISTS_AFTER_DEATH: true,
};
exports.explosion2 = {
    PARENT: [exports.bullet, ],
    MOTION_TYPE: 'expand',
    LABEL: 'Explosion',
    ACCEPTS_SCORE: false,
    BODY: {
        PENETRATION: 1.25,
        SPEED: 4.5,
        RANGE: 125,
        DENSITY: 1.15,
        SHIELD: 125,
        HEALTH: 5 * wepHealthFactor,
        DAMAGE: 5 * wepDamageFactor,
        PUSHABILITY: 0.1,
    },
    FACING_TYPE: 'smoothWithMotion',
    CAN_GO_OUTSIDE_ROOM: true,
    HITS_OWN_TYPE: 'never',
    PERSISTS_AFTER_DEATH: true,
};
exports.explosion3 = {
    PARENT: [exports.bullet, ],
    MOTION_TYPE: 'swell',
    LABEL: 'Explosion',
    ACCEPTS_SCORE: false,
    BODY: {
        PENETRATION: 1.25,
        SPEED: 4.5,
        RANGE: 125,
        DENSITY: 1.15,
        SHIELD: 125,
        HEALTH: 999 * wepHealthFactor,
        DAMAGE: 2.5 * wepDamageFactor,
        PUSHABILITY: 0.1,
    },
    FACING_TYPE: 'smoothWithMotion',
    CAN_GO_OUTSIDE_ROOM: true,
    HITS_OWN_TYPE: 'never',
    PERSISTS_AFTER_DEATH: true,
};exports.explosion3p = {
    PARENT: [exports.bullet, ],
    MOTION_TYPE: 'swell',
    LABEL: 'Poisonbomb',
    ACCEPTS_SCORE: false,
    POISON_TO_APPLY: 0,
    POISON: true,
    HEALTH: 9999999999999,
    SHOWPOISON: true,
    BODY: {
        PENETRATION: 1.25,
        SPEED: 4.5,
        RANGE: 125,
        DENSITY: 1.15,
        SHIELD: 125,
        HEALTH: 9999999 * wepHealthFactor,
        DAMAGE: 2.5 * wepDamageFactor,
        PUSHABILITY: 0.1,
    },
    FACING_TYPE: 'smoothWithMotion',
    CAN_GO_OUTSIDE_ROOM: true,
    HITS_OWN_TYPE: 'never',
    PERSISTS_AFTER_DEATH: true,
};
exports.explosion4 = {
    PARENT: [exports.bullet, ],
    MOTION_TYPE: 'micgrow',
    LABEL: 'Inferno Ball',
    ACCEPTS_SCORE: false,
    BODY: {
        PENETRATION: 1.25,
        SPEED: 4.5,
        RANGE: 125,
        DENSITY: 1.15,
        SHIELD: 125,
        HEALTH: 999 * wepHealthFactor,
        DAMAGE: 10 * wepDamageFactor,
        PUSHABILITY: 1,
    },
    FACING_TYPE: 'smoothWithMotion',
    CAN_GO_OUTSIDE_ROOM: true,
    HITS_OWN_TYPE: 'never',
    PERSISTS_AFTER_DEATH: true,
};
exports.explosion4p = {
    PARENT: [exports.bullet, ],
    MOTION_TYPE: 'micgrow',
    LABEL: 'Poison Gas',
    ACCEPTS_SCORE: false,
    POISON_TO_APPLY: 0,
  HEALTH: 9999999999999,
    POISON: true,
    SHOWPOISON: true,
    BODY: {
        PENETRATION: 1.25,
        SPEED: 4.5,
        RANGE: 125,
        DENSITY: 1.15,
        SHIELD: 125,
        HEALTH: 999 * wepHealthFactor,
        DAMAGE: 0 * wepDamageFactor,
        PUSHABILITY: 0,
    },
    FACING_TYPE: 'smoothWithMotion',
    CAN_GO_OUTSIDE_ROOM: true,
    HITS_OWN_TYPE: 'never',
    PERSISTS_AFTER_DEATH: true,
};
exports.healaura = {
    PARENT: [exports.bullet, ],
    MOTION_TYPE: 'expand',
    LABEL: 'Healing Aura',
    ACCEPTS_SCORE: false,
    BODY: {
        PENETRATION: 1.25,
        SPEED: 4.5,
        RANGE: 125,
        DENSITY: 1.15,
        SHIELD: 125,
        HEALTH: 5 * wepHealthFactor,
        DAMAGE: -1 * wepDamageFactor,
        PUSHABILITY: 0.3,
    },
    FACING_TYPE: 'smoothWithMotion',
    CAN_GO_OUTSIDE_ROOM: true,
    HITS_OWN_TYPE: 'never',
    PERSISTS_AFTER_DEATH: true,
};
exports.firebal2 = {
  LABEL: "Fireball",
  TYPE: "swarm",
  ACCEPTS_SCORE: false,
  FRAG: 'firebal2Frag',
  SHAPE: [[1.007,-0.16],[0.97,0.27],[0.72,0.71],[0.307,0.99],[-0.27,0.976],[-1.37,0.67],[-2.97,0.29],[-1.41,-0.004],[-1.71,-0.224],[-1.21,-0.43],[-2.32,-0.51],[-1,-0.81],[-1.44,-1.08],[0.387,-0.93],[0.787,-0.68]],
  MOTION_TYPE: "swarm",
  FACING_TYPE: "smoothWithMotion",
  CONTROLLERS: ["nearestDifferentMaster", "mapTargetToGoal"],
  CRAVES_ATTENTION: true,
  BODY: {
    ACCELERATION: 3,
    PENETRATION: 1,
    HEALTH: 0.1 * wepHealthFactor,
    DAMAGE: 2.5 * wepDamageFactor,
    SPEED: 7,
    RESIST: 1,
    RANGE: 1250,
    DENSITY: 1,
    PUSHABILITY: 0,
    FOV: 1
  },
  DIE_AT_RANGE: true,
  BUFF_VS_FOOD: true
};
exports.firebalp = {
  LABEL: "Posionball",
  TYPE: "swarm",
  ACCEPTS_SCORE: false,
  FRAG: 'poisonballfrag',
  SHAPE: [[1.007,-0.16],[0.97,0.27],[0.72,0.71],[0.307,0.99],[-0.27,0.976],[-1.37,0.67],[-2.97,0.29],[-1.41,-0.004],[-1.71,-0.224],[-1.21,-0.43],[-2.32,-0.51],[-1,-0.81],[-1.44,-1.08],[0.387,-0.93],[0.787,-0.68]],
  MOTION_TYPE: "swarm",
  FACING_TYPE: "smoothWithMotion",
  CONTROLLERS: ["nearestDifferentMaster", "mapTargetToGoal"],
  CRAVES_ATTENTION: true,
  BODY: {
    ACCELERATION: 3,
    PENETRATION: 1,
    HEALTH: 0.1 * wepHealthFactor,
    DAMAGE: 2.5 * wepDamageFactor,
    SPEED: 7,
    RESIST: 1,
    RANGE: 1250,
    DENSITY: 1,
    PUSHABILITY: 0,
    FOV: 1
  },
  DIE_AT_RANGE: true,
  BUFF_VS_FOOD: true
};
exports.firebal3 = {
  LABEL: "Volcanoball",
  TYPE: "swarm",
  ACCEPTS_SCORE: false,
  FRAG: 'firebal2Frag',
  SHAPE: [[1.007,-0.16],[0.97,0.27],[0.72,0.71],[0.307,0.99],[-0.27,0.976],[-1.37,0.67],[-2.97,0.29],[-1.41,-0.004],[-1.71,-0.224],[-1.21,-0.43],[-2.32,-0.51],[-1,-0.81],[-1.44,-1.08],[0.387,-0.93],[0.787,-0.68]],
  MOTION_TYPE: "swarm",
  FACING_TYPE: "smoothWithMotion",
  CONTROLLERS: ["nearestDifferentMaster", "mapTargetToGoal"],
  CRAVES_ATTENTION: true,
  BODY: {
    ACCELERATION: 3,
    PENETRATION: 1,
    HEALTH: 0.01 * wepHealthFactor,
    DAMAGE: 2.5 * wepDamageFactor,
    SPEED: 9,
    RESIST: 1,
    RANGE: 625,
    DENSITY: 1,
    PUSHABILITY: 0,
    FOV: 1
  },
  DIE_AT_RANGE: true,
  BUFF_VS_FOOD: true
};
exports.firebals = {
  LABEL: "Infernoball",
  TYPE: "swarm",
  ACCEPTS_SCORE: false,
  FRAG: 'spreadingfirefrag',
  SHAPE: [[1.007,-0.16],[0.97,0.27],[0.72,0.71],[0.307,0.99],[-0.27,0.976],[-1.37,0.67],[-2.97,0.29],[-1.41,-0.004],[-1.71,-0.224],[-1.21,-0.43],[-2.32,-0.51],[-1,-0.81],[-1.44,-1.08],[0.387,-0.93],[0.787,-0.68]],
  MOTION_TYPE: "swarm",
  FACING_TYPE: "smoothWithMotion",
  CONTROLLERS: ["nearestDifferentMaster", "mapTargetToGoal"],
  CRAVES_ATTENTION: true,
  BODY: {
    ACCELERATION: 3,
    PENETRATION: 1,
    HEALTH: 0.01 * wepHealthFactor,
    DAMAGE: 0.5 * wepDamageFactor,
    SPEED: 4,
    RESIST: 1,
    RANGE: 40,
    DENSITY: 0.1,
    PUSHABILITY: 0,
    FOV: 1.5
  },
  DIE_AT_RANGE: true,
  BUFF_VS_FOOD: true
};
exports.firebalg = {
  LABEL: "Great Fireball",
  TYPE: "swarm",
  ACCEPTS_SCORE: false,
  FRAG: 'firebalgFrag',
  SHAPE: [[1.007,-0.16],[0.97,0.27],[0.72,0.71],[0.307,0.99],[-0.27,0.976],[-1.37,0.67],[-2.97,0.29],[-1.41,-0.004],[-1.71,-0.224],[-1.21,-0.43],[-2.32,-0.51],[-1,-0.81],[-1.44,-1.08],[0.387,-0.93],[0.787,-0.68]],
  MOTION_TYPE: "swarm",
  FACING_TYPE: "smoothWithMotion",
  CONTROLLERS: ["nearestDifferentMaster", "mapTargetToGoal"],
  CRAVES_ATTENTION: true,
  BODY: {
    ACCELERATION: 6,
    PENETRATION: 1,
    HEALTH: 1 * wepHealthFactor,
    DAMAGE: 5 * wepDamageFactor,
    SPEED: 10,
    RESIST: 1,
    RANGE: 1500,
    DENSITY: 1,
    PUSHABILITY: 0,
    FOV: 2
  },
  DIE_AT_RANGE: true,
  BUFF_VS_FOOD: true
};
exports.firework = {
  LABEL: "Firework",
  ACCEPTS_SCORE: false,
  FRAG: 'fireworkfrag',
  FACING_TYPE: 'smoothWithMotion',
  SHAPE: [[-1.02,-0.53],[1.41,-0.52],[2.813,0.01],[2.81,0.016],[1.433,0.476],[-1,0.496],[-0.807,0]],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [12, 10, 1.4, 8, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.muchmorerecoil, g.lowpower]),
        TYPE: exports.bullet,
        AUTOFIRE: true
      }
    }
  ],
  BODY: {
    ACCELERATION: 3,
    PENETRATION: 1,
    HEALTH: 3 * wepHealthFactor,
    DAMAGE: 3 * wepDamageFactor,
    SPEED: 7,
    RESIST: 1,
    RANGE: 150,
    DENSITY: 1,
    PUSHABILITY: 0,
    FOV: 1
  },
  DIE_AT_RANGE: true,
};
exports.fallstar2 = {
  LABEL: "Falling Star 2.0",
  TYPE: "swarm",
  ACCEPTS_SCORE: false,
  SHAPE: 3,
  MOTION_TYPE: "swarm",
  FACING_TYPE: "smoothWithMotion",
  CONTROLLERS: ["nearestDifferentMaster", "mapTargetToGoal"],
  CRAVES_ATTENTION: true,
  BODY: {
    ACCELERATION: 3,
    PENETRATION: 1.5,
    HEALTH: 1 * wepHealthFactor,
    DAMAGE: 2 * wepDamageFactor,
    SPEED: 0.5,
    RESIST: 1.6,
    RANGE: 450,
    DENSITY: 12,
    PUSHABILITY: 0.5,
    FOV: 1.5
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 6, 2, 0, 0, 180, 0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.skim,
          g.doublereload,
          g.tonsmorrecoil,
          g.lotsmorrecoil,
          g.doublereload
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }],
        STAT_CALCULATOR: gunCalcNames.thruster
      }
    }
  ],
  DIE_AT_RANGE: true,
  BUFF_VS_FOOD: true
};
exports.autofallstar = {
  PARENT: [exports.fallstar2],
  AI: { FARMER: true },
  INDEPENDENT: true
};
exports.fireball = {
  PARENT: [exports.firebal2],
  AI: { FARMER: true },
  INDEPENDENT: true
};
exports.poisonball = {
  PARENT: [exports.firebalp],
  AI: { FARMER: true },
  INDEPENDENT: true
};
exports.volfireball = {
  PARENT: [exports.firebal3],
  AI: { FARMER: true },
  INDEPENDENT: true
};
exports.infireball = {
  PARENT: [exports.firebals],
  AI: { FARMER: true },
  INDEPENDENT: true
};
exports.grfireball = {
  PARENT: [exports.firebalg],
  AI: { FARMER: true },
  INDEPENDENT: true
};
exports.trueinfernado = {
  LABEL: "True Infernado",
  TYPE: "bullet",
  ACCEPTS_SCORE: false,
  SHAPE: [[0,1],[0.19,0.98],[0.413,0.91],[0.6,0.796],[0.77,0.63],[0.93,0.416],[0.98,0.2],[1,0.01],[0.96,-0.27],[0.81,-0.524],[0.573,-0.68],
          [0.3,-0.77],[-0.007,-0.77],[-0.28,-0.66],[-0.49,-0.464],[-0.607,-0.19],[-0.54,0.116],[-0.38,0.34],[-0.167,0.48],[0.02,0.53],[0.23,0.416],
          [0.35,0.196],[0.29,0],[0.113,-0.14],[-0.01,-0.04],[0.1,0.016],[0.06,0.15],[-0.14,0.19],[-0.23,-0.02],[-0.18,-0.25],[0.113,-0.41],
          [0.44,-0.36],[0.65,-0.104],[0.65,0.24],[0.42,0.576],[0.133,0.75],[-0.17,0.76],[-0.47,0.616],[-0.75,0.296],[-0.82,-0.11],[-0.75,-0.504],
          [-0.51,-0.804],[-0.16,-0.984],[0.18,-1],[0.573,-0.84],[0.84,-0.58],[0.97,-0.24],[0.85,-0.584],[0.57,-0.824],[0.18,-1],[-0.153,-0.99],
          [-0.52,-0.87],[-0.82,-0.6],[-0.98,-0.23],[-1.013,0.16],[-0.85,0.55],[-0.593,0.81],[-0.33,0.976]],
  BODY: {
    PENETRATION: 1,
    SPEED: 0,
    RANGE: 500,
    DENSITY: 1.25,
    HEALTH: 100 * wepHealthFactor,
    DAMAGE: 5 * wepDamageFactor,
    PUSHABILITY: 0.3
  },
  GUNS: [
    {
      /***    LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [14,    6,      1,      0,     -2,     90,      0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.slow, g.halfreload]),
        TYPE: [exports.flare1, { PERSISTS_AFTER_DEATH: true }]
      }
    },
    {
      POSITION: [14, 6, 1, 0, -2, 270, 0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.halfreload, g.halfreload, g.halfreload]),
        TYPE: [exports.fireball, { PERSISTS_AFTER_DEATH: true }]
      }
    }, {
      POSITION: [14, 6, 1, 0, -2, 0, 0.5],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.slow, g.halfreload]),
        TYPE: [exports.flare1, { PERSISTS_AFTER_DEATH: true }]
      }
    }, {
      POSITION: [14, 6, 1, 0, -2, 180, 0.5],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.slow, g.halfreload, g.halfreload, g.halfreload]),
        TYPE: [exports.fireball, { PERSISTS_AFTER_DEATH: true }]
      }
    }
  ],
  FACING_TYPE: "hyperspin",
  MOTION_TYPE: "grow",
  CAN_GO_OUTSIDE_ROOM: false,
  HITS_OWN_TYPE: "never",
  DRAW_HEALTH: true,
  DIE_AT_RANGE: true
};
exports.chaser = {
  LABEL: "Chasing Shot",
  TYPE: "swarm",
  ACCEPTS_SCORE: false,
  SHAPE: -3,
  MOTION_TYPE: "chase",
  FACING_TYPE: "smoothWithMotion",
  CONTROLLERS: ["nearestDifferentMaster", "mapTargetToGoal"],
  CRAVES_ATTENTION: true,
  BODY: {
    ACCELERATION: 3,
    PENETRATION: 1.5,
    HEALTH: 0.35 * wepHealthFactor,
    DAMAGE: 1.5 * wepDamageFactor,
    SPEED: 6,
    RESIST: 1.6,
    RANGE: 225,
    DENSITY: 12,
    PUSHABILITY: 0.5,
    FOV: 1.5
  },
  DIE_AT_RANGE: false,
  BUFF_VS_FOOD: true
};
exports.saw = {
  LABEL: "Saw",
  CONTROLLERS: ["spin"],
  TYPE: "bullet",
  ACCEPTS_SCORE: false,
  SHAPE: -6,
  BODY: {
    PENETRATION: 1,
    SPEED: 6,
    RANGE: 180,
    DENSITY: 2.5,
    HEALTH: 0.5 * wepHealthFactor,
    DAMAGE: 8 * wepDamageFactor,
    PUSHABILITY: 0.3
  },
  FACING_TYPE: "turnWithSpeed",
  CAN_GO_OUTSIDE_ROOM: false,
  HITS_OWN_TYPE: "never",
  DIE_AT_LOW_SPEED: false,
  DIE_AT_RANGE: true
};
exports.disc = {
  LABEL: "Disc",
  CONTROLLERS: ["spin"],
  TYPE: "bullet",
  ACCEPTS_SCORE: false,
  SHAPE: -8,
  BODY: {
    PENETRATION: 1,
    SPEED: 12,
    RANGE: 210,
    DENSITY: 1,
    HEALTH: 0.25 * wepHealthFactor,
    DAMAGE: 4 * wepDamageFactor,
    PUSHABILITY: 0.5
  },
  FACING_TYPE: "turnWithSpeed",
  CAN_GO_OUTSIDE_ROOM: false,
  HITS_OWN_TYPE: "never",
  DIE_AT_LOW_SPEED: false,
  DIE_AT_RANGE: true
};
exports.bruiser = {
  LABEL: "Brusing Saw",
  CONTROLLERS: ["spin"],
  TYPE: "bullet",
  ACCEPTS_SCORE: false,
  SHAPE: -6,
  BODY: {
    PENETRATION: 1,
    SPEED: 3,
    RANGE: 180,
    DENSITY: 2.5,
    HEALTH: 0.5 * wepHealthFactor,
    DAMAGE: 8 * wepDamageFactor,
    PUSHABILITY: 0.3
  },
  GUNS: [
    {
      /***    LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [14,    6,      1,      0,     -2,     90,      0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([g.basic, g.skim, g.doublereload, g.pound]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }]
      }
    },
    {
      POSITION: [14, 6, 1, 0, -2, 270, 0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([g.basic, g.skim, g.doublereload, g.pound]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }]
      }
    }
  ],
  FACING_TYPE: "turnWithSpeed",
  CAN_GO_OUTSIDE_ROOM: false,
  HITS_OWN_TYPE: "never",
  DIE_AT_LOW_SPEED: false,
  DIE_AT_RANGE: true
};
exports.basher = {
  LABEL: "Bashing Saw",
  CONTROLLERS: ["spin"],
  TYPE: "bullet",
  ACCEPTS_SCORE: false,
  SHAPE: -6,
  BODY: {
    PENETRATION: 1,
    SPEED: 3,
    RANGE: 180,
    DENSITY: 2.5,
    HEALTH: 0.5 * wepHealthFactor,
    DAMAGE: 8 * wepDamageFactor,
    PUSHABILITY: 0.3
  },
  GUNS: [
    {
      /***    LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [14,    6,      1,      0,     -2,     90,      0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([g.basic, g.skim, g.doublereload, g.pound]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }]
      }
    },
    {
      POSITION: [14, 6, 1, 0, -2, 270, 0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([g.basic, g.skim, g.doublereload, g.pound]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }]
      }
    }, {
      POSITION: [14, 6, 1, 0, -2, 0, 0.5],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([g.basic, g.skim, g.doublereload, g.pound]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }]
      }
    }, {
      POSITION: [14, 6, 1, 0, -2, 180, 0.5],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([g.basic, g.skim, g.doublereload, g.pound]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }]
      }
    }
  ],
  FACING_TYPE: "turnWithSpeed",
  CAN_GO_OUTSIDE_ROOM: false,
  HITS_OWN_TYPE: "never",
  DIE_AT_LOW_SPEED: false,
  DIE_AT_RANGE: true
};
exports.burn = {
  LABEL: "Burning Saw",
  CONTROLLERS: ["spin"],
  TYPE: "bullet",
  ACCEPTS_SCORE: false,
  SHAPE: -6,
  BODY: {
    PENETRATION: 1,
    SPEED: 1,
    RANGE: 180,
    DENSITY: 2.5,
    HEALTH: 0.5 * wepHealthFactor,
    DAMAGE: 8 * wepDamageFactor,
    PUSHABILITY: 0.3
  },
  GUNS: [
    {
      POSITION: [12, 5, 0.5, 8, 3, 90, 0]
    },
    {
      POSITION: [17, 7, 0.5, 8, 0, 90, 0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.doublereload,
          g.doublereload,
          g.doublereload,
          g.lowpower,
          g.norecoil
        ]),
        TYPE: exports.flare2
      }
    } ,{
      POSITION: [12, 5, 0.5, 8, 3, 270, 0]
    },
    {
      POSITION: [17, 7, 0.5, 8, 0, 270, 0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.doublereload,
          g.doublereload,
          g.doublereload,
          g.lowpower,
          g.norecoil
        ]),
        TYPE: exports.flare2
      }
    }
  ],
  FACING_TYPE: "turnWithSpeed",
  CAN_GO_OUTSIDE_ROOM: false,
  HITS_OWN_TYPE: "never",
  DIE_AT_LOW_SPEED: false,
  DIE_AT_RANGE: true
};
exports.bsaw = {
  LABEL: "BuzzSaw",
  CONTROLLERS: ["spin"],
  TYPE: "bullet",
  ACCEPTS_SCORE: false,
  SHAPE: -12,
  BODY: {
    PENETRATION: 1,
    SPEED: 4,
    RANGE: 160,
    DENSITY: 3.25,
    HEALTH: 0.75 * wepHealthFactor,
    DAMAGE: 9 * wepDamageFactor,
    PUSHABILITY: 0.3
  },
  FACING_TYPE: "turnWithSpeed",
  CAN_GO_OUTSIDE_ROOM: false,
  HITS_OWN_TYPE: "never",
  DIE_AT_LOW_SPEED: false,
  DIE_AT_RANGE: true

};exports.shred = {
  LABEL: "Shredder",
  CONTROLLERS: ["spin"],
  TYPE: "bullet",
  ACCEPTS_SCORE: false,
  SHAPE: 15,
  BODY: {
    PENETRATION: 2,
    SPEED: 1,
    RANGE: 200,
    DENSITY: 5,
    HEALTH: 1 * wepHealthFactor,
    DAMAGE: 11 * wepDamageFactor,
    PUSHABILITY: 0
  },
  FACING_TYPE: "turnWithSpeed",
  CAN_GO_OUTSIDE_ROOM: false,
  HITS_OWN_TYPE: "never",
  DIE_AT_LOW_SPEED: false,
  DIE_AT_RANGE: true
};
exports.bee = {
  PARENT: [exports.swarm],
  PERSISTS_AFTER_DEATH: true,
  SHAPE: 4,
  LABEL: "Drone",
  HITS_OWN_TYPE: "hardWithBuffer"
};
exports.autoswarm = {
  PARENT: [exports.swarm],
  AI: { FARMER: true },
  INDEPENDENT: true
};
exports.autoexswarm = {
  PARENT: [exports.exswarm],
  AI: { FARMER: true },
  INDEPENDENT: true,
  FRAG: 'firebal2Frag',
};
exports.autochase = {
  PARENT: [exports.chaser],
  AI: { FARMER: true },
  INDEPENDENT: true
};

exports.trap = {
  LABEL: "Thrown Trap",
  TYPE: "trap",
  ACCEPTS_SCORE: false,
  SHAPE: -3,
  MOTION_TYPE: "glide", // def
  FACING_TYPE: "turnWithSpeed",
  HITS_OWN_TYPE: "push",
  DIE_AT_RANGE: true,
  BODY: {
    HEALTH: 1 * wepHealthFactor,
    DAMAGE: 2 * wepDamageFactor,
    RANGE: 450,
    DENSITY: 2.5,
    RESIST: 2.5,
    SPEED: 0
  }
};
exports.block = {
  LABEL: "Set Trap",
  PARENT: [exports.trap],
  SHAPE: -4,
  MOTION_TYPE: "motor",
  CONTROLLERS: ["goToMasterTarget"],
  BODY: {
    SPEED: 1,
    DENSITY: 5
  }
};
exports.boomerang = {
  LABEL: "Boomerang",
  PARENT: [exports.trap],
  CONTROLLERS: ["boomerang"],
  MOTION_TYPE: "motor",
  HITS_OWN_TYPE: "never",
  SHAPE: -5,
  BODY: {
    SPEED: 1.25,
    RANGE: 120
  }
};

exports.drone = {
  LABEL: "Drone",
  TYPE: "drone",
  ACCEPTS_SCORE: false,
  DANGER: 2,
  CONTROL_RANGE: 0,
  SHAPE: 3,
  MOTION_TYPE: "chase",
  FACING_TYPE: "smoothToTarget",
  CONTROLLERS: [
    "nearestDifferentMaster",
    "canRepel",
    "mapTargetToGoal",
    "hangOutNearMaster"
  ],
  AI: { BLIND: true },
  BODY: {
    PENETRATION: 1.2,
    PUSHABILITY: 0.6,
    ACCELERATION: 0.05,
    HEALTH: 0.6 * wepHealthFactor,
    DAMAGE: 1.25 * wepDamageFactor,
    SPEED: 3.8,
    RANGE: 200,
    DENSITY: 0.03,
    RESIST: 1.5,
    FOV: 0.8
  },
  HITS_OWN_TYPE: "hard",
  DRAW_HEALTH: false,
  CLEAR_ON_MASTER_UPGRADE: true,
  BUFF_VS_FOOD: true
};
exports.charger = {
  LABEL: "Beast",
  TYPE: "drone",
  ACCEPTS_SCORE: false,
  DANGER: 2,
  CONTROL_RANGE: 0,
  SHAPE: 5,
  MOTION_TYPE: "chase",
  FACING_TYPE: "smoothToTarget",
  CONTROLLERS: [
    "nearestDifferentMaster",
    "canRepel",
    "mapTargetToGoal",
    "hangOutNearMaster"
  ],
  AI: { BLIND: true },
  BODY: {
    PENETRATION: 1.2,
    PUSHABILITY: 0.6,
    ACCELERATION: 0.05,
    HEALTH: 2.5 * wepHealthFactor,
    DAMAGE: 1.5 * wepDamageFactor,
    SPEED: 5,
    RANGE: 200,
    DENSITY: 0.03,
    RESIST: 1.5,
    FOV: 1
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 12, 2, 0, 0, 180, 0],
      PROPERTIES: {
        AUTOFIRE: false,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.charger,
          g.halfreload
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: false }],
        STAT_CALCULATOR: gunCalcNames.thruster
      }
    },  {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 12, 2, 0, 0, 180, 0],
      PROPERTIES: {
        AUTOFIRE: false,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.charger,
          g.halfreload
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: false }],
        STAT_CALCULATOR: gunCalcNames.thruster
      }
    },  {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 12, 2, 0, 0, 180, 0],
      PROPERTIES: {
        AUTOFIRE: false,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.charger,
          g.halfreload
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: false }],
        STAT_CALCULATOR: gunCalcNames.thruster
      }
    },  {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 12, 2, 0, 0, 180, 0],
      PROPERTIES: {
        AUTOFIRE: false,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.charger,
          g.halfreload
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: false }],
        STAT_CALCULATOR: gunCalcNames.thruster
      }
    },  {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 12, 2, 0, 0, 180, 0],
      PROPERTIES: {
        AUTOFIRE: false,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.charger,
          g.halfreload
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: false }],
        STAT_CALCULATOR: gunCalcNames.thruster
      }
    },
  ],
  HITS_OWN_TYPE: "hard",
  DRAW_HEALTH: true,
  CLEAR_ON_MASTER_UPGRADE: true,
  BUFF_VS_FOOD: true
};
exports.boostdrone = {
  LABEL: "Boost Drone",
  TYPE: "drone",
  ACCEPTS_SCORE: false,
  DANGER: 2,
  CONTROL_RANGE: 0,
  SHAPE: 3,
  MOTION_TYPE: "chase",
  FACING_TYPE: "smoothToTarget",
  CONTROLLERS: [
    "nearestDifferentMaster",
    "canRepel",
    "mapTargetToGoal",
    "hangOutNearMaster"
  ],
  AI: { BLIND: true },
  BODY: {
    PENETRATION: 1.2,
    PUSHABILITY: 0.6,
    ACCELERATION: 0.05,
    HEALTH: 0.6 * wepHealthFactor,
    DAMAGE: 1.25 * wepDamageFactor,
    SPEED: 3.8,
    RANGE: 200,
    DENSITY: 0.03,
    RESIST: 1.5,
    FOV: 0.8
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 6, 2, 0, 0, 180, 0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.skim,
          g.doublereload,
          g.tonsmorrecoil,
          g.doublereload
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }],
        STAT_CALCULATOR: gunCalcNames.thruster
      }
    }
  ],
  HITS_OWN_TYPE: "hard",
  DRAW_HEALTH: false,
  CLEAR_ON_MASTER_UPGRADE: true,
  BUFF_VS_FOOD: true
};
exports.fallstar = {
  LABEL: "Falling Star",
  TYPE: "drone",
  ACCEPTS_SCORE: false,
  DANGER: 2,
  CONTROL_RANGE: 0,
  SHAPE: 3,
  MOTION_TYPE: "chase",
  FACING_TYPE: "smoothToTarget",
  CONTROLLERS: [
    "nearestDifferentMaster",
    "canRepel",
    "mapTargetToGoal",
    "hangOutNearMaster"
  ],
  AI: { BLIND: true },
  BODY: {
    PENETRATION: 1.2,
    PUSHABILITY: 0.6,
    ACCELERATION: 0.05,
    HEALTH: 0.6 * wepHealthFactor,
    DAMAGE: 1.25 * wepDamageFactor,
    SPEED: 3.8,
    RANGE: 200,
    DENSITY: 0.03,
    RESIST: 1.5,
    FOV: 0.8
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 6, 2, 0, 0, 180, 0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.skim,
          g.doublereload,
          g.tonsmorrecoil,
          g.lotsmorrecoil,
          g.doublereload
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }],
        STAT_CALCULATOR: gunCalcNames.thruster
      }
    }
  ],
  HITS_OWN_TYPE: "hard",
  DRAW_HEALTH: false,
  CLEAR_ON_MASTER_UPGRADE: true,
  BUFF_VS_FOOD: true
};

exports.sunchip = {
  PARENT: [exports.drone],
  SHAPE: 4,
  NECRO: true,
  HITS_OWN_TYPE: "hard",
  BODY: {
    FOV: 0.5
  },
  AI: {
    BLIND: true,
    FARMER: true
  },
  DRAW_HEALTH: false
};
exports.autosunchip = {
  PARENT: [exports.sunchip],
  AI: {
    BLIND: true,
    FARMER: true
  },
  INDEPENDENT: true
};
exports.gunchip = {
  PARENT: [exports.drone],
  SHAPE: -2,
  NECRO: true,
  HITS_OWN_TYPE: "hard",
  BODY: {
    FOV: 0.5
  },
  AI: {
    BLIND: true,
    FARMER: true
  },
  DRAW_HEALTH: false
};

exports.missile = {
  PARENT: [exports.bullet],
  LABEL: "Missile",
  INDEPENDENT: true,
  BODY: {
    RANGE: 200
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [14, 6, 1, 0, -2, 130, 0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.skim,
          g.doublereload,
          g.lowpower,
          g.muchmorerecoil,
          g.morespeed,
          g.morespeed
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }],
        STAT_CALCULATOR: gunCalcNames.thruster
      }
    },
    {
      POSITION: [14, 6, 1, 0, 2, 230, 0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.skim,
          g.doublereload,
          g.lowpower,
          g.muchmorerecoil,
          g.morespeed,
          g.morespeed
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }],
        STAT_CALCULATOR: gunCalcNames.thruster
      }
    }
  ]
};
exports.wing = {
  PARENT: [exports.bullet],
  LABEL: "Wingman plane",
  INDEPENDENT: true,
  MOTION_TYPE: "chase",
  FACING_TYPE: "smoothToTarget",
  CONTROLLERS: [
    "nearestDifferentMaster",
    "mapTargetToGoal",
    "hangOutNearMaster"
  ],
  AI: { BLIND: true },
  BODY: {
    RANGE: 120,
    SPEED: 2.5
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [14, 6, 1, 0, -2, 140, 0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.skim,
          g.doublereload,
          g.lowpower,
          g.muchmorerecoil,
          g.morespeed,
          g.morespeed
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }],
        STAT_CALCULATOR: gunCalcNames.thruster
      }
    },
    {
      POSITION: [14, 6, 1, 0, 2, 220, 0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.skim,
          g.doublereload,
          g.lowpower,
          g.muchmorerecoil,
          g.morespeed,
          g.morespeed
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }],
        STAT_CALCULATOR: gunCalcNames.thruster
      }
    },
    {
      POSITION: [14, 6, 1, 0, 0, 0, 0],
      PROPERTIES: {
        AUTOFIRE: false,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.norecoil,
          g.mach,
          g.halfdamage,
          g.halfdamage
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }],
        STAT_CALCULATOR: gunCalcNames.thruster
      }
    }
  ]
};
exports.megaelec = {
  PARENT: [exports.bullet],
  LABEL: "Mega Electron",
  INDEPENDENT: true,
  MOTION_TYPE: "chase",
  FACING_TYPE: "smoothToTarget",
  CONTROLLERS: [
    "hangOutNearMaster"
  ],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [14, 6, 1, 0, 0, 180, 0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.doublereload,
          g.tonsmorrecoil,
          g.tonsmorrecoil,
          g.halfdamage,
          g.halfdamage,
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }],
        STAT_CALCULATOR: gunCalcNames.thruster
      }
    },
  ],
  AI: { BLIND: true },
  BODY: {
    RANGE: 99999,
    SPEED: 2.5,
    HEALTH: 2,
    DAMAGE: 1,
  },
  
};
exports.fjet = {
  PARENT: [exports.bullet],
  LABEL: "Fighter Jet",
  INDEPENDENT: true,
  MOTION_TYPE: "chase",
  FACING_TYPE: "smoothToTarget",
  CONTROLLERS: [
    "nearestDifferentMaster",
    "mapTargetToGoal",
    "hangOutNearMaster"
  ],
  AI: { BLIND: true },
  BODY: {
    RANGE: 120,
    SPEED: 2.5
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [14, 6, 1, 0, 0, 140, 0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.skim,
          g.doublereload,
          g.lowpower,
          g.muchmorerecoil,
          g.doublereload
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }],
        STAT_CALCULATOR: gunCalcNames.thruster
      }
    },
    {
      POSITION: [14, 6, 1, 0, 0, 220, 0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.skim,
          g.doublereload,
          g.lowpower,
          g.muchmorerecoil,
          g.doublereload
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }],
        STAT_CALCULATOR: gunCalcNames.thruster
      }
    },
    {
      POSITION: [14, 6, 1, 0, 0, 0, 0],
      PROPERTIES: {
        AUTOFIRE: false,
        LABEL: "Airforce Machine Gun",
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.norecoil,
          g.halfdamage,
          g.doublereload,
          g.doublereload,
          g.halfdamage,
          g.morespeed,
          g.morespeed,
          g.morespeed
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }],
        STAT_CALCULATOR: gunCalcNames.thruster
      }
    }
  ]
};
exports.apache = {
  PARENT: [exports.bullet],
  LABEL: "Apache Helicopter",
  INDEPENDENT: true,
  MOTION_TYPE: "chase",
  FACING_TYPE: "smoothToTarget",
  CONTROLLERS: [
    "nearestDifferentMaster",
    "mapTargetToGoal",
    "hangOutNearMaster"
  ],
  AI: { BLIND: true },
  BODY: {
    RANGE: 240,
    SPEED: 2
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [7.5, 6, 1, 0, 10, 0, 0],
      PROPERTIES: {
        AUTOFIRE: false,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.morespeed,
          g.morespeed,
          g.morespeed,
          g.doubledamage,
          g.doubledamage,
          g.halfreload,
          g.halfreload,
          g.halfreload
        ]),
        TYPE: [exports.autoswarm, { PERSISTS_AFTER_DEATH: true }]
      }
    },
    {
      POSITION: [7.5, 6, 1, 0, -10, 0, 0],
      PROPERTIES: {
        AUTOFIRE: false,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.morespeed,
          g.morespeed,
          g.morespeed,
          g.doubledamage,
          g.doubledamage,
          g.halfreload,
          g.halfreload,
          g.halfreload
        ]),
        TYPE: [exports.autoswarm, { PERSISTS_AFTER_DEATH: true }]
      }
    },
    {
      POSITION: [14, 6, 1, 0, 0, 0, 0],
      PROPERTIES: {
        AUTOFIRE: false,
        LABEL: "Apache Machine Gun",
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.norecoil,
          g.halfdamage,
          g.doublereload,
          g.halfdamage,
          g.morespeed
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }]
      }
    }
  ]
};
exports.chinook = {
  PARENT: [exports.bullet],
  LABEL: "Chinook",
  INDEPENDENT: true,
  MOTION_TYPE: "chase",
  FACING_TYPE: "smoothToTarget",
  CONTROLLERS: [
    "nearestDifferentMaster",
    "mapTargetToGoal",
    "hangOutNearMaster"
  ],
  AI: { BLIND: true },
  BODY: {
    RANGE: 120,
    SPEED: 2.5,
    FOV: 2.5,
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [14, 6, 1, 0, 0, 140, 0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.skim,
          g.doublereload,
          g.lowpower,
          g.muchmorerecoil,
          g.doublereload
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }],
        STAT_CALCULATOR: gunCalcNames.thruster
      }
    },
    {
      POSITION: [14, 6, 1, 0, 0, 220, 0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.skim,
          g.doublereload,
          g.lowpower,
          g.muchmorerecoil,
          g.doublereload
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }],
        STAT_CALCULATOR: gunCalcNames.thruster
      }
    },
    {
      POSITION: [25, 6, 2, 0, 0, 0, 0],
      PROPERTIES: {
        AUTOFIRE: false,
        LABEL: "Airforce Rail Gun",
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.morespeed,
          g.destroy,
          g.morespeed,
          g.morespeed,
          g.morespeed,
          g.morespeed,
          g.morespeed,
          g.morespeed,
          g.morespeed,
          
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }],
        STAT_CALCULATOR: gunCalcNames.thruster
      }
    },
    {
      POSITION: [20, 6, 2, 0, 0, 0, 0],
      PROPERTIES: {
        AUTOFIRE: false,
        LABEL: "Airforce Rail Gun",
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.morespeed,
          g.destroy,
          g.morespeed,
          g.morespeed,
          g.morespeed,
          g.morespeed,
          g.morespeed,
          g.morespeed,
          g.morespeed,
          
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }],
        STAT_CALCULATOR: gunCalcNames.thruster
      }
    }
  ]
};
exports.spin = {
  PARENT: [exports.bullet],
  LABEL: "Spinner Missile",
  INDEPENDENT: true,
  FACING_TYPE: "autospin",
  BODY: {
    RANGE: 200
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [14, 6, 1, 0, -2, 90, 0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([g.basic, g.skim, g.doublereload, g.mach]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }]
      }
    },
    {
      POSITION: [14, 6, 1, 0, -2, 270, 0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([g.basic, g.skim, g.doublereload, g.mach]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }]
      }
    }
  ]
};
exports.lilmiss = {
  PARENT: [exports.bullet],
  LABEL: "Little Missile",
  INDEPENDENT: true,
  BODY: {
    RANGE: 120
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [14, 6, 1, 0, 0, 180, 0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([g.basic, g.thruster, g.morerecoil]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }],
        STAT_CALCULATOR: gunCalcNames.thruster
      }
    }
  ]
};
exports.spark = {
  LABEL: "Sparker Spark",
  ACCEPTS_SCORE: false,
  SHAPE: -5,
  BODY: {
    PENETRATION: 2,
    SPEED: 0.5,
    RANGE: 150,
    SIZE: 1,
    DENSITY: 2,
    HEALTH: 10 * wepHealthFactor,
    DAMAGE: 3 * wepDamageFactor,
    PUSHABILITY: 0
  },
  MOTION_TYPE: "grow",
  FACING_TYPE: "autospin",
  CAN_GO_OUTSIDE_ROOM: false,
  HITS_OWN_TYPE: "never",
  DIE_AT_RANGE: true
};
exports.bonfbult = {
  PARENT: [exports.bullet],
  LABEL: "Bonfire Flare",
  INDEPENDENT: true,
  SHAPE: 4,
  BODY: {
    RANGE: 100
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [14, 10, 1.5, 0, 0, 180, 0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.skim,
          g.doublereload,
          g.doublereload,
          g.tonsmorerecoil,
          g.morespeed
          
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: false }],
        STAT_CALCULATOR: gunCalcNames.thruster
      }
    },
    {
      POSITION: [10, 6, 1.3, 0, 2, 90, 0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([g.basic, g.slow]),
        TYPE: [exports.flare1, { PERSISTS_AFTER_DEATH: true }]
      }
    },
    {
      POSITION: [10, 6, 1.3, 0, 2, 270, 0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([g.basic, g.slow]),
        TYPE: [exports.flare1, { PERSISTS_AFTER_DEATH: true }]
      }
    }
  ]
};
exports.hypermissile = {
  PARENT: [exports.missile],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [14, 6, 1, 0, -2, 150, 0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.skim,
          g.doublereload,
          g.lowpower,
          g.morerecoil,
          g.morespeed
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }],
        STAT_CALCULATOR: gunCalcNames.thruster
      }
    },
    {
      POSITION: [14, 6, 1, 0, 2, 210, 0],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.skim,
          g.doublereload,
          g.lowpower,
          g.morerecoil,
          g.morespeed
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }],
        STAT_CALCULATOR: gunCalcNames.thruster
      }
    },
    {
      POSITION: [14, 6, 1, 0, -2, 90, 0.5],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.skim,
          g.doublereload,
          g.lowpower,
          g.morerecoil,
          g.morespeed
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }]
      }
    },
    {
      POSITION: [14, 6, 1, 0, 2, 270, 0.5],
      PROPERTIES: {
        AUTOFIRE: true,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.skim,
          g.doublereload,
          g.lowpower,
          g.morerecoil,
          g.morespeed
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }]
      }
    }
  ]
};
exports.snake = {
  PARENT: [exports.bullet],
  LABEL: "Snake",
  INDEPENDENT: true,
  BODY: {
    RANGE: 120
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [6, 12, 1.4, 8, 0, 180, 0],
      PROPERTIES: {
        AUTOFIRE: true,
        STAT_CALCULATOR: gunCalcNames.thruster,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.sniper,
          g.hunter,
          g.hunter2,
          g.snake,
          g.snakeskin
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }]
      }
    },
    {
      POSITION: [10, 12, 0.8, 8, 0, 180, 0.5],
      PROPERTIES: {
        AUTOFIRE: true,
        NEGATIVE_RECOIL: true,
        STAT_CALCULATOR: gunCalcNames.thruster,
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.sniper,
          g.hunter,
          g.hunter2,
          g.snake
        ]),
        TYPE: [exports.bullet, { PERSISTS_AFTER_DEATH: true }]
      }
    }
  ]
};
exports.hive = {
  PARENT: [exports.bullet],
  LABEL: "Hive",
  BODY: {
    RANGE: 90,
    FOV: 0.5
  },
  FACING_TYPE: "turnWithSpeed",
  INDEPENDENT: true,
  CONTROLLERS: ["alwaysFire", "nearestDifferentMaster", "targetSelf"],
  AI: { NO_LEAD: true },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [7, 9.5, 0.6, 7, 0, 108, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm, g.hive, g.bees]),
        TYPE: exports.bee,
        STAT_CALCULATOR: gunCalcNames.swarm
      }
    },
    {
      POSITION: [7, 9.5, 0.6, 7, 0, 180, 0.2],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm, g.hive, g.bees]),
        TYPE: exports.bee,
        STAT_CALCULATOR: gunCalcNames.swarm
      }
    },
    {
      POSITION: [7, 9.5, 0.6, 7, 0, 252, 0.4],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm, g.hive, g.bees]),
        TYPE: exports.bee,
        STAT_CALCULATOR: gunCalcNames.swarm
      }
    },
    {
      POSITION: [7, 9.5, 0.6, 7, 0, 324, 0.6],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm, g.hive, g.bees]),
        TYPE: exports.bee,
        STAT_CALCULATOR: gunCalcNames.swarm
      }
    },
    {
      POSITION: [7, 9.5, 0.6, 7, 0, 36, 0.8],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm, g.hive, g.bees]),
        TYPE: exports.bee,
        STAT_CALCULATOR: gunCalcNames.swarm
      }
    }
  ]
};

// TANK CLASSES
const base = {
  ACCEL: 1.6,
  SPEED: 5.25,
  HEALTH: 20,
  DAMAGE: 3,
  RESIST: 1,
  PENETRATION: 1.05,
  SHIELD: 8,
  REGEN: 0.025,
  FOV: 1,
  DENSITY: 0.5
};
exports.genericTank = {
  LABEL: "Unknown Class",
  TYPE: "tank",
  DAMAGE_CLASS: 2,
  DANGER: 5,
  MOTION_TYPE: "motor",
  FACING_TYPE: "toTarget",
  SIZE: 12,
  MAX_CHILDREN: 0,
  DAMAGE_EFFECTS: false,
  BODY: {
    // def
    ACCELERATION: base.ACCEL,
    SPEED: base.SPEED,
    HEALTH: base.HEALTH,
    DAMAGE: base.DAMAGE,
    PENETRATION: base.PENETRATION,
    SHIELD: base.SHIELD,
    REGEN: base.REGEN,
    FOV: base.FOV,
    DENSITY: base.DENSITY,
    PUSHABILITY: 0.9,
    HETERO: 3
  },
  GUNS: [],
  TURRETS: [],
  GIVE_KILL_MESSAGE: true,
  DRAW_HEALTH: true
};
let gun = {};

exports.autoTurret = {
  PARENT: [exports.genericTank],
  LABEL: "Turret",
  BODY: {
    FOV: 0.8
  },
  COLOR: 16,
  CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 10, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.power,
          g.morerecoil,
          g.turret
        ]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.fortautoTurret = {
  PARENT: [exports.genericTank],
  LABEL: "Fort Turret",
  BODY: {
    FOV: 1.3
  },
  COLOR: 16,
  CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 10, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.halfreload,
          g.halfreload,
          g.halfreload,
          g.norecoil,
          g.fast,
          g.fast,
          g.fast,
        ]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.fortmachgunTurret = {
  PARENT: [exports.genericTank],
  LABEL: "Fort Mach Turret",
  BODY: {
    FOV: 0.8
  },
  COLOR: 16,
  CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 10, 1.75, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.halfreload,
          g.halfreload,
          g.doublerange,
          g.norecoil,
          g.slow,
          g.halfrange,
          g.halfrange,
        ]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.fortcenterTurret = {
  PARENT: [exports.genericTank],
  LABEL: "Central Turret",
  BODY: {
    FOV: 6.5
  },
  COLOR: 16,
  CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [44, 10, 0.8, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.norecoil,
          g.fast,
          g.fast,
          g.fast,
          g.fast,
          g.fast,
          g.fast,
        ]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.fortshotTurret = {
  PARENT: [exports.genericTank],
  LABEL: "Fort Spray Turret",
  BODY: {
    FOV: 1.1
  },
  COLOR: 16,
  CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 10, 1.5, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.doublerange,
          g.norecoil,
          g.mach,
          g.halfreload,
          g.halfreload,
          g.halfreload,
          g.fast,
          g.fast,
        ]),
        TYPE: exports.bullet
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 10, 1.5, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.doublerange,
          g.norecoil,
          g.mach,
          g.halfreload,
          g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 10, 1.5, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.doublerange,
          g.norecoil,
          g.mach,
          g.halfreload,
          g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 10, 1.5, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.doublerange,
          g.norecoil,
          g.mach,
          g.halfreload,
          g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 10, 1.5, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.doublerange,
          g.norecoil,
          g.mach,
          g.halfreload,
          g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [12, 15, 0.8, 0, 0, 0, 0],
    },
  ]
};
exports.trappTurret = {
  PARENT: [exports.genericTank],
  LABEL: "Trap Turret",
  BODY: {
    FOV: 0.8
  },
  COLOR: 16,
  CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 10, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.power,
          g.norecoil,
          g.turret,
          g.fast,
          g.fast,
          g.doublerange
        ]),
        TYPE: exports.trap
      }
    }
  ]
};
exports.forttrappTurret = {
  PARENT: [exports.genericTank],
  LABEL: "Fort Trap Turret",
  BODY: {
    FOV: 1.6
  },
  COLOR: 16,
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 20, 0.75, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.halfreload,
          g.halfreload,
          g.norecoil,
          g.fast,
          g.fast,
        ]),
        TYPE: exports.trap
      }
    }
  ]
};
exports.spectregun = {
  PARENT: [exports.genericTank],
  LABEL: "Spectre Gun",
  BODY: {
    FOV: 0.8
  },
  COLOR: 16,
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [5, 5, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.power,
          g.morerecoil,
          g.turret, g.doublereload
        ]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [5, 5, 1, 0, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.power,
          g.morerecoil,
          g.turret, g.doublereload
        ]),
        TYPE: exports.sbomb
      }
    }, 
  ]
};
exports.flyfortgun = {
  PARENT: [exports.genericTank],
  LABEL: "Flying Fortress Gun",
  BODY: {
    FOV: 0.8
  },
  COLOR: 16,
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [5, 5, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.power,
          g.morerecoil,
          g.turret, g.doublereload, g.doublereload, g.doubledamage
        ]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [5, 5, 1, 0, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.power,
          g.morerecoil,
          g.turret, g.doublereload, g.doublereload
        ]),
        TYPE: exports.sbomb
      }
    }, 
  ]
};
exports.helirotor = {
  PARENT: [exports.genericTank],
  LABEL: "Rotor",
  CONTROLLERS: ['fastspin'],
    INDEPENDENT: true,
  BODY: {
    FOV: 0
  },
  COLOR: 16,
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [88, 10, 1, 0, 0, 0, 0],
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [88, 10, 1, 0, 0, 90, 0],
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [88, 10, 1, 0, 0, 180, 0],
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [88, 10, 1, 0, 0, 270, 0],
    }, 
  ]
};
exports.chinrotor1 = {
  PARENT: [exports.genericTank],
  LABEL: "Rotor",
  CONTROLLERS: ['spin'],
    INDEPENDENT: true,
  BODY: {
    FOV: 0
  },
  COLOR: 16,
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [44, 10, 1, 0, 0, 0, 0],
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [44, 10, 1, 0, 0, 90, 0],
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [44, 10, 1, 0, 0, 180, 0],
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [44, 10, 1, 0, 0, 270, 0],
    }, 
  ]
};
exports.chinrotor2 = {
  PARENT: [exports.genericTank],
  LABEL: "Rotor",
  CONTROLLERS: ['reversespin'],
    INDEPENDENT: true,
  BODY: {
    FOV: 0
  },
  COLOR: 16,
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [44, 10, 1, 0, 0, 0, 0],
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [44, 10, 1, 0, 0, 90, 0],
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [44, 10, 1, 0, 0, 180, 0],
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [44, 10, 1, 0, 0, 270, 0],
    }, 
  ]
};
exports.reverserotor = {
  PARENT: [exports.genericTank],
  LABEL: "Rotor",
  CONTROLLERS: ['reversespin'],
    INDEPENDENT: true,
  BODY: {
    FOV: 0
  },
  COLOR: 16,
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 10, 1, 0, 0, 0, 0],
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 10, 1, 0, 0, 90, 0],
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 10, 1, 0, 0, 180, 0],
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 10, 1, 0, 0, 270, 0],
    }, 
  ]
};
exports.lazerturr = {
  PARENT: [exports.genericTank],
  LABEL: "Lazer Turret",
  BODY: {
    FOV: 0.8
  },
  COLOR: 16,
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 10, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.morespeed,
          g.doubledamage,
          g.norecoil,
          g.doubledamage,
          g.morespeed
        ]),
        TYPE: exports.lazer
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 10, 1, 0, 0, 0, 0.33],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.morespeed,
          g.doubledamage,
          g.norecoil,
          g.doubledamage,
          g.morespeed
        ]),
        TYPE: exports.lazer
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 10, 1, 0, 0, 0, 0.67],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.morespeed,
          g.doubledamage,
          g.norecoil,
          g.doubledamage,
          g.morespeed
        ]),
        TYPE: exports.lazer
      }
    }, 
  ]
};
exports.lazerturr2 = {
  PARENT: [exports.genericTank],
  LABEL: "Lazer Turret 2",
  BODY: {
    FOV: 0.8
  },
  COLOR: 16,
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 10, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.morespeed,
          g.doubledamage,
          g.norecoil,
          g.doubledamage,
          g.morespeed
        ]),
        TYPE: exports.lazer
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 10, 1, 0, 0, 120, 0.33],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.morespeed,
          g.doubledamage,
          g.norecoil,
          g.doubledamage,
          g.morespeed
        ]),
        TYPE: exports.lazer
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 10, 1, 0, 0, 240, 0.67],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.morespeed,
          g.doubledamage,
          g.norecoil,
          g.doubledamage,
          g.morespeed
        ]),
        TYPE: exports.lazer
      }
    }, 
  ]
};
exports.railTurret = {
  PARENT: [exports.genericTank],
  LABEL: "Turret",
  BODY: {
    FOV: 0.8
  },
  COLOR: 16,
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      POSITION: [27, 8, 1.2, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.morespeed,
          g.destroy,
          g.morespeed
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [23, 8, 1.2, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.morespeed,
          g.destroy,
          g.morespeed
        ]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.TtRTurret = {
  PARENT: [exports.genericTank],
  LABEL: "Turret",
  BODY: {
    FOV: 0.8
  },
  COLOR: 36,
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 10, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.power,
          g.morerecoil,
          g.turret,
          g.tastetherainbow
        ]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.twinTurret = {
  PARENT: [exports.genericTank],
  LABEL: "Turret",
  BODY: {
    FOV: 0.8
  },
  COLOR: 16,
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 10, 1, 0, 5, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.power,
          g.morerecoil,
          g.turret
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [22, 10, 1, 0, -5, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.power,
          g.morerecoil,
          g.turret
        ]),
        TYPE: exports.bullet
      }
    }
  ]
}; 
exports.machineAutoTurret = {
  PARENT: [exports.genericTank],
  LABEL: "Turret",
  COLOR: 16,
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [14, 11, 1.3, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.power,
          g.morerecoil,
          g.turret,
          g.mach
        ]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.dustdevilAutoTurret = {
  PARENT: [exports.genericTank],
  LABEL: "Turret",
  CONTROLLERS: ['spin'],
  COLOR: 16,
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [10, 10, 1, 0, 15, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.spam]),
        TYPE: exports.bullet,
        AUTOFIRE: true
      }
    },
    {
      POSITION: [10, 10, 1, 0, 15, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.spam]),
        TYPE: exports.bullet,
        AUTOFIRE: true
      }
    }
  ]
};
exports.autoSmasherTurret = {
  PARENT: [exports.genericTank],
  LABEL: "Turret",
  COLOR: 16,
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 6, 1, 0, 5, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.power,
          g.morerecoil,
          g.turret,
          g.fast,
          g.mach,
          g.pound,
          g.morereload,
          g.morereload
        ]),
        TYPE: exports.bullet,
        STAT_CALCULATOR: gunCalcNames.fixedReload
      }
    },
    {
      POSITION: [20, 6, 1, 0, -5, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.power,
          g.morerecoil,
          g.turret,
          g.fast,
          g.mach,
          g.pound,
          g.morereload,
          g.morereload
        ]),
        TYPE: exports.bullet,
        STAT_CALCULATOR: gunCalcNames.fixedReload
      }
    }
  ]
};
exports.oldAutoSmasherTurret = {
  PARENT: [exports.genericTank],
  LABEL: "Turret",
  COLOR: 16,
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 7, 1, 0, -5.75, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.power,
          g.lotsmorrecoil,
          g.morereload
        ]),
        TYPE: exports.bullet,
        STAT_CALCULATOR: gunCalcNames.fixedReload
      }
    },
    {
      POSITION: [20, 7, 1, 0, 5.75, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.power,
          g.lotsmorrecoil,
          g.morereload
        ]),
        TYPE: exports.bullet,
        STAT_CALCULATOR: gunCalcNames.fixedReload
      }
    }
  ]
};

exports.auto3gun = {
  PARENT: [exports.genericTank],
  LABEL: "",
  BODY: {
    FOV: 3
  },
  CONTROLLERS: [
    "canRepel",
    "onlyAcceptInArc",
    "mapAltToFire",
    "nearestDifferentMaster"
  ],
  COLOR: 16,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 10, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.auto]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.auto5gun = {
  PARENT: [exports.genericTank],
  LABEL: "",
  BODY: {
    FOV: 3
  },
  CONTROLLERS: [
    "canRepel",
    "onlyAcceptInArc",
    "mapAltToFire",
    "nearestDifferentMaster"
  ],
  COLOR: 16,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [24, 11, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.auto, g.five]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.heavy3gun = {
  PARENT: [exports.genericTank],
  LABEL: "",
  BODY: {
    FOV: 2,
    SPEED: 0.9
  },
  CONTROLLERS: [
    "canRepel",
    "onlyAcceptInArc",
    "mapAltToFire",
    "nearestDifferentMaster"
  ],
  COLOR: 16,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 14, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound, g.auto]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.masterGun = {
  PARENT: [exports.genericTank],
  LABEL: "",
  BODY: {
    FOV: 3
  },
  CONTROLLERS: ["nearestDifferentMaster"],
  COLOR: 16,
  MAX_CHILDREN: 6,
  AI: {
    NO_LEAD: true,
    SKYNET: true,
    FULL_VIEW: true
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [8, 14, 1.3, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.drone, g.master]),
        TYPE: exports.drone,
        AUTOFIRE: true,
        SYNCS_SKILLS: true,
        STAT_CALCULATOR: gunCalcNames.drone
      }
    }
  ]
};
exports.sniper3gun = {
  PARENT: [exports.genericTank],
  LABEL: "",
  BODY: {
    FOV: 5
  },
  CONTROLLERS: [
    "canRepel",
    "onlyAcceptInArc",
    "mapAltToFire",
    "nearestDifferentMaster"
  ],
  COLOR: 16,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [27, 9, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.sniper,
          g.auto,
          g.assass,
          g.autosnipe
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 9, -1.5, 8, 0, 0, 0]
    }
  ]
};
exports.bansheegun = {
  PARENT: [exports.genericTank],
  LABEL: "",
  CONTROLLERS: [
    "canRepel",
    "onlyAcceptInArc",
    "mapAltToFire",
    "nearestDifferentMaster"
  ],
  COLOR: 16,
  INDEPENDENT: true,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [26, 10, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.auto, g.lessreload]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.auto4gun = {
  PARENT: [exports.genericTank],
  LABEL: "",
  BODY: {
    FOV: 2
  },
  CONTROLLERS: [
    "canRepel",
    "onlyAcceptInArc",
    "mapAltToFire",
    "nearestDifferentMaster"
  ],
  COLOR: 16,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [16, 4, 1, 0, -3.5, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.auto,
          g.gunner,
          g.twin,
          g.power,
          g.slow
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [16, 4, 1, 0, 3.5, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.auto,
          g.gunner,
          g.twin,
          g.power,
          g.slow
        ]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.bigauto4gun = {
  PARENT: [exports.genericTank],
  LABEL: "",
  CONTROLLERS: [
    "canRepel",
    "onlyAcceptInArc",
    "mapAltToFire",
    "nearestDifferentMaster"
  ],
  COLOR: 16,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [14, 5, 1, 0, -4.5, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.auto,
          g.gunner,
          g.twin,
          g.twin,
          g.power,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [14, 5, 1, 0, 4.5, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.auto,
          g.gunner,
          g.twin,
          g.twin,
          g.power,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [16, 5, 1, 0, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.auto,
          g.gunner,
          g.twin,
          g.twin,
          g.power,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    }
  ]
};

exports.tritrapgun = {
  PARENT: [exports.genericTank],
  LABEL: "",
  COLOR: 16,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 16, 1, 0, 0, 0, 0]
    },
    {
      POSITION: [2, 16, 1.1, 20, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap, g.block, g.auto]),
        TYPE: exports.block
      }
    }
  ]
};
exports.smasherBody = {
  LABEL: "",
  CONTROLLERS: ["spin"],
  COLOR: 9,
  SHAPE: 6,
  INDEPENDENT: true
};
exports.spikeBody = {
  LABEL: "",
  CONTROLLERS: ["spin"],
  COLOR: 9,
  SHAPE: -4,
  INDEPENDENT: true
};
exports.spikeBody1 = {
  LABEL: "",
  CONTROLLERS: ["fastspin"],
  COLOR: 9,
  SHAPE: 3,
  INDEPENDENT: true
};
exports.spikeBody2 = {
  LABEL: "",
  CONTROLLERS: ["reversespin"],
  COLOR: 9,
  SHAPE: 3,
  INDEPENDENT: true
};
exports.megasmashBody = {
  LABEL: "",
  CONTROLLERS: ["spin"],
  COLOR: 9,
  SHAPE: -6,
  INDEPENDENT: true
};
exports.jumpbody = {
  LABEL: "",
  COLOR: 16,
  INDEPENDENT: true
};
exports.dominationBody = {
  LABEL: "",
  CONTROLLERS: ["dontTurn"],
  COLOR: 9,
  SHAPE: 8,
  INDEPENDENT: true
};
exports.baseSwarmTurret = {
  PARENT: [exports.genericTank],
  LABEL: "Protector",
  COLOR: 16,
  BODY: {
    FOV: 2
  },
  CONTROLLERS: ["nearestDifferentMaster"],
  AI: {
    NO_LEAD: true,
    LIKES_SHAPES: true
  },
  INDEPENDENT: true,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [5, 4.5, 0.6, 7, 2, 0, 0.15],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm, g.protectorswarm]),
        TYPE: exports.swarm,
        STAT_CALCULATOR: gunCalcNames.swarm
      }
    },
    {
      POSITION: [5, 4.5, 0.6, 7, -2, 0, 0.15],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm, g.protectorswarm]),
        TYPE: exports.swarm,
        STAT_CALCULATOR: gunCalcNames.swarm
      }
    },
    {
      POSITION: [5, 4.5, 0.6, 7.5, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm, g.protectorswarm]),
        TYPE: [
          exports.swarm,
          { INDEPENDENT: true, AI: { LIKES_SHAPES: true } }
        ],
        STAT_CALCULATOR: gunCalcNames.swarm
      }
    }
  ]
};
exports.baseGunTurret = {
  PARENT: [exports.genericTank],
  LABEL: "Protector",
  BODY: {
    FOV: 5
  },
  ACCEPTS_SCORE: false,
  CONTROLLERS: ["nearestDifferentMaster"],
  INDEPENDENT: true,
  COLOR: 16,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [12, 12, 1, 6, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.destroy]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [11, 13, 1, 6, 0, 0, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.destroy]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [7, 13, -1.3, 6, 0, 0, 0]
    }
  ]
};
exports.baseProtector = {
  PARENT: [exports.genericTank],
  LABEL: "Base",
  SIZE: 64,
  DAMAGE_CLASS: 0,
  ACCEPTS_SCORE: false,
  SKILL: skillSet({
    rld: 1,
    dam: 1,
    pen: 1,
    spd: 1,
    str: 1
  }),
  BODY: {
    // def
    SPEED: 0,
    HEALTH: 10000,
    DAMAGE: 10,
    PENETRATION: 0.25,
    SHIELD: 1000,
    REGEN: 100,
    FOV: 1,
    PUSHABILITY: 0,
    HETERO: 0
  },
  //CONTROLLERS: ['nearestDifferentMaster'],
  FACING_TYPE: "autospin",
  TURRETS: [
    {
      /*  SIZE     X       Y     ANGLE    ARC */
      POSITION: [25, 0, 0, 0, 360, 0],
      TYPE: exports.dominationBody
    },
    {
      POSITION: [12, 7, 0, 45, 100, 0],
      TYPE: exports.baseSwarmTurret
    },
    {
      POSITION: [12, 7, 0, 135, 100, 0],
      TYPE: exports.baseSwarmTurret
    },
    {
      POSITION: [12, 7, 0, 225, 100, 0],
      TYPE: exports.baseSwarmTurret
    },
    {
      POSITION: [12, 7, 0, 315, 100, 0],
      TYPE: exports.baseSwarmTurret
    }
  ],
  GUNS: [
    /***** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */ {
      POSITION: [4.5, 11.5, -1.3, 6, 0, 45, 0]
    },
    {
      POSITION: [4.5, 11.5, -1.3, 6, 0, 135, 0]
    },
    {
      POSITION: [4.5, 11.5, -1.3, 6, 0, 225, 0]
    },
    {
      POSITION: [4.5, 11.5, -1.3, 6, 0, 315, 0]
    },
    {
      POSITION: [4.5, 8.5, -1.5, 7, 0, 45, 0]
    },
    {
      POSITION: [4.5, 8.5, -1.5, 7, 0, 135, 0]
    },
    {
      POSITION: [4.5, 8.5, -1.5, 7, 0, 225, 0]
    },
    {
      POSITION: [4.5, 8.5, -1.5, 7, 0, 315, 0]
    }
  ]
};


exports.permhelimin = {
  PARENT: [exports.genericTank],
  LABEL: "Permanent Pilot",
  TYPE: "minion",
  HITS_OWN_TYPE: "hardWithBuffer",
  FACING_TYPE: "smoothToTarget",
  DANGER: 3,
  DIE_AT_RANGE: false,
  BODY: {
    FOV: 0.5,
    SPEED: 10,
    ACCELERATION: 1,
    HEALTH: 999,
    SHIELD: 0,
    DAMAGE: 2,
    RESIST: 1,
    PENETRATION: 1.2,
    DENSITY: 1
  },
  GUNS: [
    {
      POSITION: [16, 3.5, 1, 0, 3.75, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray, g.doubledamage]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [16, 3.5, 1, 0, -3.75, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray, g.doubledamage]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [32, 7, 1, 0, 0, 180, 0],
    },
  ],
  TURRETS: [
    {
      /*********  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, 0, 0, 45, 360, 1],
      TYPE: [exports.helirotor]
    }, {
      /*********  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, -20, 20, 45, 360, 1],
      TYPE: [exports.reverserotor]
    }, 
  ],
  CONTROLLERS: [
    "nearestDifferentMaster",
    "mapAltToFire",
    "minion",
    "canRepel",
    "hangOutNearMaster"
  ],
  AI: {
    BLIND: true
  },
};
exports.helimin = {
  PARENT: [exports.genericTank],
  LABEL: "Heli Pilot",
  TYPE: "minion",
  HITS_OWN_TYPE: "hardWithBuffer",
  FACING_TYPE: "smoothToTarget",
  DANGER: 3,
  DIE_AT_RANGE: true,
  BODY: {
    FOV: 0.5,
    SPEED: 10,
    ACCELERATION: 1,
    HEALTH: 7.5,
    SHIELD: 0,
    RANGE: 250,
    DAMAGE: 1.5,
    RESIST: 1,
    PENETRATION: 1.2,
    DENSITY: 1
  },
  GUNS: [
    {
      POSITION: [16, 3.5, 1, 0, 3.75, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray, g.doubledamage]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [16, 3.5, 1, 0, -3.75, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray, g.doubledamage]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [32, 7, 1, 0, 0, 180, 0],
    },
  ],
  TURRETS: [
    {
      /*********  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, 0, 0, 45, 360, 1],
      TYPE: [exports.helirotor]
    }, {
      /*********  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, -20, 20, 45, 360, 1],
      TYPE: [exports.reverserotor]
    }, 
  ],
  CONTROLLERS: [
    "nearestDifferentMaster",
    "mapAltToFire",
    "minion",
    "canRepel",
    "hangOutNearMaster"
  ],
  AI: {
    BLIND: true
  },
};
exports.minion = {
  PARENT: [exports.genericTank],
  LABEL: "Minion",
  TYPE: "minion",
  DAMAGE_CLASS: 0,
  HITS_OWN_TYPE: "hardWithBuffer",
  FACING_TYPE: "smoothToTarget",
  BODY: {
    FOV: 0.5,
    SPEED: 3,
    ACCELERATION: 0.4,
    HEALTH: 5,
    SHIELD: 0,
    DAMAGE: 1.2,
    RESIST: 1,
    PENETRATION: 1,
    DENSITY: 0.4
  },
  AI: {
    BLIND: true
  },
  DRAW_HEALTH: false,
  CLEAR_ON_MASTER_UPGRADE: true,
  GIVE_KILL_MESSAGE: false,
  CONTROLLERS: [
    "nearestDifferentMaster",
    "mapAltToFire",
    "minion",
    "canRepel",
    "hangOutNearMaster"
  ],
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [17, 9, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.minion]),
        WAIT_TO_CYCLE: true,
        TYPE: exports.bullet
      }
    }
  ]
};
exports.marine = {
  PARENT: [exports.genericTank],
  LABEL: "Marine",
  TYPE: "minion",
  DAMAGE_CLASS: 0,
  DIE_AT_RANGE: true,
  HITS_OWN_TYPE: "hardWithBuffer",
  FACING_TYPE: "smoothToTarget",
  BODY: {
    FOV: 5,
    SPEED: 0,
    ACCELERATION: 0.1,
    HEALTH: 100,
    SHIELD: 50,
    RANGE: 1200,
    DAMAGE: 1.5,
    RESIST: 1,
    PENETRATION: 1,
    DENSITY: 0.4
  },
  AI: {
    BLIND: true
  },
  DRAW_HEALTH: false,
  CLEAR_ON_MASTER_UPGRADE: true,
  GIVE_KILL_MESSAGE: false,
  CONTROLLERS: [
    "nearestDifferentMaster",
    "mapAltToFire",
    "minion",
    "hangOutNearMaster"
  ],
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [12, 5.5, 1, 0, -4, 40, 0],
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [17, 3, 1, 0, 4, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.doublereload, g.doublereload, g.doublereload, g.norecoil, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        WAIT_TO_CYCLE: true,
        TYPE: exports.bullet
      }
    }, 
  ]
};
exports.minidust = {
  PARENT: [exports.genericTank],
  LABEL: "Superstorm Dust Devil",
  TYPE: "minion",
  DAMAGE_CLASS: 0,
  HITS_OWN_TYPE: "hardWithBuffer",
  FACING_TYPE: "autospin",
  BODY: {
    FOV: 0.5,
    SPEED: 5,
    ACCELERATION: 1,
    HEALTH: 100,
    SHIELD: 100,
    DAMAGE: 0,
    RESIST: 1,
    PENETRATION: 100,
    DENSITY: 0.4
  },
  AI: {
    BLIND: true
  },
  DRAW_HEALTH: false,
  CLEAR_ON_MASTER_UPGRADE: true,
  GIVE_KILL_MESSAGE: false,
  CONTROLLERS: ["hangOutNearMaster"],
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /***      LENGTH  WIDTH   ASPECT    X       Y       ANGLE   DELAY */
      POSITION: [10, 10, 1, 0, 15, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.spam]),
        TYPE: exports.bullet,
        AUTOFIRE: true
      }
    },
    {
      POSITION: [10, 10, 1, 0, 15, -90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.spam]),
        TYPE: exports.bullet,
        AUTOFIRE: true
      }
    }
  ]
};
exports.twinion = {
  PARENT: [exports.genericTank],
  LABEL: "Twin Minion",
  TYPE: "minion",
  DAMAGE_CLASS: 0,
  HITS_OWN_TYPE: "hardWithBuffer",
  FACING_TYPE: "smoothToTarget",
  BODY: {
    FOV: 0.5,
    SPEED: 3,
    ACCELERATION: 0.4,
    HEALTH: 5,
    SHIELD: 0,
    DAMAGE: 1.2,
    RESIST: 1,
    PENETRATION: 1,
    DENSITY: 0.4
  },
  AI: {
    BLIND: true
  },
  DRAW_HEALTH: false,
  CLEAR_ON_MASTER_UPGRADE: true,
  GIVE_KILL_MESSAGE: false,
  CONTROLLERS: [
    "nearestDifferentMaster",
    "mapAltToFire",
    "minion",
    "canRepel",
    "hangOutNearMaster"
  ],
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [17, 9, 1, 0, 4.5, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin]),
        WAIT_TO_CYCLE: true,
        TYPE: exports.bullet
      }
    },
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [17, 9, 1, 0, -4.5, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin]),
        WAIT_TO_CYCLE: true,
        TYPE: exports.bullet
      }
    }
  ]
};
exports.machminion = {
  PARENT: [exports.genericTank],
  LABEL: "Machine Minion",
  TYPE: "minion",
  DAMAGE_CLASS: 0,
  HITS_OWN_TYPE: "hardWithBuffer",
  FACING_TYPE: "smoothToTarget",
  BODY: {
    FOV: 0.5,
    SPEED: 3,
    ACCELERATION: 0.4,
    HEALTH: 5,
    SHIELD: 0,
    DAMAGE: 1.2,
    RESIST: 1,
    PENETRATION: 1,
    DENSITY: 0.4
  },
  AI: {
    BLIND: true
  },
  DRAW_HEALTH: false,
  CLEAR_ON_MASTER_UPGRADE: true,
  GIVE_KILL_MESSAGE: false,
  CONTROLLERS: [
    "nearestDifferentMaster",
    "mapAltToFire",
    "minion",
    "canRepel",
    "hangOutNearMaster"
  ],
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [17, 9, 1.3, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        WAIT_TO_CYCLE: true,
        TYPE: exports.bullet
      }
    }
  ]
};
exports.pillboxTurret = {
  PARENT: [exports.genericTank],
  LABEL: "",
  COLOR: 16,
  BODY: {
    FOV: 2
  },
  HAS_NO_RECOIL: true,
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 11, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.minion,
          g.turret,
          g.power,
          g.auto,
          g.notdense
        ]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.chain = {
  PARENT: [exports.genericTank],
  LABEL: "",
  COLOR: 16,
  BODY: {
    FOV: 2
  },
  HAS_NO_RECOIL: true,
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [0, 11, 1, 0, 0, 0, 0],
    }
  ]
};
exports.pillboxMachTurret = {
  PARENT: [exports.genericTank],
  LABEL: "",
  COLOR: 16,
  BODY: {
    FOV: 2
  },
  HAS_NO_RECOIL: true,
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 11, 1.5, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.minion,
          g.turret,
          g.power,
          g.auto,
          g.notdense,
          g.mach
        ]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.pillbox = {
  LABEL: "Pillbox",
  PARENT: [exports.trap],
  SHAPE: -4,
  MOTION_TYPE: "motor",
  CONTROLLERS: ["goToMasterTarget", "nearestDifferentMaster"],
  INDEPENDENT: true,
  BODY: {
    SPEED: 1,
    DENSITY: 5
  },
  DIE_AT_RANGE: true,
  TURRETS: [
    {
      /*  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, 0, 0, 0, 360, 1],
      TYPE: exports.pillboxTurret
    }
  ]
};
exports.skimturret = {
  PARENT: [exports.genericTank],
  BODY: {
    FOV: base.FOV * 2
  },
  COLOR: 2,
  CONTROLLERS: [
    "canRepel",
    "onlyAcceptInArc",
    "mapAltToFire",
    "nearestDifferentMaster"
  ],
  LABEL: "",
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [10, 14, -0.5, 9, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.pound,
          g.arty,
          g.arty,
          g.skim
        ]),
        TYPE: exports.hypermissile
      }
    },
    {
      POSITION: [17, 15, 1, 0, 0, 0, 0]
    }
  ]
};
exports.skimboss = {
  PARENT: [exports.genericTank],
  BODY: {
    HEALTH: 300,
    DAMAGE: 2,
    SHIELD: 200
  },
  SHAPE: 3,
  COLOR: 2,
  FACING_TYPE: "autospin",
  TURRETS: [
    {
      /*  SIZE     X       Y     ANGLE    ARC */
      POSITION: [15, 5, 0, 60, 170, 0],
      TYPE: exports.skimturret
    },
    {
      POSITION: [15, 5, 0, 180, 170, 0],
      TYPE: exports.skimturret
    },
    {
      POSITION: [15, 5, 0, 300, 170, 0],
      TYPE: exports.skimturret
    }
  ]
};

function makeAuto(type, name = -1, options = {}) {
  let turret = { type: exports.autoTurret, size: 10, independent: true };
  if (options.type != null) {
    turret.type = options.type;
  }
  if (options.size != null) {
    turret.size = options.size;
  }
  if (options.independent != null) {
    turret.independent = options.independent;
  }

  let output = JSON.parse(JSON.stringify(type));
  let autogun = {
    /*********  SIZE               X       Y     ANGLE    ARC */
    POSITION: [turret.size, 0, 0, 180, 360, 1],
    TYPE: [
      turret.type,
      {
        CONTROLLERS: ["nearestDifferentMaster"],
        INDEPENDENT: turret.independent
      }
    ]
  };
  if (type.GUNS != null) {
    output.GUNS = type.GUNS;
  }
  if (type.TURRETS == null) {
    output.TURRETS = [autogun];
  } else {
    output.TURRETS = [...type.TURRETS, autogun];
  }
  if (name == -1) {
    output.LABEL = "Auto-" + type.LABEL;
  } else {
    output.LABEL = name;
  }
  output.DANGER = type.DANGER + 1;
  return output;
}
function makeHybrid(type, name = -1) {
  let output = JSON.parse(JSON.stringify(type));
  let spawner = {
    /********* LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
    POSITION: [7, 12, 1.2, 8, 0, 180, 0],
    PROPERTIES: {
      SHOOT_SETTINGS: combineStats([g.drone, g.weak]),
      TYPE: [exports.drone, { INDEPENDENT: true }],
      AUTOFIRE: true,
      SYNCS_SKILLS: true,
      STAT_CALCULATOR: gunCalcNames.drone,
      WAIT_TO_CYCLE: false,
      MAX_CHILDREN: 3
    }
  };
  if (type.TURRETS != null) {
    output.TURRETS = type.TURRETS;
  }
  if (type.GUNS == null) {
    output.GUNS = [spawner];
  } else {
    output.GUNS = [...type.GUNS, spawner];
  }
  if (name == -1) {
    output.LABEL = "Hybrid " + type.LABEL;
  } else {
    output.LABEL = name;
  }
  return output;
}

exports.basic = {
  PARENT: [exports.genericTank],
  LABEL: "Basic",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic]),
        TYPE: exports.bullet,
        LABEL: "", // def
        STAT_CALCULATOR: 0, // def
        WAIT_TO_CYCLE: false, // def
        AUTOFIRE: false, // def
        SYNCS_SKILLS: false, // def
        MAX_CHILDREN: 0, // def
        ALT_FIRE: false, // def
        NEGATIVE_RECOIL: false // def
      }
    }
  ]
};
exports.hexmas = {
  PARENT: [exports.genericTank],
  LABEL: "Hex Master",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.doublereload]),
        TYPE: exports.hexbul,
        LABEL: "", // def
        STAT_CALCULATOR: 0, // def
        WAIT_TO_CYCLE: false, // def
        AUTOFIRE: false, // def
        SYNCS_SKILLS: false, // def
        MAX_CHILDREN: 0, // def
        ALT_FIRE: false, // def
        NEGATIVE_RECOIL: false // def
      }
    }
  ]
};
exports.bbasic = {
  PARENT: [exports.genericTank],
  LABEL: "Fire Basic",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic]),
        TYPE: exports.fire,
        LABEL: "", // def
        STAT_CALCULATOR: 0, // def
        WAIT_TO_CYCLE: false, // def
        AUTOFIRE: false, // def
        SYNCS_SKILLS: false, // def
        MAX_CHILDREN: 0, // def
        ALT_FIRE: false, // def
        NEGATIVE_RECOIL: false // def
      }
    }
  ]
};
exports.plbasic = {
  PARENT: [exports.genericTank],
  LABEL: "Plauge Basic",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic]),
        TYPE: exports.plaugebullet,
        LABEL: "", // def
        STAT_CALCULATOR: 0, // def
        WAIT_TO_CYCLE: false, // def
        AUTOFIRE: false, // def
        SYNCS_SKILLS: false, // def
        MAX_CHILDREN: 0, // def
        ALT_FIRE: false, // def
        NEGATIVE_RECOIL: false // def
      }
    }
  ]
};
exports.fbasic = {
  PARENT: [exports.genericTank],
  LABEL: "Freezing Basic",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic]),
        TYPE: exports.snowball,
        LABEL: "", // def
        STAT_CALCULATOR: 0, // def
        WAIT_TO_CYCLE: false, // def
        AUTOFIRE: false, // def
        SYNCS_SKILLS: false, // def
        MAX_CHILDREN: 0, // def
        ALT_FIRE: false, // def
        NEGATIVE_RECOIL: false // def
      }
    }
  ]
};
exports.laztest = {
  PARENT: [exports.genericTank],
  LABEL: "Lazer Test",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.nospray, g.noshudder, g.norecoil, g.doublesize, g.doublesize, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.lazerbeam,
        LABEL: "", // def
        STAT_CALCULATOR: 0, // def
        WAIT_TO_CYCLE: false, // def
        AUTOFIRE: false, // def
        SYNCS_SKILLS: false, // def
        MAX_CHILDREN: 0, // def
        ALT_FIRE: false, // def
        NEGATIVE_RECOIL: false // def
      }
    }
  ]
};
exports.smol = {
  PARENT: [exports.genericTank],
  LABEL: "Smollest Boi",
  SIZE: 0.01,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic]),
        TYPE: exports.bullet,
        LABEL: "", // def
        STAT_CALCULATOR: 0, // def
        WAIT_TO_CYCLE: false, // def
        AUTOFIRE: false, // def
        SYNCS_SKILLS: false, // def
        MAX_CHILDREN: 0, // def
        ALT_FIRE: false, // def
        NEGATIVE_RECOIL: false // def
      }
    }
  ]
};
exports.lance = {
  PARENT: [exports.genericTank],
  LABEL: "Lancer",
  
  BODY :{
    SPEED: base.SPEED * 2
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [10, 1, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.lancerblade]),
        TYPE: exports.bullet,
        LABEL: "Blade", // def
        STAT_CALCULATOR: 0, // def
        WAIT_TO_CYCLE: false, // def
        AUTOFIRE: true, // def
        SYNCS_SKILLS: false, // def`
        MAX_CHILDREN: 0, // def
        ALT_FIRE: false, // def
        NEGATIVE_RECOIL: false
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [30, 15, 0.05, 0, 0, 0, 0], }
  ],
};
exports.dagger = {
  PARENT: [exports.genericTank],
  LABEL: "Dagger",
  
  BODY :{
    SPEED: base.SPEED * 3
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [10, 1, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.lancerblade]),
        TYPE: exports.bullet,
        LABEL: "Blade", // def
        STAT_CALCULATOR: 0, // def
        WAIT_TO_CYCLE: false, // def
        AUTOFIRE: true, // def
        SYNCS_SKILLS: false, // def`
        MAX_CHILDREN: 0, // def
        ALT_FIRE: false, // def
        NEGATIVE_RECOIL: false
      }
    }, {
     
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [21, 15, 0.05, 0, 0, 0, 0],
    },{
        POSITION: [5, 7, -1.6, 8, 0, 0, 0]
    }
  ],
};
exports.shortsword = {
  PARENT: [exports.genericTank],
  LABEL: "Short Sword",
  
  BODY :{
    SPEED: base.SPEED * 4
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [10, 1, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.lancerblade]),
        TYPE: exports.bullet,
        LABEL: "Blade", // def
        STAT_CALCULATOR: 0, // def
        WAIT_TO_CYCLE: false, // def
        AUTOFIRE: true, // def
        SYNCS_SKILLS: false, // def`
        MAX_CHILDREN: 0, // def
        ALT_FIRE: false, // def
        NEGATIVE_RECOIL: false
      }
    }, {
     
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [21, 19, 0.05, 0, 0, 0, 0],
    },{
        POSITION: [5, 8, 1.6, 8, 0, 0, 0]
    }
  ],
};



exports.slance = {
  PARENT: [exports.genericTank],
  LABEL: " Speed Lancer",
  
  BODY :{
    SPEED: base.SPEED * 3
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [10, 1, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.lancerblade]),
        TYPE: exports.bullet,
        LABEL: "Blade", // def
        STAT_CALCULATOR: 0, // def
        WAIT_TO_CYCLE: false, // def
        AUTOFIRE: true, // def
        SYNCS_SKILLS: false, // def`
        MAX_CHILDREN: 0, // def
        ALT_FIRE: false, // def
        NEGATIVE_RECOIL: false
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [30, 15, 0.05, 0, 0, 0, 0], }
  ],
};
exports.jlance = {
  PARENT: [exports.genericTank],
  LABEL: " Jump Lancer",
  
  BODY :{
    SPEED: base.SPEED * 2
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [10, 1, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.lancerblade]),
        TYPE: exports.bullet,
        LABEL: "Blade", // def
        STAT_CALCULATOR: 0, // def
        WAIT_TO_CYCLE: false, // def
        AUTOFIRE: true, // def
        SYNCS_SKILLS: false, // def`
        MAX_CHILDREN: 0, // def
        ALT_FIRE: false, // def
        NEGATIVE_RECOIL: false
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [30, 15, 0.05, 0, 0, 0, 0], },
    {
    POSITION: [10, 1, 1, 0, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.jump]),
        TYPE: exports.bullet,
        ALT_FIRE: true, // de
      }
    }
  ],
   TURRETS: [
    {
      /** SIZE     X       Y     ANGLE    ARC */
      POSITION: [24.5, 0, 0, 0, 360, 0],
      TYPE: exports.jumpbody
    },
     ]
};
exports.VPNlance = {
  PARENT: [exports.genericTank],
  LABEL: "Griefer",
   INVISIBLE: [2, 1, 0],
  BODY :{
    SPEED: base.SPEED * 2
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [10, 1, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.lancerblade]),
        TYPE: exports.bullet,
        LABEL: "Blade", // def
        STAT_CALCULATOR: 0, // def
        WAIT_TO_CYCLE: false, // def
        SYNCS_SKILLS: false, // def`
        MAX_CHILDREN: 0, // def
        ALT_FIRE: false, // def
        NEGATIVE_RECOIL: false
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [30, 15, 0.05, 0, 0, 0, 0], }
  ],
};
exports.lancef = {
  PARENT: [exports.genericTank],
  LABEL: "Flank Lancer",
  FACING_TYPE: "crazyspin",
  BODY :{
    SPEED: base.SPEED * 2
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [10, 1, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.lancerblade]),
        TYPE: exports.bullet,
        LABEL: "Blade", // def
        STAT_CALCULATOR: 0, // def
        WAIT_TO_CYCLE: false, // def
        AUTOFIRE: true, // def
        SYNCS_SKILLS: false, // def`
        MAX_CHILDREN: 0, // def
        ALT_FIRE: false, // def
        NEGATIVE_RECOIL: false
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [30, 15, 0.05, 0, 0, 0, 0], },
     {
    POSITION: [10, 1, 1, 0, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.lancerblade]),
        TYPE: exports.bullet,
        LABEL: "Blade", // def
        STAT_CALCULATOR: 0, // def
        WAIT_TO_CYCLE: false, // def
        AUTOFIRE: true, // def
        SYNCS_SKILLS: false, // def`
        MAX_CHILDREN: 0, // def
        ALT_FIRE: false, // def
        NEGATIVE_RECOIL: false
         }
    }, {
      POSITION: [30, 15, 0.05, 0, 0, 180, 0], 
      },
    {
       POSITION: [10, 1, 1, 0, 0, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.lancerblade]),
        TYPE: exports.bullet,
        LABEL: "Blade", // def
        STAT_CALCULATOR: 0, // def
        WAIT_TO_CYCLE: false, // def
        AUTOFIRE: true, // def
        SYNCS_SKILLS: false, // def`
        MAX_CHILDREN: 0, // def
        ALT_FIRE: false, // def
        NEGATIVE_RECOIL: false
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [30, 15, 0.05, 0, 0, 90, 0], },
     {
    POSITION: [10, 1, 1, 0, 0, 270, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.lancerblade]),
        TYPE: exports.bullet,
        LABEL: "Blade", // def
        STAT_CALCULATOR: 0, // def
        WAIT_TO_CYCLE: false, // def
        AUTOFIRE: true, // def
        SYNCS_SKILLS: false, // def`
        MAX_CHILDREN: 0, // def
        ALT_FIRE: false, // def
        NEGATIVE_RECOIL: false
         }
    }, {
      POSITION: [30, 15, 0.05, 0, 0, 270, 0], 
    },
  ],
};
exports.sword = {
  PARENT: [exports.genericTank],
  LABEL: "Jouster",
  BODY :{
    SPEED: base.SPEED * 2,
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [10, 1, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.swordblade]),
        TYPE: exports.bullet,
        LABEL: "Blade", // def
        STAT_CALCULATOR: 0, // def
        WAIT_TO_CYCLE: false, // def
        AUTOFIRE: true, // def
        SYNCS_SKILLS: false, // def
        MAX_CHILDREN: 0, // def
        ALT_FIRE: false, // def
        NEGATIVE_RECOIL: false
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [36, 18, 0.05, 0, 0, 0, 0], 
    }
  ],
};

exports.longsword = {
  PARENT: [exports.genericTank],
  LABEL: "Long Sword",
  BODY :{
    SPEED: base.SPEED * 2,
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [10, 1, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.swordblade]),
        TYPE: exports.bullet,
        LABEL: "Blade", // def
        STAT_CALCULATOR: 0, // def
        WAIT_TO_CYCLE: false, // def
        AUTOFIRE: true, // def
        SYNCS_SKILLS: false, // def
        MAX_CHILDREN: 0, // def
        ALT_FIRE: false, // def
        NEGATIVE_RECOIL: false
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [40, 15, 0.05, 0, 0, 0, 0], 
    }
  ],
};
exports.hamm = {
  PARENT: [exports.genericTank],
  LABEL: "Hammer",
  BODY: {
    SPEED: base.SPEED * 2 
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [10, 10, 1, 0, -60, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.hammerbash, g.doublereload, g.halfrange]),
        TYPE: exports.bullet,
        LABEL: "Hammer", // def
        STAT_CALCULATOR: 0, // def
        WAIT_TO_CYCLE: false, // def
        AUTOFIRE: true, // def
        SYNCS_SKILLS: false, // def
        MAX_CHILDREN: 0, // def
        ALT_FIRE: false, // def
        NEGATIVE_RECOIL: false // def
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [10, 10, 1, 0, 60, 270, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.hammerbash, g.doublereload, g.halfrange]),
        TYPE: exports.bullet,
        LABEL: "Hammer", // def
        STAT_CALCULATOR: 0, // def
        WAIT_TO_CYCLE: false, // def
        AUTOFIRE: true, // def
        SYNCS_SKILLS: false, // def
        MAX_CHILDREN: 0, // def
        ALT_FIRE: false, // def
        NEGATIVE_RECOIL: false // def
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [10, 10, 1, 0, -70, 90, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.hammerbash, g.doublereload, g.halfrange]),
        TYPE: exports.bullet,
        LABEL: "Hammer", // def
        STAT_CALCULATOR: 0, // def
        WAIT_TO_CYCLE: false, // def
        AUTOFIRE: true, // def
        SYNCS_SKILLS: false, // def
        MAX_CHILDREN: 0, // def
        ALT_FIRE: false, // def
        NEGATIVE_RECOIL: false // def
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [10, 10, 1, 0, 70, 270, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.hammerbash, g.doublereload, g.halfrange]),
        TYPE: exports.bullet,
        LABEL: "Hammer", // def
        STAT_CALCULATOR: 0, // def
        WAIT_TO_CYCLE: false, // def
        AUTOFIRE: true, // def
        SYNCS_SKILLS: false, // def
        MAX_CHILDREN: 0, // def
        ALT_FIRE: false, // def
        NEGATIVE_RECOIL: false // def
      }
    },  {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [90, 10, 1, 0, 0, 0, 0], }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [30, 50, 1, 50, 0, 0, 0], },
  ]
};
exports.pbasic = {
  PARENT: [exports.genericTank],
  LABEL: "Poison Basic",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic]),
        TYPE: exports.snakespit,
        LABEL: "Poisoned Bullet", // def
        STAT_CALCULATOR: 0, // def
        WAIT_TO_CYCLE: false, // def
        AUTOFIRE: false, // def
        SYNCS_SKILLS: false, // def
        MAX_CHILDREN: 0, // def
        ALT_FIRE: false, // def
        NEGATIVE_RECOIL: false // def
      }
    },
  ]
};
exports.fbbasic = {
  PARENT: [exports.genericTank],
  LABEL: "Frostbite Basic",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic]),
        TYPE: exports.frostbiteball,
        STAT_CALCULATOR: 0, // def
        WAIT_TO_CYCLE: false, // def
        AUTOFIRE: false, // def
        SYNCS_SKILLS: false, // def
        MAX_CHILDREN: 0, // def
        ALT_FIRE: false, // def
        NEGATIVE_RECOIL: false // def
      }
    },
  ]
};
exports.frbrbasic = {
  PARENT: [exports.genericTank],
  LABEL: "Freezerburn Basic",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic]),
        TYPE: exports.snowfireball,
        STAT_CALCULATOR: 0, // def
        WAIT_TO_CYCLE: false, // def
        AUTOFIRE: false, // def
        SYNCS_SKILLS: false, // def
        MAX_CHILDREN: 0, // def
        ALT_FIRE: false, // def
        NEGATIVE_RECOIL: false // def
      }
    },
  ]
};
exports.mag = {
  PARENT: [exports.genericTank],
  LABEL: "Magnum",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.knock = {
  PARENT: [exports.genericTank],
  LABEL: "Knockback",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.knocker]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.ko = {
  PARENT: [exports.genericTank],
  LABEL: "KO",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.KO]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.observer = {
  PARENT: [exports.genericTank],
  LABEL: "Observer",
  BODY: {
    // def
    SHIELD: 999,
    REGEN: 999,
    HEALTH: 999,
    DAMAGE: 0,
    DENSITY: 999,
    FOV: 2,
    SPEED: 20
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH    ASPECT   X       Y      ANGLE    DELAY */
      POSITION: [14, 5, 2, 0, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.thruster,
          g.mach,
          g.mach,
          g.lotsmorrecoil
        ]),
        TYPE: exports.bullet
      }
    }
  ]
  //CONTROLLERS: ['nearestDifferentMaster'],
};
exports.pg2 = {
  PARENT: [exports.genericTank],
  LABEL: "PG-2",
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic]),
        TYPE: exports.bullet,
        LABEL: "", // def
        STAT_CALCULATOR: 0, // def
        WAIT_TO_CYCLE: false, // def
        AUTOFIRE: false, // def
        SYNCS_SKILLS: false, // def
        MAX_CHILDREN: 0, // def
        ALT_FIRE: false, // def
        NEGATIVE_RECOIL: false // def
      }
    }
  ]
};
exports.bosses = {
  PARENT: [exports.genericTank],
  LABEL: "Bosses"
};
exports.champ = {
  PARENT: [exports.genericTank],
  LABEL: "Champions"
};
exports.misc = {
  PARENT: [exports.genericTank],
  LABEL: "Miscellaneous",
SHAPE: "m -0.74832,-0.74832 a 1.05832,1.05832 0 0 1 1.15332,-0.229412 1.05832,1.05832 0 0 1 0.65332,0.97776 1.05832,1.05832 0 0 1 -0.65332,0.97776 1.05832,1.05832 0 0 1 -1.15332,-0.229412 l 0.74832,-0.74832 z",
};
exports.freetbtanks = {
  PARENT: [exports.genericTank],
  LABEL: "Free Testbed Tanks"
};
exports.tastetherainbow = makeAuto(exports.observer, "TASTE THE RAINBOW", {
  type: exports.TtRTurret,
  size: 11
});
exports.arenaclose = {
  PARENT: [exports.genericTank],
  LABEL: "Arena Closer",
  BODY: {
    // def
    SHIELD: 999,
    REGEN: 999,
    HEALTH: 999,
    DAMAGE: 999,
    DENSITY: 999,
    FOV: 10
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [13, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.arenaclose]),
        TYPE: exports.bullet,
        LABEL: "BAN BULLET", // def
        STAT_CALCULATOR: 0, // def
        WAIT_TO_CYCLE: false, // def
        AUTOFIRE: false, // def
        SYNCS_SKILLS: false, // def
        MAX_CHILDREN: 0, // def
        ALT_FIRE: false, // def
        NEGATIVE_RECOIL: false // def
      }
    }
  ]
};
exports.testbed = {
  PARENT: [exports.genericTank],
  LABEL: "TESTBED",
  RESET_UPGRADES: true,
  SKILL: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  LEVEL: -1,
  BODY: {
    // def
    SHIELD: 1000,
    REGEN: 10,
    HEALTH: 1000000000,
    DAMAGE: 10,
    DENSITY: 20,
    FOV: 2
  },
  SHAPE: [
    [-1, -0.8],
    [-0.8, -1],
    [0.8, -1],
    [1, -0.8],
    [0.2, 0],
    [1, 0.8],
    [0.8, 1],
    [-0.8, 1],
    [-1, 0.8]
  ],
  TURRETS: [],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 10, -1.4, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.mach, g.norecoil, g.op]),
        TYPE: [exports.flare1, { SHAPE: 5 }]
      }
    }
  ]
};
exports.single = {
  PARENT: [exports.genericTank],
  LABEL: "Single",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [19, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.single]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5.5, 8, -1.8, 6.5, 0, 0, 0]
    }
  ]
};

let smshskl = 12; //13;
exports.smash = {
  PARENT: [exports.genericTank],
  LABEL: "Smasher",
  DANGER: 6,
  BODY: {
    FOV: base.FOV * 1.05,
    DENSITY: base.DENSITY * 2
  },
  TURRETS: [
    {
      /** SIZE     X       Y     ANGLE    ARC */
      POSITION: [21.5, 0, 0, 0, 360, 0],
      TYPE: exports.smasherBody
    }
  ],
  IS_SMASHER: true,
  SKILL_CAP: [smshskl, 0, 0, 0, 0, smshskl, smshskl, smshskl, smshskl, smshskl],
  STAT_NAMES: statnames.smasher
};
exports.jumpsmash = {
  PARENT: [exports.genericTank],
LABEL:"Jump Smasher",
  DANGER: 6,
  BODY: {
    FOV: base.FOV * 1.05,
    SPEED: base.SPEED * 2,
    DENSITY: base.DENSITY * 2
  },
  TURRETS: [
    {
      /** SIZE     X       Y     ANGLE    ARC */
      POSITION: [21.5, 0, 0, 0, 360, 0],
      TYPE: exports.smasherBody
    },
    {
      POSITION: [22, 0, 0, 0, 360, 0],
      TYPE: exports.jumpbody
    }
  ],
  GUNS: [ {
  POSITION: [10, 1, 1, 0, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.jump]),
        TYPE: exports.bullet,
        ALT_FIRE:false, // de
      },
  }
    ],
  IS_SMASHER: true,
  SKILL_CAP: [smshskl, 0, 0, 0, 0, smshskl, smshskl, smshskl, smshskl, smshskl],
  STAT_NAMES: statnames.smasher
    
};
exports.macemain = {
  PARENT: [exports.genericTank],
LABEL:"Mace",
  DANGER: 6,
  TURRETS: [
    {
      /** SIZE     X       Y     ANGLE    ARC */
       POSITION: [18, 0, 0, 0, 360, 0],
      TYPE: exports.spikeBody
    },
    {
      POSITION: [18, 0, 0, 120, 360, 0],
      TYPE: exports.spikeBody
    },
    {
      POSITION: [18, 0, 0, 240, 360, 0],
      TYPE: exports.spikeBody
    },
  ],
  GUNS: [ {
  POSITION: [10,0, -1, 0, 0, 360, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mace]),
        TYPE: exports.bullet,
        ALT_FIRE:false, // de
      },
  }
    ],
  IS_SMASHER: true,
  SKILL_CAP: [smshskl, 0, 0, 0, 0, smshskl, smshskl, smshskl, smshskl, smshskl],
  STAT_NAMES: statnames.smasher
    
};
exports.microsmash = {
  PARENT: [exports.genericTank],
  LABEL: "Bonker",
  DANGER: 6,
  SIZE: 7,
  BODY: {
    FOV: base.FOV * 1.05,
    DENSITY: base.DENSITY * 2,
    SPEED: base.SPEED * 2
  },
  TURRETS: [
    {
      /** SIZE     X       Y     ANGLE    ARC */
      POSITION: [21.5, 0, 0, 0, 360, 0],
      TYPE: exports.spikeBody
    }
  ],
  IS_SMASHER: true,
  SKILL_CAP: [smshskl, 0, 0, 0, 0, smshskl, smshskl, smshskl, smshskl, smshskl],
  STAT_NAMES: statnames.smasher
};
exports.megasmash = {
  PARENT: [exports.genericTank],
  LABEL: "Mega-Smasher",
  DANGER: 7,
  BODY: {
    SPEED: base.speed * 1.05,
    FOV: base.FOV * 1.1,
    DENSITY: base.DENSITY * 4
  },
  IS_SMASHER: true,
  SKILL_CAP: [smshskl, 0, 0, 0, 0, smshskl, smshskl, smshskl, smshskl, smshskl],
  STAT_NAMES: statnames.smasher,
  TURRETS: [
    {
      /** SIZE     X       Y     ANGLE    ARC */
      POSITION: [24, 0, 0, 0, 360, 0],
      TYPE: exports.megasmashBody
    }
  ]
};
exports.spike = {
  PARENT: [exports.genericTank],
  LABEL: "Spike",
  DANGER: 7,
  BODY: {
    SPEED: base.speed * 0.9,
    DAMAGE: base.DAMAGE * 1.1,
    FOV: base.FOV * 1.05,
    DENSITY: base.DENSITY * 2
  },
  IS_SMASHER: true,
  SKILL_CAP: [smshskl, 0, 0, 0, 0, smshskl, smshskl, smshskl, smshskl, smshskl],
  STAT_NAMES: statnames.smasher,
  TURRETS: [
    {
      /** SIZE     X       Y     ANGLE    ARC */
      POSITION: [20.5, 0, 0, 0, 360, 0],
      TYPE: exports.spikeBody
    },
    {
      POSITION: [20.5, 0, 0, 120, 360, 0],
      TYPE: exports.spikeBody
    },
    {
      POSITION: [20.5, 0, 0, 240, 360, 0],
      TYPE: exports.spikeBody
    }
  ]
};
exports.weirdspike = {
  PARENT: [exports.genericTank],
  LABEL: "Wierd Spike",
  DANGER: 7,
  BODY: {
    DAMAGE: base.DAMAGE * 1.15,
    FOV: base.FOV * 1.05,
    DENSITY: base.DENSITY * 1.5
  },
  IS_SMASHER: true,
  SKILL_CAP: [smshskl, 0, 0, 0, 0, smshskl, smshskl, smshskl, smshskl, smshskl],
  STAT_NAMES: statnames.smasher,
  TURRETS: [
    {
      /** SIZE     X       Y     ANGLE    ARC */
      POSITION: [20.5, 0, 0, 0, 360, 0],
      TYPE: exports.spikeBody1
    },
    {
      POSITION: [20.5, 0, 0, 180, 360, 0],
      TYPE: exports.spikeBody2
    }
  ]
};
exports.autosmash = makeAuto(exports.smash, "Auto-Smasher", {
  type: exports.autoSmasherTurret,
  size: 11
});
exports.autosmash.SKILL_CAP = [
  smshskl,
  smshskl,
  smshskl,
  smshskl,
  smshskl,
  smshskl,
  smshskl,
  smshskl,
  smshskl,
  smshskl
];

exports.twin = {
  PARENT: [exports.genericTank],
  LABEL: "Twin",
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 8, 1, 0, 5.5, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin]),
        TYPE: exports.bullet
      }
    },
    {
      /* LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 8, 1, 0, -5.5, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.static = {
  PARENT: [exports.genericTank],
  LABEL: "Static",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron, g.slow]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.333],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron, g.slow]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.667],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron, g.slow]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [10.5, 8.5, -2, 2.5, 0, 180, 0],
    }
  ]
};
exports.explode = {
  PARENT: [exports.genericTank],
  LABEL: "Exploder",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.freeze, g.infihealth, g.halfreload, g.halfreload, g.halfreload]),
        TYPE: exports.bomb,
        ALT_FIRE: true
      }
    }
  ]
};
exports.shout = {
  PARENT: [exports.genericTank],
  LABEL: "Shout",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.infihealth, g.halfreload, g.halfreload, g.halfreload, g.doublesize, g.doublesize]),
        TYPE: exports.soundwave,
        ALT_FIRE: true
      }
    }
  ]
};
exports.yell = {
  PARENT: [exports.genericTank],
  LABEL: "Yell",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.halfreload, g.halfreload, g.halfreload, g.doublesize, g.doublesize]),
        TYPE: exports.soundwave,
        ALT_FIRE: true
      }
    }
  ]
};
exports.microphone = {
  PARENT: [exports.genericTank],
  LABEL: "Microphone",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.doubledamage, g.halfreload, g.halfreload, g.halfreload, g.doublesize, g.doublesize]),
        TYPE: exports.soundwave2,
        ALT_FIRE: true
      }
    }
  ]
};
exports.Singer = {
  PARENT: [exports.genericTank],
  LABEL: "Singer",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.halfreload, g.halfreload, g.halfreload, g.mach, g.doublesize, g.doubledamage]),
        TYPE: exports.soundwave2,
        ALT_FIRE: true
      }
    }
  ]
};
exports.megaphone = {
  PARENT: [exports.genericTank],
  LABEL: "Megaphone",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.doubledamage, g.halfreload, g.halfreload, g.halfreload, g.doublesize, g.doublesize, g.doubledamage]),
        TYPE: exports.soundwave3,
        ALT_FIRE: true
      }
    }
  ]
};
exports.scream = {
  PARENT: [exports.genericTank],
  LABEL: "Scream",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.halfdamage, g.halfreload, g.mach]),
        TYPE: exports.soundwave,
        ALT_FIRE: true
      }
    }
  ]
};
exports.kima = {
  PARENT: [exports.genericTank],
  LABEL: "Kimakaze",
  FRAG: 'Kimakaze',
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.freeze, g.infihealth, g.halfreload, g.halfreload]),
        TYPE: exports.bullet,
        ALT_FIRE: true,
      }
    }
  ]
};
exports.tsarbomba = {
  PARENT: [exports.genericTank],
  LABEL: "Tsar Bomba",
BODY:{
  HEALTH: 1,
  FOV: 10,
},
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.freeze, g.infihealth, g.halfreload, g.halfreload, g.halfreload]),
        TYPE: exports.tsar,
        ALT_FIRE: true
      }
    }
  ]
};
exports.uv = {
  PARENT: [exports.genericTank],
  LABEL: "UV Rays",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron, g.slow]),
        TYPE: exports.flare1
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.333],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron, g.slow]),
        TYPE: exports.flare1
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.667],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron, g.slow]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [13.5, 4.5, -2, 2.5, 0, 180, 0],
    }, {
      POSITION: [10.5, 8.5, -2, 2.5, 0, 180, 0],
    }
  ]
};
exports.solwin = {
  PARENT: [exports.genericTank],
  LABEL: "Solar Wind",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron2, g.slow, g.halfreload]),
        TYPE: exports.flare1
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.333],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron2, g.slow, g.halfreload]),
        TYPE: exports.flare1
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.667],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron2, g.slow, g.halfreload]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [13.5, 4.5, -2, 2.5, 0, 0, 0],
    }, {
      POSITION: [10.5, 8.5, -2, 2.5, 0, 0, 0],
    }
  ]
};
exports.infa = {
  PARENT: [exports.genericTank],
  LABEL: "Infared",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron, g.slow, g.doublereload]),
        TYPE: exports.flare1
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.333],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron, g.slow, g.doublereload]),
        TYPE: exports.flare1
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.667],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron, g.slow, g.doublereload]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [13.5, 4.5, -2, 2.5, 0, 180, 0],
    }, {
      POSITION: [10.5, 8.5, -2, 2.5, 0, 180, 0],
    }, {
      POSITION: [8.5, 6.5, -2, 2.5, 0, 180, 0],
    }
  ]
};
exports.gamray = {
  PARENT: [exports.genericTank],
  LABEL: "Gamma Ray",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron2, g.slow]),
        TYPE: exports.flare1
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.333],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron2, g.slow]),
        TYPE: exports.flare1
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.667],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron2, g.slow]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [13.5, 4.5, -2, 2.5, 0, 0, 0],
    }, {
      POSITION: [10.5, 8.5, -2, 2.5, 0, 0, 0],
    }, {
      POSITION: [8.5, 6.5, -2, 2.5, 0, 0, 0],
    }
  ]
};
exports.resistor = {
  PARENT: [exports.genericTank],
  LABEL: "Resistor",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 120, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 240, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron, g.slow]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.333],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron, g.slow]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.667],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron, g.slow]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [10.5, 8.5, -2, 2.5, 0, 180, 0],
    }
  ]
};
exports.thunder = {
  PARENT: [exports.genericTank],
  LABEL: "Thunder",
  BODY: {
   FOV: 1.25 
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.333],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.667],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [10.5, 8.5, -2, 2.5, 0, 180, 0],
    }
  ]
};
exports.thunderb = {
  PARENT: [exports.genericTank],
  LABEL: "Thunderbolt",
  BODY: {
   FOV: 1.75 
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [27.5, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.assass]),
        TYPE: exports.bullet
      }
    }, {
     POSITION: [17, 12, 0.5, 0, 0, 0, 0],
      },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.333],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.667],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [10.5, 8.5, -2, 2.5, 0, 180, 0],
    }
  ]
};
exports.kenetic = {
  PARENT: [exports.genericTank],
  LABEL: "Kenetic",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron, g.slow, g.mach]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron, g.slow, g.mach]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [10.5, 8.5, -2, 2.5, 0, 180, 0],
    }, {
      POSITION: [9, 7.5, -2, 2.5, 0, 180, 0],
    }
  ]
};
exports.electro = {
  PARENT: [exports.genericTank],
  LABEL: "Electro Field",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [24, 16, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.doubledamage, g.doubledamage, g.halfreload, g.halfreload]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron, g.doublereload, g.mach, g.doublereload, g.morespeed]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron, g.doublereload, g.mach, g.doublereload, g.morespeed]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron, g.doublereload, g.mach, g.doublereload, g.morespeed]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron, g.doublereload, g.mach, g.doublereload, g.morespeed]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.electron, g.halfreload, g.halfreload, g.halfreload, g.doublesize, g.doublesize]),
        TYPE: exports.megaelec,
        MAX_CHILDREN: 10
      }
    }, 
  ]
};
exports.discharge = {
  PARENT: [exports.genericTank],
  LABEL: "Discharger",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.electron,
          g.slow,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.electron,
          g.slow,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.electron,
          g.slow,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.electron,
          g.slow,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.electron,
          g.slow,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.electron,
          g.slow,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.electron,
          g.slow,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.electron,
          g.slow,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.electron,
          g.slow,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.electron,
          g.slow,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.electron,
          g.slow,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.electron,
          g.slow,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.electron,
          g.slow,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.electron,
          g.slow,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.electron,
          g.slow,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.electron,
          g.slow,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.electron,
          g.slow,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.electron,
          g.slow,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 5, 1, -10, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.electron,
          g.slow,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [12.5, 10.5, -1.5, 2.5, 0, 180, 0],
    }, {
      POSITION: [10.5, 8.5, -2, 2.5, 0, 180, 0],
    }, {
      POSITION: [8.5, 6.5, -2, 2.5, 0, 180, 0],
    }
  ]
};
exports.Sawshot = {
  PARENT: [exports.genericTank],
  LABEL: "Saw shot",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 12, 1.3, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.halfreload, g.halfreload]),
        TYPE: exports.saw,
        LABEL: "Flung Saw"
      }
    }
  ]
};
exports.discgun = {
  PARENT: [exports.genericTank],
  LABEL: "Disc Launcher",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 10, 1.15, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.halfreload, g.halfreload]),
        TYPE: exports.disc,
        LABEL: "Flung Disc"
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 10, 1.15, 0, 0, 0, 0.33],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.halfreload, g.halfreload]),
        TYPE: exports.disc,
        LABEL: "Flung Disc"
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [16, 10, 1.15, 0, 0, 0, 0.67],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.halfreload, g.halfreload, g.doubledamage]),
        TYPE: exports.disc,
        LABEL: "Flung Disc"
      }
    }
  ]
};
exports.sawblaz = {
  PARENT: [exports.genericTank],
  LABEL: "Saws Blazing",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 12, 1.6, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.halfreload, g.halfreload, g.mach]),
        TYPE: exports.saw,
        LABEL: "Flung Saw"
      }
    }
  ]
};
exports.bruisers = {
  PARENT: [exports.genericTank],
  LABEL: "Bruiser Saw",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 12, 1.3, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.halfreload, g.halfreload]),
        TYPE: exports.bruiser,
        LABEL: "Flung Bruiser"
      }
    }
  ]
};
exports.bashers = {
  PARENT: [exports.genericTank],
  LABEL: "Bashing Saw",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 12, 1.3, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.halfreload, g.halfreload]),
        TYPE: exports.basher,
        LABEL: "Flung Basher"
      }
    }
  ]
};
exports.burners = {
  PARENT: [exports.genericTank],
  LABEL: "Burner Saw",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 12, 1.3, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.halfreload, g.halfreload]),
        TYPE: exports.burn,
        LABEL: "Burning Saw"
      }
    }
  ]
};
exports.buzcan = {
  PARENT: [exports.genericTank],
  LABEL: "Buzzsaw cannon",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [30, 12, 1.7, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.halfreload,
          g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.bsaw,
        LABEL: "Flung Buzzsaw"
      }
    }
  ]
}; exports.shredd = {
  PARENT: [exports.genericTank],
  LABEL: "Shredder Deployer",
  //CONTROLLERS: ['nearestDifferentMaster'],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [30, 12, 1.7, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.halfreload,
          g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.shred,
        LABEL: "Deployed Shredder"
      }
    }
  ]
};
exports.gunner = {
  PARENT: [exports.genericTank],
  LABEL: "Gunner",
  DANGER: 6,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [12, 3.5, 1, 0, 7.25, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [12, 3.5, 1, 0, -7.25, 0, 0.75],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [16, 3.5, 1, 0, 3.75, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [16, 3.5, 1, 0, -3.75, 0, 0.25],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.pellet = {
  PARENT: [exports.genericTank],
  LABEL: "Pelleter",
  DANGER: 3,
  GUNS: [
    {
      POSITION: [16, 3.5, 1, 0, 3.75, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [16, 3.5, 1, 0, -3.75, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.heli = {
  PARENT: [exports.genericTank],
  LABEL: "Heli Pilot",
  DANGER: 3,
  BODY: {
    SPEED: 10
  },
  GUNS: [
    {
      POSITION: [16, 3.5, 1, 0, 3.75, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [16, 3.5, 1, 0, -3.75, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [32, 7, 1, 0, 0, 180, 0],
    },
  ],
  TURRETS: [
    {
      /*********  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, 0, 0, 45, 360, 1],
      TYPE: [exports.helirotor]
    }, {
      /*********  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, -20, 20, 45, 360, 1],
      TYPE: [exports.reverserotor]
    }, 
  ]
};
exports.cheli = {
  PARENT: [exports.genericTank],
  LABEL: "Commanche Defense",
  DANGER: 10,
  BODY: {
    SPEED: 13
  },
  GUNS: [
    {
      POSITION: [16, 3.5, 1, 0, 3.75, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [1, 6, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload]),
        TYPE: exports.helimin,
        ALT_FIRE: true
      }
    }, {
      POSITION: [1, 6, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload]),
        TYPE: exports.helimin,
        ALT_FIRE: true
      }
    }, {
      POSITION: [1, 6, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload]),
        TYPE: exports.helimin,
        ALT_FIRE: true
      }
    },
    {
      POSITION: [16, 3.5, 1, 0, -3.75, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [32, 7, 1, 0, 0, 180, 0],
    }, {
      POSITION: [16, 12, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray, g.KO]),
        TYPE: exports.bullet
      }
    },
  ],
  TURRETS: [
    {
      /*********  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, 0, 0, 45, 360, 1],
      TYPE: [exports.helirotor]
    }, {
      /*********  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, -20, 20, 45, 360, 1],
      TYPE: [exports.reverserotor]
    }, 
  ]
};
exports.comcom = {
  PARENT: [exports.genericTank],
  LABEL: "Commanche Commander",
  DANGER: 15,
  BODY: {
    SPEED: 15
  },
  GUNS: [
    {
      POSITION: [16, 3.5, 1, 0, 3.75, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [1, 6, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload]),
        TYPE: exports.permhelimin,
        ALT_FIRE: true,
        MAX_CHILDREN: 1
      }
    }, {
      POSITION: [1, 6, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload]),
        TYPE: exports.permhelimin,
        ALT_FIRE: true,
        MAX_CHILDREN: 1
      }
    }, {
      POSITION: [1, 6, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload]),
        TYPE: exports.permhelimin,
        ALT_FIRE: true,
        MAX_CHILDREN: 1
      }
    },
    {
      POSITION: [16, 3.5, 1, 0, -3.75, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [32, 7, 1, 0, 0, 180, 0],
    }, {
      POSITION: [14, 12, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray, g.KO]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [18, 3, 0.9, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.autoexswarm
      }
    }, 
  ],
  TURRETS: [
    {
      /*********  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, 0, 0, 45, 360, 1],
      TYPE: [exports.helirotor]
    }, {
      /*********  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, -20, 20, 45, 360, 1],
      TYPE: [exports.reverserotor]
    }, 
  ]
};
exports.chinheli = {
  PARENT: [exports.genericTank],
  LABEL: "Support Chinook",
  SIZE: 20,
  DANGER: 5,
  SHAPE: [[-1,-1],[1.513,-1.004],[2.09,-0.624],[2.09,0.62],[1.53,1.01],[-1,1],[-1.7,0.75],[-1.71,-0.73]],
  HEALTH: 10,
  BODY: {
    SPEED: 7
  },
  GUNS:[
    {
      POSITION: [32, 3.5, 1, 0, 3.75, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.mach, g.fast, g.fast, g.fast, g.halfdamage, g.noshudder, g.nospray]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [32, 3.5, 1, 0, -3.75, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.mach, g.fast, g.fast, g.fast, g.halfdamage, g.noshudder, g.nospray]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [5, 10, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.halfreload, g.halfreload, g.freeze, g.fast, g.fast, g.halfdamage, g.noshudder, g.nospray]),
        TYPE: exports.healcrate,
        ALT_FIRE: true,
      }
    },
  ],
  TURRETS: [
    {
      /*********  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, 10, -10, 45, 360, 1],
      TYPE: [exports.chinrotor1]
    }, {
      /*********  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, -5, 5, 45, 360, 1],
      TYPE: [exports.chinrotor2]
    }, 
  ]
};
exports.specoper = {
  PARENT: [exports.genericTank],
  LABEL: "Special Operations",
  SIZE: 35,
  DANGER: 5,
  SHAPE: [[-1,-1],[1.513,-1.004],[2.09,-0.624],[2.09,0.62],[1.53,1.01],[-1,1],[-1.7,0.75],[-1.71,-0.73]],
  HEALTH: 10,
  BODY: {
    SPEED: 7
  },
  GUNS:[
    {
      POSITION: [32, 3.5, 1, 0, 3.75, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.mach, g.fast, g.fast, g.fast, g.halfdamage, g.noshudder, g.nospray]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [32, 3.5, 1, 0, -3.75, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.mach, g.fast, g.fast, g.fast, g.halfdamage, g.noshudder, g.nospray]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [5, 10, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload]),
        TYPE: exports.marine,
        ALT_FIRE: true,
      }
    },
  ],
  TURRETS: [
    {
      /*********  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, 10, -10, 45, 360, 1],
      TYPE: [exports.chinrotor1]
    }, {
      /*********  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, -5, 5, 45, 360, 1],
      TYPE: [exports.chinrotor2]
    }, 
  ]
};
exports.dguns = {
  PARENT: [exports.genericTank],
  LABEL: "Dlux Guns",
  DANGER: 7,
  BODY: {
    SPEED: 10
  },
  GUNS: [
    {
      POSITION: [16, 12, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray, g.KO]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [32, 7, 1, 0, 0, 180, 0],
    },
  ],
  TURRETS: [
    {
      /*********  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, 0, 0, 45, 360, 1],
      TYPE: [exports.helirotor]
    }, {
      /*********  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, -20, 20, 45, 360, 1],
      TYPE: [exports.reverserotor]
    }, 
  ]
};
exports.aheli = {
  PARENT: [exports.genericTank],
  LABEL: "Attack Heli",
  DANGER: 7,
  BODY: {
    SPEED: 10
  },
  GUNS: [
    {
      POSITION: [16, 3, 1, -5, 8, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [16, 3, 1, -5, -8, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [16, 3, 1, 0, 4, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [16, 3, 1, 0, -4, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [32, 7, 1, 0, 0, 180, 0],
    },
  ],
  TURRETS: [
    {
      /*********  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, 0, 0, 45, 360, 1],
      TYPE: [exports.helirotor]
    }, {
      /*********  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, -20, 20, 45, 360, 1],
      TYPE: [exports.reverserotor]
    }, 
  ]
};
exports.apacheheli = {
  PARENT: [exports.genericTank],
  LABEL: "Apache Helicopter",
  DANGER: 10,
  BODY: {
    SPEED: 10
  },
  GUNS: [
    {
      POSITION: [16, 3, 1, -5, -8, -45, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.autoexswarm
      }
    }, {
      POSITION: [16, 3, 1, 0, -4, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.bullet
      }
    },  {
      POSITION: [16, 3, 1, -5, 8, 45, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.autoexswarm
      }
    }, 
    {
      POSITION: [16, 3, 1, 0, 4, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [16, 3, 1.5, 5, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.doublereload, g.doublereload, g.halfdamage, g.halfdamage, g.halfspray, g.norecoil, g.halfspray]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [32, 7, 1, 0, 0, 180, 0],
    },
  ],
  TURRETS: [
    {
      /*********  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, 0, 0, 45, 360, 1],
      TYPE: [exports.helirotor]
    }, {
      /*********  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, -20, 20, 45, 360, 1],
      TYPE: [exports.reverserotor]
    }, 
  ]
};
exports.apacheauto = {
  PARENT: [exports.genericTank],
  LABEL: "Apache TEST",
  DANGER: 10,
  BODY: {
    SPEED: 10
  },
  GUNS: [
    {
      POSITION: [16, 3, 1, 0, -4, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.bullet
      }
    }, 
    {
      POSITION: [16, 3, 1, 0, 4, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.bullet
        }
    }, 
    {
      POSITION: [16, 3, 1, 0, 4, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.bullet
        }
    }, 
    {
      POSITION: [16, 3, 1, 0, 4, 270, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.auto3
      }
    }, {
      POSITION: [32, 7, 1, 0, 0, 180, 0],
    },
  ],
  TURRETS: [
    {
      /*********  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, 0, 0, 45, 360, 1],
      TYPE: [exports.helirotor]
    }, {
      /*********  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, -20, 20, 45, 360, 1],
      TYPE: [exports.reverserotor]
    }, 
  ]
};
exports.apacheprheli = {
  PARENT: [exports.genericTank],
  LABEL: "Apache Prime",
  DANGER: 10,
  BODY: {
    SPEED: 20,
    FOV: 5
  },
  GUNS: [
    {
      POSITION: [16, 3, 1, -5, -10, -45, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.autoexswarm
      }
    }, {
      POSITION: [16, 3, 1, -5, -8, -45, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.autoexswarm
      }
    }, {
      POSITION: [13, 3, 1, 0, -6, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [16, 3, 1, 0, -4, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.bullet
      }
    },  {
      POSITION: [16, 3, 1, -5, 10, 45, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.autoexswarm
      }
    }, {
      POSITION: [16, 3, 1, -5, 8, 45, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.autoexswarm
      }
    }, 
    {
      POSITION: [13, 3, 1, 0, 6, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [16, 3, 1, 0, 4, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.bullet
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 4.25, 0.8, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.doublereload, g.doublereload, g.doublereload, g.doublereload, g.norecoil]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [15, 5, 0.85, 0, 0, 0, 0]
    }, {
      POSITION: [32, 7, 1, 0, 0, 180, 0],
    },
  ],
  TURRETS: [
    {
      /*********  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, 0, 0, 45, 360, 1],
      TYPE: [exports.helirotor]
    }, {
      /*********  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, -20, 20, 45, 360, 1],
      TYPE: [exports.reverserotor]
    }, 
  ]
};
exports.downh = {
  PARENT: [exports.genericTank],
  LABEL: "Dlux Rotors",
  DANGER: 3,
  BODY: {
    SPEED: 11,
    HEALTH: 10
  },
  GUNS: [
    {
      POSITION: [16, 3.5, 1, 0, 3.75, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [16, 3.5, 1, 0, -3.75, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.puregunner, g.fast, g.fast, g.fast, g.fast, g.noshudder, g.nospray]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [32, 7, 1, 0, 0, 180, 0],
    },
  ],
  TURRETS: [
    {
      /*********  SIZE     X       Y     ANGLE    ARC */
      POSITION: [16, 0, 0, 45, 360, 1],
      TYPE: [exports.helirotor]
    }, {
      /*********  SIZE     X       Y     ANGLE    ARC */
      POSITION: [16, -20, 20, 45, 360, 1],
      TYPE: [exports.reverserotor]
    }, 
  ]
};
exports.submach = {
  PARENT: [exports.genericTank],
  LABEL: "Submachine",
  DANGER: 4,
  GUNS: [
    {
      POSITION: [25, 2, 1, 0, 3.75, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.fast,
          g.doublereload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [25, 2, 1, 0, -3.75, 0, 0.333],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.fast,
          g.doublereload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [25, 2, 1, 0, 0, 0, 0.667],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.fast,
          g.doublereload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 12, 1, 15, 0, 0, 0]
    }
  ]
};
exports.vulcan = {
  PARENT: [exports.genericTank],
  LABEL: "Vulcan",
  DANGER: 4,
  GUNS: [
    {
      POSITION: [25, 2, 1, 0, 1, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.fast,
          g.doublereload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [25, 2, 1, 0, 2, 0, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.fast,
          g.doublereload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [25, 2, 1, 0, 3, 0, 0.2],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.fast,
          g.doublereload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [25, 2, 1, 0, 4, 0, 0.3],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.fast,
          g.doublereload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [25, 2, 1, 0, 0, 0, 0.4],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.fast,
          g.doublereload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [25, 2, 1, 0, -1, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.fast,
          g.doublereload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [25, 2, 1, 0, -2, 0, 0.6],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.fast,
          g.doublereload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [25, 2, 1, 0, -3, 0, 0.7],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.fast,
          g.doublereload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [25, 2, 1, 0, -4, 0, 0.8],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.fast,
          g.doublereload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [25, 2, 1, 0, 0, 0, 0.9],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.fast,
          g.doublereload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 12, 1, 15, 0, 0, 0]
    }
  ]
};
exports.heavy = {
  PARENT: [exports.genericTank],
  LABEL: "Heavy",
  DANGER: 10,
  HEALTH: 300,
  GUNS: [
    {
      POSITION: [25, 2, 1, 4, 1, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.fast,
          g.fast,
          g.fast,
          g.fast,
          g.doublereload
        ]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [25, 2, 1, 3, 1, 0, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.fast,
          g.fast,
          g.fast,
          g.fast,
          g.doublereload
        ]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [25, 2, 1, 2, 1, 0, 0.2],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.fast,
          g.fast,
          g.fast,
          g.fast,
          g.doublereload
        ]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [25, 2, 1, 1, 1, 0, 0.3],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.fast,
          g.fast,
          g.fast,
          g.fast,
          g.doublereload
        ]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [25, 2, 1, -4, 1, 0, 0.4],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.fast,
          g.fast,
          g.fast,
          g.fast,
          g.doublereload
        ]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [25, 2, 1, -3, 1, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.fast,
          g.fast,
          g.fast,
          g.fast,
          g.doublereload
        ]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [25, 2, 1, -2, 1, 0, 0.6],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.fast,
          g.fast,
          g.fast,
          g.fast,
          g.doublereload
        ]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [25, 2, 1, -1, 1, 0, 0.7],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.fast,
          g.fast,
          g.fast,
          g.fast,
          g.doublereload
        ]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [25, 2, 1, 5, 1, 0, 0.8],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.fast,
          g.fast,
          g.fast,
          g.fast,
          g.doublereload
        ]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [25, 2, 1, -5, 1, 0, 0.9],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.fast,
          g.fast,
          g.fast,
          g.fast,
          g.doublereload
        ]),
        TYPE: exports.bullet
      }
    }, 
    {
      POSITION: [25, 2, 1, 0, 1, 0, 0.9],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.fast,
          g.fast,
          g.fast,
          g.fast,
          g.doublereload
        ]),
        TYPE: exports.bullet
      }
    }, 
    {
      POSITION: [5, 12, 1, 15, 0, 0, 0]
    }
  ]
};
exports.machinegunner = {
  PARENT: [exports.genericTank],
  LABEL: "Machine Gunner",
  DANGER: 6,
  BODY: {
    SPEED: base.SPEED * 0.9
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [14, 3, 4.0, -3, 5, 0, 0.6],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.machgun
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [14, 3, 4.0, -3, -5, 0, 0.8],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.machgun
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [14, 3, 4.0, 0, 2.5, 0, 0.4],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.machgun
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [14, 3, 4.0, 0, -2.5, 0, 0.2],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.machgun
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [14, 3, 4.0, 3, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.puregunner,
          g.machgun
        ]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.autogunner = makeAuto(exports.gunner);
exports.nailgun = {
  PARENT: [exports.genericTank],
  LABEL: "M-60.4",
  DANGER: 7,
  BODY: {
    FOV: base.FOV * 1.1,
    SPEED: base.SPEED * 0.9
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [25, 2, 1, 0, 0, 0, 0.25],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.power,
          g.twin,
          g.nail
        ]),
        TYPE: exports.bullet

      }
    },
    {
      POSITION: [25, 2, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.power,
          g.twin,
          g.nail
        ]),
        TYPE: exports.bullet
             }
    },
    {
      POSITION: [25, 2, 1, 0, 0, 0, 0.75],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.power,
          g.twin,
          g.nail
        ]),
        TYPE: exports.bullet 
              }
    },
    {
      POSITION: [25, 2, 1, 0, 0, 0, 1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.power,
          g.twin,
          g.nail
        ]),
        TYPE: exports.bullet
      },    
    },
    {
      POSITION: [25, 2, 1, 0, 0, 0, 0.50],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.power,
          g.twin,
          g.nail
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [14, 8, 0, 6.5, 0, 0, 0]
    },
    {
      POSITION: [12, 8, 0, 6.5, 0, 0, 0] 
    },
    {
       POSITION: [10, 8, 0, 6.5, 0, 0, 0]
    },
    {
      POSITION: [14
                 , 8, 1.3, 0, 0, 0, 0]
    },
    {
      POSITION: [5.5, 8, -1.8, 6.5, 0, 0, 0]
    },
    {
      POSITION: [14, 2, 1, -5.5, 0, 0, 0.25],
    },
    {
      POSITION: [14, 2, 1, 0, -7, 0, 0.25],
    },
    {
      POSITION: [14, 2, 1, 0, 7, 0, 0.25],
    }
  ]
};

exports.double = {
  PARENT: [exports.genericTank],
  LABEL: "Double Twin",
  DANGER: 6,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 8, 1, 0, 5.5, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.double]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [20, 8, 1, 0, -5.5, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.double]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [20, 8, 1, 0, 5.5, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.double]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [20, 8, 1, 0, -5.5, 180, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.double]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.tripletwin = {
  PARENT: [exports.genericTank],
  LABEL: "Triple Twin",
  DANGER: 7,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 8, 1, 0, 5.5, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.spam, g.double]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [20, 8, 1, 0, -5.5, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.spam, g.double]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [20, 8, 1, 0, 5.5, 120, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.spam, g.double]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [20, 8, 1, 0, -5.5, 120, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.spam, g.double]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [20, 8, 1, 0, 5.5, 240, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.spam, g.double]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [20, 8, 1, 0, -5.5, 240, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.spam, g.double]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.autodouble = makeAuto(exports.double, "Auto-Double");
exports.split = {
  PARENT: [exports.genericTank],
  LABEL: "Hewn Double",
  DANGER: 7,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [19, 8, 1, 0, 5.5, 25, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.twin,
          g.double,
          g.hewn,
          g.morerecoil
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [19, 8, 1, 0, -5.5, -25, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.twin,
          g.double,
          g.hewn,
          g.morerecoil
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [20, 8, 1, 0, 5.5, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.double,
          g.hewn,
          g.morerecoil
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [20, 8, 1, 0, -5.5, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.twin,
          g.double,
          g.hewn,
          g.morerecoil
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [20, 8, 1, 0, 5.5, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.double, g.hewn]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [20, 8, 1, 0, -5.5, 180, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.double, g.hewn]),
        TYPE: exports.bullet
      }
    }
  ]
};

exports.bent = {
  PARENT: [exports.genericTank],
  LABEL: "Triple Shot",
  DANGER: 6,
  BODY: {
    SPEED: base.SPEED * 0.9
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [19, 8, 1, 0, -2, -20, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.bent]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [19, 8, 1, 0, 2, 20, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.bent]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [22, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.bent]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.bentdouble = {
  PARENT: [exports.genericTank],
  LABEL: "Bent Double",
  DANGER: 7,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [19, 8, 1, 0, -1, -25, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.bent, g.double]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [19, 8, 1, 0, 1, 25, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.bent, g.double]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [22, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.bent, g.double]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [19, 8, 1, 0, -1, 155, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.bent, g.double]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [19, 8, 1, 0, 1, -155, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.bent, g.double]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [22, 8, 1, 0, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.bent, g.double]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.penta = {
  PARENT: [exports.genericTank],
  LABEL: "Penta Shot",
  DANGER: 7,
  BODY: {
    SPEED: base.SPEED * 0.85
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [16, 8, 1, 0, -3, -30, 0.667],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.bent]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [16, 8, 1, 0, 3, 30, 0.667],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.bent]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [19, 8, 1, 0, -2, -15, 0.333],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.bent]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [19, 8, 1, 0, 2, 15, 0.333],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.bent]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [22, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.bent]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.benthybrid = makeHybrid(exports.bent, "Bent Hybrid");

exports.triple = {
  PARENT: [exports.genericTank],
  DANGER: 6,
  BODY: {
    FOV: base.FOV * 1.05
  },
  LABEL: "Triplet",
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 10, 1, 0, 5, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.triple]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 10, 1, 0, -5, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.triple]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [21, 10, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.triple]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.battery = {
  PARENT: [exports.genericTank],
  LABEL: "Battery",
  DANGER: 6,
  BODY: {
    FOV: 1.2
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 5, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.pound,
          g.doublereload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 8, 1, 0, -5, 0, 0.333],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.pound,
          g.doublereload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [22, 8, 1, 0, 0, 0, 0.667],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.pound,
          g.doublereload
        ]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.quint = {
  PARENT: [exports.genericTank],
  DANGER: 7,
  BODY: {
    FOV: base.FOV * 1.1
  },
  LABEL: "Quintuplet",
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [16, 10, 1, 0, -5, 0, 0.667],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.triple, g.quint]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [16, 10, 1, 0, 5, 0, 0.667],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.triple, g.quint]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [19, 10, 1, 0, -3, 0, 0.333],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.triple, g.quint]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [19, 10, 1, 0, 3, 0, 0.333],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.triple, g.quint]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [22, 10, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.triple, g.quint]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.dual = {
  PARENT: [exports.genericTank],
  DANGER: 7,
  BODY: {
    ACCEL: base.ACCEL * 0.8,
    FOV: base.FOV * 1.1
  },
  LABEL: "Dual",
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 7, 1, 0, 5.5, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.dual, g.lowpower]),
        TYPE: exports.bullet,
        LABEL: "Small"
      }
    },
    {
      POSITION: [18, 7, 1, 0, -5.5, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.dual, g.lowpower]),
        TYPE: exports.bullet,
        LABEL: "Small"
      }
    },
    {
      POSITION: [16, 8.5, 1, 0, 5.5, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.dual]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [16, 8.5, 1, 0, -5.5, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.dual]),
        TYPE: exports.bullet
      }
    }
  ]
};

exports.sniper = {
  PARENT: [exports.genericTank],
  LABEL: "Sniper",
  BODY: {
    ACCELERATION: base.ACCEL * 0.7,
    FOV: base.FOV * 1.2
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [24, 8.5, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.incog = {
  PARENT: [exports.genericTank],
  LABEL: "Incognito",
  DANGER: 0,
  HEALTH: 20,
  BODY: {
    ACCELERATION: base.ACCEL * 0.7,
    FOV: base.FOV * 1.2
  },
  INVISIBLE: [2, 1, 0.3],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [24, 8.5, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.vpn = {
  PARENT: [exports.genericTank],
  LABEL: "VPN",
  DANGER: 0,
  HEALTH: 1,
  BODY: {
    ACCELERATION: base.ACCEL * 0.7,
    FOV: base.FOV * 1.2
  },
  INVISIBLE: [2, 1, 0],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [24, 8.5, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.fbeam = {
  PARENT: [exports.genericTank],
  LABEL: "Focused Beam",
  BODY: {
    ACCELERATION: base.ACCEL * 0.7,
    FOV: base.FOV * 2
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [24, 8.5, 0.8, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.focusedbeam]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [20, 10, 0.85, 0, 0, 0, 0]
    }
  ]
};
exports.bigberth = {
  PARENT: [exports.genericTank],
  LABEL: "Plasma Rifle",
  BODY: {
    ACCELERATION: base.ACCEL * 0.25,
    FOV: base.FOV * 2
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [24, 14, 0.8, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.fast, g.fast, g.bigbertha, g.halfdamage, g.halfdamage, g.halfdamage, g.halfdamage, g.mach, g.doublereload, g.doublereload]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [20, 16.5, 0.85, 0, 0, 0, 0]
    }
  ]
};
exports.lazera = {
  PARENT: [exports.genericTank],
  LABEL: "Lazer",
  BODY: {
    ACCELERATION: base.ACCEL * 0.7,
    FOV: base.FOV * 1.5
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 8.5, 0.8, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.lazer]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [16, 10, 0.85, 0, 0, 0, 0]
    }
  ]
};
exports.caval = {
  PARENT: [exports.genericTank],
  LABEL: "Cavalry",
  BODY: {
    ACCELERATION: base.ACCEL * 0.7,
    FOV: base.FOV * 1.5
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 8.5, 0.8, 0, 4.25, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.lazer]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [20, 8.5, 0.8, 0, -4.25, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.lazer]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [16, 10, 0.85, 0, 5, 0, 0]
    },
    {
      POSITION: [16, 10, 0.85, 0, -5, 0, 0]
    }
  ]
};
exports.twinbeam = {
  PARENT: [exports.genericTank],
  LABEL: "Twin Beams",
  BODY: {
    ACCELERATION: base.ACCEL * 0.7,
    FOV: base.FOV * 2
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [24, 8.5, 0.8, 0, 4.25, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.focusedbeam]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [24, 8.5, 0.8, 0, -4.25, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.focusedbeam]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [20, 10, 0.85, 0, 5, 0, 0]
    },
    {
      POSITION: [20, 10, 0.85, 0, -5, 0, 0]
    }
  ]
};
exports.atomi = {
  PARENT: [exports.genericTank],
  LABEL: "Atomizer",
  BODY: {
    ACCELERATION: base.ACCEL * 0.7,
    FOV: base.FOV * 2.5
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 8.5, 0.8, 0, 0, 6, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.lazer, g.doubledamage, g.doubledamage]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [20, 8.5, 0.8, 0, 0, -6, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.lazer, g.doubledamage, g.doubledamage]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [16, 10, 0.85, 0, 0, 6, 0]
    },
    {
      POSITION: [16, 10, 0.85, 0, 0, -6, 0]
    },
    {
      POSITION: [22.5, 8.5, 0.8, 0, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.sniper,
          g.lazer,
          g.morespeed,
          g.morespeed,
          g.morespeed,
          g.doublereload, g.doubledamage, g.doubledamage
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18.5, 10, 0.85, 0, 0, 0, 0]
    }
  ]
};
exports.rifle = {
  PARENT: [exports.genericTank],
  LABEL: "Rifle",
  BODY: {
    ACCELERATION: base.ACCEL * 0.7,
    FOV: base.FOV * 1.225
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */

      POSITION: [20, 10.5, 1, 0, 0, 0, 0]
    },
    {
      POSITION: [24, 7, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.rifle]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.srifle = {
  PARENT: [exports.genericTank],
  LABEL: "Sniper Rifle",
  BODY: {
    ACCELERATION: base.ACCEL * 0.7,
    FOV: base.FOV * 1.225
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */

      POSITION: [20, 10.5, 1, 0, 0, 0, 0]
    },
    {
      POSITION: [24, 7, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.rifle]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.asrifle = {
  PARENT: [exports.genericTank],
  LABEL: "Assault Rifle",
  BODY: {
    ACCELERATION: base.ACCEL * 0.7,
    FOV: base.FOV * 1.1
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */

      POSITION: [20, 10.5, 1.5, 0, 0, 0, 0]
    },
    {
      POSITION: [24, 7, 1.5, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.sniper,
          g.rifle,
          g.mach,
          g.mach,
          g.halfreload,
          g.doubledamage
        ]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.awp = {
  PARENT: [exports.genericTank],
  LABEL: "Super Machine Gun",
  BODY: {
    ACCELERATION: base.ACCEL * 0.7,
    FOV: base.FOV * 0.75
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */

      POSITION: [15, 10.5, 1.5, 0, 0, 0, 0]
    },
    {
      POSITION: [19, 7, 1.5, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.sniper,
          g.rifle,
          g.mach,
          g.mach,
          g.doubledamage,
          g.fast
        ]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.musket = {
  PARENT: [exports.genericTank],
  LABEL: "Musket",
  BODY: {
    ACCELERATION: base.ACCEL * 0.7,
    FOV: base.FOV * 1.225
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */

      POSITION: [20, 20, 1, 0, 0, 0, 0]
    },
    {
      POSITION: [24, 7, 1, 0, 4.5, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.rifle, g.twin]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [24, 7, 1, 0, -4.5, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.rifle, g.twin]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.archer = {
  PARENT: [exports.genericTank],
  LABEL: "Archer",
  BODY: {
    ACCELERATION: base.ACCEL * 0.7,
    FOV: base.FOV * 1.225
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */

      POSITION: [20, 10.5, 1, 0, 0, 0, 0]
    },
    {
      POSITION: [24, 7, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.rifle, g.mini]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [22, 7, 1, 0, 0, 0, 0.333],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.rifle, g.mini]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [20, 7, 1, 0, 0, 0, 0.667],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.rifle, g.mini]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.assassin = {
  PARENT: [exports.genericTank],
  DANGER: 6,
  LABEL: "Assassin",
  BODY: {
    ACCELERATION: base.ACCEL * 0.6,
    SPEED: base.SPEED * 0.85,
    FOV: base.FOV * 1.4
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [27, 8.5, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.assass]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 8.5, -1.6, 8, 0, 0, 0]
    }
  ]
};
exports.ggatl = {
  PARENT: [exports.genericTank],
  DANGER: 6,
  LABEL: "Great Gatler",
  BODY: {
    ACCELERATION: base.ACCEL * 0.6,
    SPEED: base.SPEED * 0.85,
    FOV: base.FOV * 1.4
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [27, 8.5, 1.5, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.assass, g.mach]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 8.5, -1.6, 8, 0, 0, 0]
    }
  ]
};
exports.m24 = {
  PARENT: [exports.genericTank],
  DANGER: 12,
  LABEL: "M24",
  BODY: {
    ACCELERATION: base.ACCEL * 0.6,
    SPEED: base.SPEED * 0.85,
    FOV: base.FOV * 2.5
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [33, 4, 1, 0, 0, 0, 1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.assass, g.halfreload, g.halfreload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast, g.doublerecoil]),
        TYPE: exports.bullet
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [7, 4, 1, 0, -5, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.assass, g.doublereload, g.doublereload, g.doublereload, g.doublereload, g.doublereload, g.norecoil]),
        TYPE: exports.pointer
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [15, 6, 1, 30, 0, 0, 1],
    }, 
  ]
};
exports.m48 = {
  PARENT: [exports.genericTank],
  DANGER: 12,
  LABEL: "M48",
  BODY: {
    ACCELERATION: base.ACCEL * 0.5,
    SPEED: base.SPEED * 0.65,
    FOV: base.FOV * 3.5
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [44, 4, 1, 0, 0, 0, 1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.assass, g.halfreload, g.halfreload, g.halfreload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast, g.doublerecoil, g.doublerecoil, g.doubledamage]),
        TYPE: exports.bullet
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [7, 4, 1, 0, -5, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.assass, g.doublereload, g.doublereload, g.doublereload, g.doublereload, g.doublereload, g.norecoil, g.doublerange]),
        TYPE: exports.pointer
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [15, 6, 1, 40, 0, 0, 1],
    }, 
  ]
};
exports.ranger = {
  PARENT: [exports.genericTank],
  LABEL: "Ranger",
  DANGER: 7,
  BODY: {
    ACCELERATION: base.ACCEL * 0.5,
    SPEED: base.SPEED * 0.8,
    FOV: base.FOV * 1.5
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [32, 8.5, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.assass]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [5, 8.5, -1.6, 8, 0, 0, 0]
    }
  ]
};
exports.autoass = makeAuto(exports.assassin, "Auto assassin");
exports.autosaw = makeAuto(exports.Sawshot, "Auto Sawshot");

exports.hunter = {
  PARENT: [exports.genericTank],
  LABEL: "Hunter",
  DANGER: 6,
  BODY: {
    ACCELERATION: base.ACCEL * 0.7,
    SPEED: base.SPEED * 0.9,
    FOV: base.FOV * 1.25
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [24, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.hunter, g.hunter2]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [21, 12, 1, 0, 0, 0, 0.25],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.hunter]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.preda = {
  PARENT: [exports.genericTank],
  LABEL: "Predator",
  DANGER: 7,
  BODY: {
    ACCELERATION: base.ACCEL * 0.7,
    SPEED: base.SPEED * 0.85,
    FOV: base.FOV * 1.3
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [24, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.sniper,
          g.hunter,
          g.hunter2,
          g.hunter2,
          g.preda
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [21, 12, 1, 0, 0, 0, 0.15],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.sniper,
          g.hunter,
          g.hunter2,
          g.preda
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 16, 1, 0, 0, 0, 0.3],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.hunter, g.preda]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.poach = makeHybrid(exports.hunter, "Poacher");
exports.sidewind = {
  PARENT: [exports.genericTank],
  LABEL: "Sidewinder",
  DANGER: 7,
  BODY: {
    ACCELERATION: base.ACCEL * 0.7,
    SPEED: base.SPEED * 0.8,
    FOV: base.FOV * 1.3
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [10, 11, -0.5, 14, 0, 0, 0]
    },
    {
      POSITION: [21, 12, -1.1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.hunter, g.sidewind]),
        TYPE: exports.snake,
        STAT_CALCULATOR: gunCalcNames.sustained
      }
    }
  ]
};

exports.director = {
  PARENT: [exports.genericTank],
  LABEL: "Director",
  STAT_NAMES: statnames.drone,
  DANGER: 5,
  BODY: {
    ACCELERATION: base.ACCEL * 0.75,
    FOV: base.FOV * 1.1
  },
  MAX_CHILDREN: 5,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [6, 12, 1.2, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.drone, g.over]),
        TYPE: exports.drone,
        AUTOFIRE: true,
        SYNCS_SKILLS: true,
        STAT_CALCULATOR: gunCalcNames.drone
      }
    }
  ]
};
exports.lightning = {
  PARENT: [exports.genericTank],
  LABEL: "Lightning",
  STAT_NAMES: statnames.drone,
  DANGER: 5,
  BODY: {
    ACCELERATION: base.ACCEL * 0.75,
    FOV: base.FOV * 2.5
  },
  MAX_CHILDREN: 5,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [12, 12, 1.3, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.drone, g.over]),
        TYPE: exports.boostdrone,
        AUTOFIRE: true,
        SYNCS_SKILLS: true,
        STAT_CALCULATOR: gunCalcNames.drone
      }
    }
  ]
};
exports.lightspeed = {
  PARENT: [exports.genericTank],
  LABEL: "Lightspeed",
  STAT_NAMES: statnames.drone,
  DANGER: 5,
  BODY: {
    ACCELERATION: base.ACCEL * 0.75,
    FOV: base.FOV * 5
  },
  MAX_CHILDREN: 5,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 12, 1.3, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.drone, g.over]),
        TYPE: exports.fallstar,
        AUTOFIRE: true,
        SYNCS_SKILLS: true,
        STAT_CALCULATOR: gunCalcNames.drone
      }
    }
  ]
};
exports.master = {
  PARENT: [exports.genericTank],
  LABEL: "Colony",
  STAT_NAMES: statnames.drone,
  SHAPE: 6,
  DANGER: 7,
  BODY: {
    ACCELERATION: base.ACCEL * 0.75,
    FOV: base.FOV * 1.15
  },
  FACING_TYPE: "autospin",
  TURRETS: [
    {
      /*  SIZE     X       Y     ANGLE    ARC */
      POSITION: [16, 1, 0, 0, 0, 0],
      TYPE: exports.masterGun
    },
    {
      POSITION: [16, 1, 0, 120, 0, 0],
      TYPE: [exports.masterGun, { INDEPENDENT: true }]
    },
    {
      POSITION: [16, 1, 0, 240, 0, 0],
      TYPE: [exports.masterGun, { INDEPENDENT: true }]
    }
  ]
};

exports.overseer = {
  PARENT: [exports.genericTank],
  LABEL: "Overseer",
  DANGER: 6,
  STAT_NAMES: statnames.drone,
  BODY: {
    ACCELERATION: base.ACCEL * 0.75,
    SPEED: base.SPEED * 0.9,
    FOV: base.FOV * 1.1
  },
  MAX_CHILDREN: 8,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [6, 12, 1.2, 8, 0, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.drone, g.over]),
        TYPE: exports.drone,
        AUTOFIRE: true,
        SYNCS_SKILLS: true,
        STAT_CALCULATOR: gunCalcNames.drone,
        WAIT_TO_CYCLE: true
      }
    },
    {
      POSITION: [6, 12, 1.2, 8, 0, 270, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.drone, g.over]),
        TYPE: exports.drone,
        AUTOFIRE: true,
        SYNCS_SKILLS: true,
        STAT_CALCULATOR: gunCalcNames.drone,
        WAIT_TO_CYCLE: true
      }
    }
  ]
};
exports.zeus = {
  PARENT: [exports.genericTank],
  LABEL: "Zeus",
  DANGER: 6,
  STAT_NAMES: statnames.drone,
  BODY: {
    ACCELERATION: base.ACCEL * 0.75,
    SPEED: base.SPEED * 0.9,
    FOV: base.FOV * 1.1
  },
  MAX_CHILDREN: 8,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [6, 12, 1.2, 8, 0, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.drone, g.over]),
        TYPE: exports.boostdrone,
        AUTOFIRE: true,
        SYNCS_SKILLS: true,
        STAT_CALCULATOR: gunCalcNames.drone,
        WAIT_TO_CYCLE: true
      }
    },
    {
      POSITION: [6, 12, 1.2, 8, 0, 270, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.drone, g.over]),
        TYPE: exports.boostdrone,
        AUTOFIRE: true,
        SYNCS_SKILLS: true,
        STAT_CALCULATOR: gunCalcNames.drone,
        WAIT_TO_CYCLE: true
      }
    }
  ]
};
exports.overlord = {
  PARENT: [exports.genericTank],
  LABEL: "Overlord",
  DANGER: 7,
  STAT_NAMES: statnames.drone,
  BODY: {
    ACCELERATION: base.ACCEL * 0.75,
    SPEED: base.SPEED * 0.8,
    FOV: base.FOV * 1.1
  },
  MAX_CHILDREN: 8,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [6, 12, 1.2, 8, 0, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.drone, g.over]),
        TYPE: exports.drone,
        AUTOFIRE: true,
        SYNCS_SKILLS: true,
        STAT_CALCULATOR: gunCalcNames.drone,
        WAIT_TO_CYCLE: true
      }
    },
    {
      POSITION: [6, 12, 1.2, 8, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.drone, g.over]),
        TYPE: exports.drone,
        AUTOFIRE: true,
        SYNCS_SKILLS: true,
        STAT_CALCULATOR: gunCalcNames.drone,
        WAIT_TO_CYCLE: true
      }
    },
    {
      POSITION: [6, 12, 1.2, 8, 0, 270, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.drone, g.over]),
        TYPE: exports.drone,
        AUTOFIRE: true,
        SYNCS_SKILLS: true,
        STAT_CALCULATOR: gunCalcNames.drone,
        WAIT_TO_CYCLE: true
      }
    },
    {
      POSITION: [6, 12, 1.2, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.drone, g.over]),
        TYPE: exports.drone,
        AUTOFIRE: true,
        SYNCS_SKILLS: true,
        STAT_CALCULATOR: gunCalcNames.drone,
        WAIT_TO_CYCLE: true
      }
    }
  ]
};
exports.overtrap = {
  PARENT: [exports.genericTank],
  LABEL: "Overtrapper",
  DANGER: 7,
  STAT_NAMES: statnames.generic,
  BODY: {
    ACCELERATION: base.ACCEL * 0.6,
    SPEED: base.SPEED * 0.8,
    FOV: base.FOV * 1.2
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [6, 11, 1.2, 8, 0, 125, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.drone, g.over, g.meta]),
        TYPE: exports.drone,
        AUTOFIRE: true,
        SYNCS_SKILLS: true,
        STAT_CALCULATOR: gunCalcNames.drone,
        WAIT_TO_CYCLE: true,
        MAX_CHILDREN: 3
      }
    },
    {
      POSITION: [6, 11, 1.2, 8, 0, 235, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.drone, g.over, g.meta]),
        TYPE: exports.drone,
        AUTOFIRE: true,
        SYNCS_SKILLS: true,
        STAT_CALCULATOR: gunCalcNames.drone,
        WAIT_TO_CYCLE: true,
        MAX_CHILDREN: 3
      }
    },
    {
      POSITION: [14, 8, 1, 0, 0, 0, 0]
    },
    {
      POSITION: [4, 8, 1.5, 14, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap]),
        TYPE: exports.trap,
        STAT_CALCULATOR: gunCalcNames.trap
      }
    }
  ]
};
exports.banshee = {
  PARENT: [exports.genericTank],
  LABEL: "",
  DANGER: 7,
  BODY: {
    ACCELERATION: base.ACCEL * 0.5,
    SPEED: base.SPEED * 0.8,
    FOV: base.FOV * 1.1
  },
  FACING_TYPE: "autospin",
  TURRETS: [
    {
      /*  SIZE     X       Y     ANGLE    ARC */
      POSITION: [10, 8, 0, 0, 80, 0],
      TYPE: exports.bansheegun
    },
    {
      POSITION: [10, 8, 0, 120, 80, 0],
      TYPE: exports.bansheegun
    },
    {
      POSITION: [10, 8, 0, 240, 80, 0],
      TYPE: exports.bansheegun
    }
  ],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [6, 11, 1.2, 8, 0, 60, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.drone, g.over, g.meta]),
        TYPE: exports.drone,
        AUTOFIRE: true,
        SYNCS_SKILLS: true,
        STAT_CALCULATOR: gunCalcNames.drone,
        WAIT_TO_CYCLE: true,
        MAX_CHILDREN: 2
      }
    },
    {
      POSITION: [6, 11, 1.2, 8, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.drone, g.over, g.meta]),
        TYPE: exports.drone,
        AUTOFIRE: true,
        SYNCS_SKILLS: true,
        STAT_CALCULATOR: gunCalcNames.drone,
        WAIT_TO_CYCLE: true,
        MAX_CHILDREN: 2
      }
    },
    {
      POSITION: [6, 11, 1.2, 8, 0, 300, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.drone, g.over, g.meta]),
        TYPE: exports.drone,
        AUTOFIRE: true,
        SYNCS_SKILLS: true,
        STAT_CALCULATOR: gunCalcNames.drone,
        WAIT_TO_CYCLE: true,
        MAX_CHILDREN: 2
      }
    }
  ]
};
exports.autoover = makeAuto(exports.overseer, "");
exports.overgunner = {
  PARENT: [exports.genericTank],
  LABEL: "Overgunner",
  DANGER: 7,
  STAT_NAMES: statnames.generic,
  BODY: {
    ACCELERATION: base.ACCEL * 0.75,
    SPEED: base.SPEED * 0.9,
    FOV: base.FOV * 1.1
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [6, 11, 1.2, 8, 0, 125, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.drone, g.over, g.meta]),
        TYPE: exports.drone,
        AUTOFIRE: true,
        SYNCS_SKILLS: true,
        STAT_CALCULATOR: gunCalcNames.drone,
        WAIT_TO_CYCLE: true,
        MAX_CHILDREN: 3
      }
    },
    {
      POSITION: [6, 11, 1.2, 8, 0, 235, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.drone, g.over, g.meta]),
        TYPE: exports.drone,
        AUTOFIRE: true,
        SYNCS_SKILLS: true,
        STAT_CALCULATOR: gunCalcNames.drone,
        WAIT_TO_CYCLE: true,
        MAX_CHILDREN: 3
      }
    },
    {
      POSITION: [19, 2, 1, 0, -2.5, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.power,
          g.twin,
          g.slow,
          g.flank,
          g.lotsmorrecoil
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [19, 2, 1, 0, 2.5, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.power,
          g.twin,
          g.slow,
          g.flank,
          g.lotsmorrecoil
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [12, 11, 1, 0, 0, 0, 0]
    }
  ]
};

function makeSwarmSpawner(guntype) {
  return {
    PARENT: [exports.genericTank],
    LABEL: "",
    BODY: {
      FOV: 2
    },
    CONTROLLERS: ["nearestDifferentMaster"],
    COLOR: 16,
    AI: {
      NO_LEAD: true,
      SKYNET: true,
      FULL_VIEW: true
    },
    GUNS: [
      {
        /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
        POSITION: [14, 15, 0.6, 14, 0, 0, 0],
        PROPERTIES: {
          SHOOT_SETTINGS: guntype,
          TYPE: exports.swarm,
          STAT_CALCULATOR: gunCalcNames.swarm
        }
      }
    ]
  };
}
exports.cruiserGun = makeSwarmSpawner(combineStats([g.swarm]));
exports.cruiser = {
  PARENT: [exports.genericTank],
  LABEL: "Cruiser",
  DANGER: 6,
  FACING_TYPE: "locksFacing",
  STAT_NAMES: statnames.swarm,
  BODY: {
    ACCELERATION: base.ACCEL * 0.75,
    FOV: base.FOV * 1.2
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [7, 7.5, 0.6, 7, 4, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm]),
        TYPE: exports.swarm,
        STAT_CALCULATOR: gunCalcNames.swarm
      }
    },
    {
      POSITION: [7, 7.5, 0.6, 7, -4, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm]),
        TYPE: exports.swarm,
        STAT_CALCULATOR: gunCalcNames.swarm
      }
    },
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [7, 7.5, 0.6, 7, 4, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm]),
        TYPE: exports.swarm,
        STAT_CALCULATOR: gunCalcNames.swarm
      }
    },
    {
      POSITION: [7, 7.5, 0.6, 7, -4, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm]),
        TYPE: exports.swarm,
        STAT_CALCULATOR: gunCalcNames.swarm
      }
    }
  ]
};
exports.battleship = {
  PARENT: [exports.genericTank],
  LABEL: "Battleship",
  DANGER: 7,
  STAT_NAMES: statnames.swarm,
  FACING_TYPE: "locksFacing",
  BODY: {
    ACCELERATION: base.ACCEL,
    FOV: base.FOV * 1.2
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [7, 7.5, 0.6, 7, 4, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm, g.battle]),
        TYPE: exports.swarm,
        STAT_CALCULATOR: gunCalcNames.swarm,
        LABEL: "Guided"
      }
    },
    {
      POSITION: [7, 7.5, 0.6, 7, -4, 90, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm]),
        TYPE: [exports.autoswarm],
        STAT_CALCULATOR: gunCalcNames.swarm,
        LABEL: "Autonomous"
      }
    },
    {
      POSITION: [7, 7.5, 0.6, 7, 4, 270, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm]),
        TYPE: [exports.autoswarm],
        STAT_CALCULATOR: gunCalcNames.swarm,
        LABEL: "Autonomous"
      }
    },
    {
      POSITION: [7, 7.5, 0.6, 7, -4, 270, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm, g.battle]),
        TYPE: exports.swarm,
        STAT_CALCULATOR: gunCalcNames.swarm,
        LABEL: "Guided"
      }
    }
  ]
};
exports.lightspeed2 = {
  PARENT: [exports.genericTank],
  LABEL: "Lightspeed 2.0",
  DANGER: 7,
  STAT_NAMES: statnames.swarm,
  FACING_TYPE: "locksFacing",
  BODY: {
    ACCELERATION: base.ACCEL,
    FOV: base.FOV * 5
  },
  GUNS: [
    {
      POSITION: [7, 7.5, 0.6, 7, -4, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm]),
        TYPE: [exports.autofallstar],
        STAT_CALCULATOR: gunCalcNames.swarm,
        LABEL: "Autonomous"
      }
    },
    {
      POSITION: [7, 7.5, 0.6, 7, 4, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm]),
        TYPE: [exports.autofallstar],
        STAT_CALCULATOR: gunCalcNames.swarm,
        LABEL: "Autonomous"
      }
    }
  ]
};
exports.carrier = {
  PARENT: [exports.genericTank],
  LABEL: "Carrier",
  DANGER: 7,
  STAT_NAMES: statnames.swarm,
  FACING_TYPE: "locksFacing",
  BODY: {
    ACCELERATION: base.ACCEL * 0.75,
    FOV: base.FOV * 1.3
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [7, 7.5, 0.6, 7, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm, g.battle, g.carrier]),
        TYPE: exports.swarm,
        STAT_CALCULATOR: gunCalcNames.swarm
      }
    },
    {
      POSITION: [7, 7.5, 0.6, 7, 2, 40, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm, g.battle, g.carrier]),
        TYPE: exports.swarm,
        STAT_CALCULATOR: gunCalcNames.swarm
      }
    },
    {
      POSITION: [7, 7.5, 0.6, 7, -2, -40, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm, g.battle, g.carrier]),
        TYPE: exports.swarm,
        STAT_CALCULATOR: gunCalcNames.swarm
      }
    }
  ]
};
exports.autocruiser = makeAuto(exports.cruiser, "");
exports.fortress = {
  PARENT: [exports.genericTank],
  LABEL: "Fortress", //'Palisade',
  DANGER: 7,
  STAT_NAMES: statnames.generic,
  BODY: {
    SPEED: base.SPEED * 0.8,
    FOV: base.FOV * 1.2
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [7, 7.5, 0.6, 7, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm]),
        TYPE: [exports.swarm, { CONTROLLERS: ["canRepel"] }],
        STAT_CALCULATOR: gunCalcNames.swarm
      }
    },
    {
      POSITION: [7, 7.5, 0.6, 7, 0, 120, 1 / 3],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm]),
        TYPE: [exports.swarm, { CONTROLLERS: ["canRepel"] }],
        STAT_CALCULATOR: gunCalcNames.swarm
      }
    },
    {
      POSITION: [7, 7.5, 0.6, 7, 0, 240, 2 / 3],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm]),
        TYPE: [exports.swarm, { CONTROLLERS: ["canRepel"] }],
        STAT_CALCULATOR: gunCalcNames.swarm
      }
    },
    {
      POSITION: [14, 9, 1, 0, 0, 60, 0]
    },
    {
      POSITION: [4, 9, 1.5, 14, 0, 60, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap, g.halfrange, g.slow]),
        TYPE: exports.trap,
        STAT_CALCULATOR: gunCalcNames.trap
      }
    },
    {
      POSITION: [14, 9, 1, 0, 0, 180, 0]
    },
    {
      POSITION: [4, 9, 1.5, 14, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap, g.halfrange, g.slow]),
        TYPE: exports.trap,
        STAT_CALCULATOR: gunCalcNames.trap
      }
    },
    {
      POSITION: [14, 9, 1, 0, 0, 300, 0]
    },
    {
      POSITION: [4, 9, 1.5, 14, 0, 300, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap, g.halfrange, g.slow]),
        TYPE: exports.trap,
        STAT_CALCULATOR: gunCalcNames.trap
      }
    }
  ]
};

exports.underseer = {
  PARENT: [exports.genericTank],
  LABEL: "Underseer",
  DANGER: 6,
  STAT_NAMES: statnames.drone,
  BODY: {
    ACCELERATION: base.ACCEL * 0.7,
    SPEED: base.SPEED * 0.9,
    FOV: base.FOV * 1.1
  },
  SHAPE: 4,
  MAX_CHILDREN: 14,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [5, 12, 1.2, 8, 0, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.drone, g.sunchip]),
        TYPE: exports.sunchip,
        AUTOFIRE: true,
        SYNCS_SKILLS: true,
        STAT_CALCULATOR: gunCalcNames.necro
      }
    },
    {
      POSITION: [5, 12, 1.2, 8, 0, 270, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.drone, g.sunchip]),
        TYPE: exports.sunchip,
        AUTOFIRE: true,
        SYNCS_SKILLS: true,
        STAT_CALCULATOR: gunCalcNames.necro
      }
    }
  ]
};
exports.necromancer = {
  PARENT: [exports.genericTank],
  LABEL: "Necromancer",
  DANGER: 7,
  STAT_NAMES: statnames.necro,
  BODY: {
    ACCELERATION: base.ACCEL * 0.7,
    SPEED: base.SPEED * 0.8,
    FOV: base.FOV * 1.15
  },
  SHAPE: 4,
  FACING_TYPE: "autospin",
  MAX_CHILDREN: 14,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [5, 12, 1.2, 8, 0, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.drone, g.sunchip]),
        TYPE: exports.sunchip,
        AUTOFIRE: true,
        SYNCS_SKILLS: true,
        STAT_CALCULATOR: gunCalcNames.necro
      }
    },
    {
      POSITION: [5, 12, 1.2, 8, 0, 270, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.drone, g.sunchip]),
        TYPE: exports.sunchip,
        AUTOFIRE: true,
        SYNCS_SKILLS: true,
        STAT_CALCULATOR: gunCalcNames.necro
      }
    },
    {
      POSITION: [5, 12, 1.2, 8, 0, 0, 0.25],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.drone,
          g.sunchip,
          g.weak,
          g.doublereload
        ]),
        TYPE: exports.autosunchip,
        AUTOFIRE: true,
        SYNCS_SKILLS: true,
        MAX_CHILDREN: 4,
        STAT_CALCULATOR: gunCalcNames.necro,
        LABEL: "Guard"
      }
    },
    {
      POSITION: [5, 12, 1.2, 8, 0, 180, 0.75],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.drone,
          g.sunchip,
          g.weak,
          g.doublereload
        ]),
        TYPE: exports.autosunchip,
        AUTOFIRE: true,
        SYNCS_SKILLS: true,
        MAX_CHILDREN: 4,
        STAT_CALCULATOR: gunCalcNames.necro,
        LABEL: "Guard"
      }
    }
  ]
};

exports.lilfact = {
  PARENT: [exports.genericTank],
  LABEL: "spawner",
  DANGER: 6,
  STAT_NAMES: statnames.drone,
  BODY: {
    SPEED: base.SPEED * 0.8,
    ACCELERATION: base.ACCEL * 0.5,
    FOV: 1.1
  },
  GUNS: [
    {
      /**** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [4.5, 10, 1, 10.5, 0, 0, 0]
    },
    {
      POSITION: [1, 12, 1, 15, 0, 0, 0],
      PROPERTIES: {
        MAX_CHILDREN: 4,
        SHOOT_SETTINGS: combineStats([g.factory, g.babyfactory]),
        TYPE: exports.minion,
        STAT_CALCULATOR: gunCalcNames.drone,
        AUTOFIRE: true,
        SYNCS_SKILLS: true
      }
    },
    {
      POSITION: [3.5, 12, 1, 8, 0, 0, 0]
    }
  ]
};
exports.superstorm = {
  PARENT: [exports.genericTank],
  LABEL: "Superstorm",
  FACING_TYPE: "autospin",
  DANGER: 6,
  STAT_NAMES: statnames.drone,
  BODY: {
    SPEED: base.SPEED * 0.8,
    ACCELERATION: base.ACCEL * 0.5,
    FOV: 1.5
  },
  GUNS: [
    {
      /**** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [4.5, 10, 1, 10.5, 0, 270, 0]
    },
    {
      POSITION: [1, 12, 1, 15, 0, 270, 0],
      PROPERTIES: {
        MAX_CHILDREN: 4,
        SHOOT_SETTINGS: combineStats([g.basic, g.spam]),
        TYPE: exports.minidust,
        STAT_CALCULATOR: gunCalcNames.drone,
        AUTOFIRE: true,
        SYNCS_SKILLS: true
      }
    },
    {
      POSITION: [5, 12, 0.5, 8, 0, 270, 0]
    },
    {
      /**** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [4.5, 10, 1, 10.5, 0, 90, 0]
    },
    {
      POSITION: [1, 12, 1, 15, 0, 90, 0.5],
      PROPERTIES: {
        MAX_CHILDREN: 4,
        SHOOT_SETTINGS: combineStats([g.basic, g.spam]),
        TYPE: exports.minidust,
        STAT_CALCULATOR: gunCalcNames.drone,
        AUTOFIRE: true,
        SYNCS_SKILLS: true
      }
    },
    {
      POSITION: [5, 12, 0.5, 8, 0, 90, 0]
    },
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [10, 10, 1, 0, 15, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.spam]),
        TYPE: exports.bullet,
        AUTOFIRE: true
      }
    },
    {
      POSITION: [10, 10, 1, 0, 15, 270, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.spam]),
        TYPE: exports.bullet,
        AUTOFIRE: true
      }
    }
  ]
};
exports.crusade = {
  PARENT: [exports.genericTank],
  LABEL: "Crusade",
  DANGER: 6,
  STAT_NAMES: statnames.drone,
  BODY: {
    SPEED: base.SPEED * 0.8,
    ACCELERATION: base.ACCEL * 0.5,
    FOV: 1.1
  },
  GUNS: [
    {
      /**** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [4.5, 10, 1, 10.5, 0, 0, 0]
    },
    {
      POSITION: [1, 12, 1, 15, 0, 0, 0],
      PROPERTIES: {
        MAX_CHILDREN: 4,
        SHOOT_SETTINGS: combineStats([g.factory, g.babyfactory]),
        TYPE: exports.twinion,
        STAT_CALCULATOR: gunCalcNames.drone,
        AUTOFIRE: true,
        SYNCS_SKILLS: true
      }
    },
    {
      POSITION: [3.5, 6, 1, 8, 3, 0, 0]
    },
    {
      POSITION: [3.5, 6, 1, 8, -3, 0, 0]
    }
  ]
};
exports.brigade = {
  PARENT: [exports.genericTank],
  LABEL: "Brigade",
  DANGER: 6,
  STAT_NAMES: statnames.drone,
  BODY: {
    SPEED: base.SPEED * 0.8,
    ACCELERATION: base.ACCEL * 0.5,
    FOV: 1.2
  },
  GUNS: [
    {
      /**** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [4.5, 10, 1, 10.5, 0, 0, 0]
    },
    {
      POSITION: [1, 12, 1, 15, 0, 0, 0],
      PROPERTIES: {
        MAX_CHILDREN: 4,
        SHOOT_SETTINGS: combineStats([g.factory, g.babyfactory]),
        TYPE: exports.machminion,
        STAT_CALCULATOR: gunCalcNames.drone,
        AUTOFIRE: true,
        SYNCS_SKILLS: true
      }
    },
    {
      POSITION: [3.5, 12, 1.2, 8, 0, 0, 0]
    }
  ]
};
exports.factory = {
  PARENT: [exports.genericTank],
  LABEL: "Factory",
  DANGER: 7,
  STAT_NAMES: statnames.drone,
  BODY: {
    SPEED: base.SPEED * 0.8,
    FOV: 1.1
  },
  MAX_CHILDREN: 6,
  GUNS: [
    {
      /**** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [5, 11, 1, 10.5, 0, 0, 0]
    },
    {
      POSITION: [2, 14, 1, 15.5, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.factory]),
        TYPE: exports.minion,
        STAT_CALCULATOR: gunCalcNames.drone,
        AUTOFIRE: true,
        SYNCS_SKILLS: true
      }
    },
    {
      POSITION: [4, 14, 1, 8, 0, 0, 0]
    }
  ]
};

exports.machine = {
  PARENT: [exports.genericTank],
  LABEL: "Machine Gun",
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [12, 10, 1.4, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.gatl = {
  PARENT: [exports.genericTank],
  LABEL: "gatling gun",
  BODY: {
    FOV: base.FOV * 1.2,
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [17, 8, 1.4, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.sniper]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.infer = {
  PARENT: [exports.genericTank],
  LABEL: "Inferno",
  GUNS: [
    {
      POSITION: [12, 5, 1.2, 8, 3, 0, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      }
    }
  ]
};
exports.firestorm = {
  PARENT: [exports.genericTank],
  LABEL: "Firestorm",
  GUNS: [
    {
      POSITION: [12, 5, 1.2, 8, 3, 0, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    },  {
      POSITION: [12, 5, 1.2, 8, 3, 12, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 12, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    },  {
      POSITION: [12, 5, 1.2, 8, 3, 24, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 24, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    },  {
      POSITION: [12, 5, 1.2, 8, 3, 36, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 36, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    },  {
      POSITION: [12, 5, 1.2, 8, 3, 48, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 48, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    },  {
      POSITION: [12, 5, 1.2, 8, 3, 60, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 60, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    },  {
      POSITION: [12, 5, 1.2, 8, 3, 72, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 72, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    },  {
      POSITION: [12, 5, 1.2, 8, 3, 84, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 84, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    },  {
      POSITION: [12, 5, 1.2, 8, 3, 96, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 96, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    },  {
      POSITION: [12, 5, 1.2, 8, 3, 108, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 108, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    },  {
      POSITION: [12, 5, 1.2, 8, 3, 120, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 120, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    },  {
      POSITION: [12, 5, 1.2, 8, 3, 132, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 132, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    },  {
      POSITION: [12, 5, 1.2, 8, 3, 144, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 144, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    },  {
      POSITION: [12, 5, 1.2, 8, 3, 156, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 156, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    },  {
      POSITION: [12, 5, 1.2, 8, 3, 168, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 168, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    }, {
      POSITION: [12, 5, 1.2, 8, 3, 168, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 168, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    }, {
      POSITION: [12, 5, 1.2, 8, 3, 180, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    }, {
      POSITION: [12, 5, 1.2, 8, 3, 192, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 192, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    }, {
      POSITION: [12, 5, 1.2, 8, 3, 204, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 204, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    }, {
      POSITION: [12, 5, 1.2, 8, 3, 216, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 216, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    }, {
      POSITION: [12, 5, 1.2, 8, 3, 228, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 228, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    }, {
      POSITION: [12, 5, 1.2, 8, 3, 240, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 240, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    }, {
      POSITION: [12, 5, 1.2, 8, 3, 252, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 252, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    }, {
      POSITION: [12, 5, 1.2, 8, 3, 264, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 264, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    }, {
      POSITION: [12, 5, 1.2, 8, 3, 276, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 276, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    }, {
      POSITION: [12, 5, 1.2, 8, 3, 288, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 288, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    }, {
      POSITION: [12, 5, 1.2, 8, 3, 300, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 300, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    }, {
      POSITION: [12, 5, 1.2, 8, 3, 312, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 312, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    }, {
      POSITION: [12, 5, 1.2, 8, 3, 324, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 324, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    }, {
      POSITION: [12, 5, 1.2, 8, 3, 336, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 336, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    }, {
      POSITION: [12, 5, 1.2, 8, 3, 348, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 348, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      },
    }, {
      POSITION: [12, 5, 1.2, 8, 3, 5, 0]
    },
    {
      POSITION: [17, 7, 1.7, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.doublesize]),
        TYPE: exports.fireball
      }
    }, {
      POSITION: [10, 5, 1.2, 8, 3, 65, 0]
    },
    {
      POSITION: [15, 7, 1.7, 8, 0, 60, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.doublesize]),
        TYPE: exports.fireball
      }
    }, {
      POSITION: [10, 5, 1.2, 8, 3, 125, 0]
    },
    {
      POSITION: [15, 7, 1.7, 8, 0, 120, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.doublesize]),
        TYPE: exports.fireball
      }
    }, {
      POSITION: [10, 5, 1.2, 8, 3, 185, 0]
    },
    {
      POSITION: [15, 7, 1.7, 8, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.doublesize]),
        TYPE: exports.fireball
      }
    }, {
      POSITION: [10, 5, 1.2, 8, 3, 245, 0]
    },
    {
      POSITION: [15, 7, 1.7, 8, 0, 240, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.doublesize]),
        TYPE: exports.fireball
      }
    }, {
      POSITION: [10, 5, 1.2, 8, 3, 305, 0]
    },
    {
      POSITION: [15, 7, 1.7, 8, 0, 300, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.doublesize]),
        TYPE: exports.fireball
      }
    }, {
      POSITION: [12, 5, 1.2, 8, 4, 30, 0]
    },
    {
      POSITION: [17, 7, 10, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.doublesize, g.doublesize, g.doublesize, g.nospeed]),
        TYPE: exports.infernado,
        MAX_CHILDREN: 1
      }
    }
  ]
};
exports.fireballer = {
  PARENT: [exports.genericTank],
  LABEL: "Fireballer",
  BODY: {
    FOV: 2
  },
  GUNS: [
    {
      POSITION: [12, 5, 1.2, 8, 3, 5, 0]
    },
    {
      POSITION: [17, 7, 1.7, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.doublesize]),
        TYPE: exports.fireball
      }
    }
  ]
};exports.poisonballer = {
  PARENT: [exports.genericTank],
  LABEL: "Poisonballer",
  BODY: {
    FOV: 2
  },
  GUNS: [
    {
      POSITION: [12, 5, 1.2, 8, 3, 5, 0]
    },
    {
      POSITION: [17, 7, 1.7, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.doublesize]),
        TYPE: exports.poisonball
      }
    }
  ]
};
exports.volc = {
  PARENT: [exports.genericTank],
  LABEL: "Volcano",
  BODY: {
    FOV: 2.5
  },
  GUNS: [
    {
      POSITION: [12, 5, 1.6, 8, 3, 5, 0]
    },
    {
      POSITION: [17, 7, 2.1, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.halfreload]),
        TYPE: exports.volfireball
      }
    }
  ]
};
exports.incann = {
  PARENT: [exports.genericTank],
  LABEL: "Infernoballer",
  BODY: {
    FOV: 2.25
  },
  GUNS: [
    {
      POSITION: [12, 10, 1.2, 8, 3, 5, 0]
    }, {
      POSITION: [10, 5, 1, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.KO, g.doublereload, g.doubledamage, g.doublepen, g.KO, g.nospray, g.halfdamage, g.halfdamage, g.halfdamage]),
        TYPE: exports.bullet,
        ALT_FIRE: true
      }
    },
    {
      POSITION: [17, 14, 1.7, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.doublesize]),
        TYPE: exports.infireball
      }
    }
  ]
};
exports.gfireballer = {
  PARENT: [exports.genericTank],
  LABEL: "Great Fireballer",
  BODY: {
    FOV: 3
  },
  GUNS: [
    {
      POSITION: [18, 7, 1.5, 8, 3, 5, 0]
    },
    {
      POSITION: [23, 9, 2, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.doublesize]),
        TYPE: exports.grfireball
      }
    }
  ]
};
exports.fireballspam = {
  PARENT: [exports.genericTank],
  LABEL: "Fireballerspammer",
  BODY: {
    FOV: 2
  },
  GUNS: [
    {
      POSITION: [12, 5, 1.2, 8, 3, 5, 0]
    },
    {
      POSITION: [17, 7, 1.7, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.doublesize]),
        TYPE: exports.fireball
      }
    }
  ]
};
exports.infersheild = {
  PARENT: [exports.genericTank],
  LABEL: "Inferno Sheild",
  GUNS: [
    {
      POSITION: [8, 5, 1.2, 8, 3, 120, 0]
    },
    {
      POSITION: [11, 7, 1.2, 8, 0, 120, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.halfreload]),
        TYPE: exports.fshield,
        MAX_CHILDREN: 2
      }
    }, {
      POSITION: [8, 5, 1.2, 8, 3, 240, 0]
    },
    {
      POSITION: [11, 7, 1.2, 8, 0, 240, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.halfreload]),
        TYPE: exports.fshield,
        MAX_CHILDREN: 2
      }
    }, {
      POSITION: [12, 5, 1.2, 8, 3, 0, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      }
    }
  ]
};
exports.infernadoshooter = {
  PARENT: [exports.genericTank],
  LABEL: "Infernado Shooter",
  GUNS: [
    {
      POSITION: [12, 5, 1.2, 8, 4, 30, 0]
    },
    {
      POSITION: [17, 7, 10, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.doublesize, g.doublesize, g.doublesize, g.nospeed]),
        TYPE: exports.infernado,
        MAX_CHILDREN: 1
      }
    }
  ]
};
exports.trueinfernadoshooter = {
  PARENT: [exports.genericTank],
  LABEL: "True Infernado Shooter",
  BODY:{
    FOV: 2
  },
  GUNS: [
    {
      POSITION: [12, 5, 1.2, 8, 4, 30, 0]
    },
    {
      POSITION: [17, 7, 10, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.doublesize, g.doublesize, g.doublesize, g.nospeed]),
        TYPE: exports.trueinfernado,
        MAX_CHILDREN: 5
      }
    }
  ]
};
exports.smoker = {
  PARENT: [exports.genericTank],
  LABEL: "Smoker",
  GUNS: [
    {
      POSITION: [12, 5, 1.2, 8, 3, 0, 0]
    },  {
      POSITION: [6.5, 5, 1.2, 0, -12, 90, 1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.smokerside]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [6.5, 5, 1.2, 0, 12, 270, 1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.smokerside]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [17, 7, 1.2, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      }
    }
  ]
};
exports.fireb = {
  PARENT: [exports.genericTank],
  LABEL: "Firebreather",
  GUNS: [
    {
      POSITION: [12, 5, 1.2, 8, 3, 0, 0]
    },  {
      POSITION: [6.5, 5, 1.2, 0, -12, 90, 1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.fire]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [6.5, 5, 1.2, 0, 12, 270, 1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.fire]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [17, 7, 1.2, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [17, 7, 1.2, 8, 0, 0, 1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.halfreload]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [17, 7, 1.2, 8, 0, 0, 1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.halfreload]),
        TYPE: exports.flare1
      }
    }, 
  ]
};
exports.sflare = {
  PARENT: [exports.genericTank],
  LABEL: "Infernis",
  GUNS: [
    {
      POSITION: [12, 5, 1.2, 8, 3, 0, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.halfreload, g.halfreload, g.halfreload]),
        TYPE: exports.flare4
      }
    }
  ]
};
exports.rof = {
  FACING_TYPE: "autospin",
  PARENT: [exports.genericTank],
  LABEL: "Ring of Fire",
  GUNS: [
    {
      POSITION: [12, 5, 1.2, 8, 3, 0, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.slow, g.slow]),
        TYPE: exports.flare1
      }
    },
    {
      POSITION: [12, 5, 1.2, 8, 3, 60, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 60, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.slow, g.slow]),
        TYPE: exports.flare1
      }
    },
    {
      POSITION: [12, 5, 1.2, 8, 3, 120, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 120, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.slow, g.slow]),
        TYPE: exports.flare1
      }
    },
    {
      POSITION: [12, 5, 1.2, 8, 3, 180, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 180, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.slow, g.slow]),
        TYPE: exports.flare1
      }
    },
    {
      POSITION: [12, 5, 1.2, 8, 3, 240, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 240, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.slow, g.slow]),
        TYPE: exports.flare1
      }
    },
    {
      POSITION: [12, 5, 1.2, 8, 3, 300, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 300, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.slow, g.slow]),
        TYPE: exports.flare1
      }
    }
  ]
};
exports.blaze = {
  PARENT: [exports.genericTank],
  LABEL: "Blazer",
  GUNS: [
    {
      POSITION: [12, 5, 1.2, 8, 3, 0, 0]
    },
    {
      POSITION: [17, 9, 0.7, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.halfreload, g.halfreload]),
        TYPE: exports.spark
      }
    }
  ]
};
exports.bonfire = {
  PARENT: [exports.genericTank],
  LABEL: "Bonfire",
  GUNS: [
    {
      POSITION: [12, 5, 1.2, 8, 3, 0, 0]
    },
    {
      POSITION: [17, 7, 1.2, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.halfreload, g.halfreload]),
        TYPE: exports.bonfbult
      }
    },
    {
      POSITION: [15, 5, 1.2, 8, 0, 0, 0]
    }
  ]
};
exports.smoke = {
  PARENT: [exports.genericTank],
  LABEL: "Smokescreen",
  GUNS: [
    {
      POSITION: [22, 3.5, 1.2, 8, 3, 0, 0]
    },
    {
      POSITION: [27, 5, 1.2, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.smokescreen,
          g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.flare3
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 0, 0]
    }
  ]
};
exports.bunsen = {
  PARENT: [exports.genericTank],
  LABEL: "Bunsen Burner",
  GUNS: [
    {
      POSITION: [12, 5, 0.5, 8, 3, 0, 0]
    },
    {
      POSITION: [17, 7, 0.5, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.doublereload,
          g.doublereload,
          g.doublereload,
          g.lowpower,
          g.norecoil
        ]),
        TYPE: exports.flare2
      }
    }
  ]
};
exports.btorch = {
  PARENT: [exports.genericTank],
  LABEL: "Blow Torch",
  GUNS: [
    {
      POSITION: [17, 7, 0.2, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.doublereload,
          g.doublereload,
          g.doublereload,
          g.norecoil,
          g.halfrange
        ]),
        TYPE: exports.flare2
      }
    }
  ]
};
exports.blastf = {
  PARENT: [exports.genericTank],
  LABEL: "Blast Furnace",
  GUNS: [
    {
      POSITION: [15, 5, 1.5, 8, 3, 5, 0]
    },
    {
      POSITION: [17, 7, 1.5, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.halfreload, 
          g.halfreload
        ]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [17, 7, 1.5, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.halfreload, 
          g.halfreload
        ]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [17, 7, 1.5, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.halfreload, 
          g.halfreload
        ]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [17, 7, 1.5, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.halfreload, 
          g.halfreload
        ]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [17, 7, 1.5, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.halfreload, 
          g.halfreload
        ]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [19, 8.5, 1.25, 8, 0, 0, 0],
    },
  ]
};
exports.heatwav = {
  PARENT: [exports.genericTank],
  LABEL: "Heat Wave",
  GUNS: [
    {
      POSITION: [15, 5, 1.5, 8, 3, 5, 0]
    },
    {
      POSITION: [17, 7, 1.5, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.halfreload, g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [17, 7, 1.5, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.halfreload, g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [17, 7, 1.5, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.halfreload, g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [17, 7, 1.5, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.halfreload, g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [17, 7, 1.5, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.halfreload, g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [17, 7, 1.5, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.halfreload, g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [17, 7, 1.5, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.halfreload, g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [17, 7, 1.5, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.halfreload, g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [17, 7, 1.5, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.halfreload, g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [17, 7, 1.5, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.halfreload, g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [17, 7, 1.5, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.halfreload, g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [17, 7, 1.5, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.halfreload, g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [17, 7, 1.5, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.halfreload, g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [17, 7, 1.5, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.halfreload, g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [17, 7, 1.5, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.halfreload, g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.flare1
      }
    }, {
      POSITION: [22, 10, 1.25, 8, 0, 0, 0],
    },
  ]
};
exports.aspect = {
  PARENT: [exports.genericTank],
  LABEL: "aspect",
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [12, 10, -2, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.destroy]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.spray = {
  PARENT: [exports.genericTank],
  LABEL: "Sprayer",
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [23, 7, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.lowpower,
          g.mach,
          g.morerecoil
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [12, 10, 1.4, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach]),
        TYPE: exports.bullet
      }
    }
  ]
};

exports.mini = {
  PARENT: [exports.genericTank],
  LABEL: "Minigun",
  DANGER: 6,
  BODY: {
    FOV: 1.2
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [20, 8, 1, 0, 0, 0, 0.333],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 0, 0.667],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.uzi = {
  PARENT: [exports.genericTank],
  LABEL: "Uzi",
  DANGER: 6,
  BODY: {
    FOV: 1.2
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.rain = {
  PARENT: [exports.genericTank],
  LABEL: "Rain",
  DANGER: 7,
  BODY: {
    FOV: 1.2
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 8, 1.5, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini, g.mach]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 8, 1.5, 0, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini, g.mach]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.downpour = {
  PARENT: [exports.genericTank],
  LABEL: "Downpour",
  DANGER: 7,
  BODY: {
    FOV: 1.2
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 8, 1.5, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini, g.mach]),
        TYPE: exports.bullet
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 8, 1.5, 0, 0, 0, 0.33],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini, g.mach]),
        TYPE: exports.bullet
      }
    }, 
    {
      POSITION: [18, 8, 1.5, 0, 0, 0, 0.67],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini, g.mach]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [16, 12, 1.5, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.mach,
          g.pound,
          g.morespeed,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [14, 12, 1.5, 0, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.mach,
          g.pound,
          g.morespeed,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [24, 3, 2, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.mach,
          g.morespeed,
          g.morespeed,
          g.halfreload,
          g.halfdamage,
          g.doublesize
        ]),
        TYPE: exports.bullet
      }
    }, 
  ]
};
exports.torrent = {
  PARENT: [exports.genericTank],
  LABEL: "Torrent",
  DANGER: 7,
  BODY: {
    FOV: 1.2
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [24, 8, 1.5, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini, g.mach]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [22, 8, 1.5, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini, g.mach]),
        TYPE: exports.bullet
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 8, 1.5, 0, 0, 0, 0.33],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini, g.mach]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 8, 1.5, 0, 0, 0, 0.33],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini, g.mach]),
        TYPE: exports.bullet
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [16, 8, 1.5, 0, 0, 0, 0.67],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini, g.mach]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [14, 8, 1.5, 0, 0, 0, 0.67],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini, g.mach]),
        TYPE: exports.bullet
      }
    }, 
    {
      POSITION: [12, 12, 1.5, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.mach,
          g.pound,
          g.morespeed,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [10, 12, 1.5, 0, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.mach,
          g.pound,
          g.morespeed,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [26, 3, 2, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.mach,
          g.morespeed,
          g.morespeed,
          g.halfreload,
          g.halfdamage,
          g.doublesize
        ]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [13, 3, 2, 0, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.mach,
          g.morespeed,
          g.morespeed,
          g.halfreload,
          g.halfdamage,
          g.doublesize
        ]),
        TYPE: exports.bullet
      }
    }, 
  ]
};
exports.torrentialdownpour = {
  PARENT: [exports.genericTank],
  LABEL: "Torrential Downpour",
  DANGER: 7,
  FACING_TYPE: 'autospin',
  BODY: {
    FOV: 1.2
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 8, 1.5, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini, g.mach]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [20, 8, 1.5, 0, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini, g.mach]),
        TYPE: exports.bullet
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1.5, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini, g.mach]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [16, 8, 1.5, 0, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini, g.mach]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [14, 12, 1.5, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.mach,
          g.pound,
          g.morespeed,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [12, 12, 1.5, 0, 0, 0, 0.33],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.mach,
          g.pound,
          g.morespeed,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [10, 12, 1.5, 0, 0, 0, 0.67],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.mach,
          g.pound,
          g.morespeed,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 8, 1.5, 0, 0, 120, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini, g.mach]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [20, 8, 1.5, 0, 0, 120, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini, g.mach]),
        TYPE: exports.bullet
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1.5, 0, 0, 120, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini, g.mach]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [16, 8, 1.5, 0, 0, 120, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini, g.mach]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [14, 12, 1.5, 0, 0, 120, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.mach,
          g.pound,
          g.morespeed,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [12, 12, 1.5, 0, 0, 120, 0.33],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.mach,
          g.pound,
          g.morespeed,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [10, 12, 1.5, 0, 0, 120, 0.67],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.mach,
          g.pound,
          g.morespeed,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [22, 8, 1.5, 0, 0, 240, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini, g.mach]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [20, 8, 1.5, 0, 0, 240, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini, g.mach]),
        TYPE: exports.bullet
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1.5, 0, 0, 240, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini, g.mach]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [16, 8, 1.5, 0, 0, 240, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini, g.mach]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [14, 12, 1.5, 0, 0, 240, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.mach,
          g.pound,
          g.morespeed,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [12, 12, 1.5, 0, 0, 240, 0.33],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.mach,
          g.pound,
          g.morespeed,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    }, {
      POSITION: [10, 12, 1.5, 0, 0, 240, 0.67],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.mach,
          g.pound,
          g.morespeed,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    }, 
  ],
  TURRETS: [
    {
      /*  SIZE     X       Y     ANGLE    ARC */
      POSITION: [14, 6, 0, 180, 190, 0],
      TYPE: [exports.lazerturr, { COLOR: 1 }]
    },
    {
      POSITION: [14, 6, 0, 60, 190, 0],
      TYPE: [exports.lazerturr, { COLOR: 5 }]
    }, 
    {
      POSITION: [14, 6, 0, -60, 190, 0],
      TYPE: [exports.lazerturr, { COLOR: 9 }]
    }, 
    {
      POSITION: [14, 6, 100, 0, 190, 0],
      TYPE: [exports.lazerturr2, { COLOR: 13 }]
    }, 
    {
      POSITION: [14, 6, 100, 120, 190, 0],
      TYPE: [exports.lazerturr2, { COLOR: 15 }]
    }, 
    {
      POSITION: [14, 6, 100, 240, 190, 0],
      TYPE: [exports.lazerturr2, { COLOR: 17 }]
    }
  ]
};
exports.railgun = {
  PARENT: [exports.genericTank],
  LABEL: "Railgun",
  DANGER: 6,
  BODY: {
    FOV: 2
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [27, 8, 1.2, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.morespeed,
          g.destroy,
          g.morespeed
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [23, 8, 1.2, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.morespeed,
          g.destroy,
          g.morespeed
        ]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.blastg = {
  PARENT: [exports.genericTank],
  LABEL: "Blast Railgun",
  DANGER: 6,
  BODY: {
    FOV: 2
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [30, 8, 1.1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.morespeed,
          g.destroy,
          g.morespeed,
          g.mach,
          g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [26, 8, 1.2, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.morespeed,
          g.destroy,
          g.morespeed,
          g.mach,
          g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [23, 8, 1.3, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.morespeed,
          g.destroy,
          g.morespeed,
          g.mach,
          g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [20, 8, 1.4, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.morespeed,
          g.destroy,
          g.morespeed,
          g.mach,
          g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.duobarrel = {
  PARENT: [exports.genericTank],
  LABEL: "Double Barrel",
  DANGER: 6,
  BODY: {
    FOV: 2
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [30, 8, 1.3, 0, 7, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.morespeed,
          g.destroy,
          g.morespeed
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [30, 8, 1.3, 0, -7, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.morespeed,
          g.destroy,
          g.morespeed
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [26, 8, 1.3, 0, 7, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.morespeed,
          g.destroy,
          g.morespeed
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [26, 8, 1.3, 0, -7, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mini,
          g.morespeed,
          g.destroy,
          g.morespeed
        ]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.stream = {
  PARENT: [exports.genericTank],
  LABEL: "Streamliner",
  DANGER: 7,
  BODY: {
    FOV: 1.3
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [25, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini, g.stream]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [23, 8, 1, 0, 0, 0, 0.2],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini, g.stream]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [21, 8, 1, 0, 0, 0, 0.4],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini, g.stream]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [19, 8, 1, 0, 0, 0, 0.6],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini, g.stream]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [17, 8, 1, 0, 0, 0, 0.8],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini, g.stream]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.hybridmini = makeHybrid(exports.mini, "Crop Duster");
exports.minitrap = {
  PARENT: [exports.genericTank],
  DANGER: 6,
  LABEL: "",
  STAT_NAMES: statnames.trap,
  BODY: {
    FOV: 1.15
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */

      POSITION: [24, 8, 1, 0, 0, 0, 0]
    },
    {
      POSITION: [4, 8, 1.3, 22, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap, g.mini, g.halfrange]),
        TYPE: exports.trap,
        STAT_CALCULATOR: gunCalcNames.trap
      }
    },
    {
      POSITION: [4, 8, 1.3, 18, 0, 0, 0.333],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap, g.mini, g.halfrange]),
        TYPE: exports.trap,
        STAT_CALCULATOR: gunCalcNames.trap
      }
    },
    {
      POSITION: [4, 8, 1.3, 14, 0, 0, 0.667],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap, g.mini, g.halfrange]),
        TYPE: exports.trap,
        STAT_CALCULATOR: gunCalcNames.trap
      }
    }
  ]
};

exports.pound = {
  PARENT: [exports.genericTank],
  DANGER: 5,
  BODY: {
    ACCELERATION: base.ACCEL * 0.8
  },
  LABEL: "Pounder",
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 12, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.destroy = {
  PARENT: [exports.genericTank],
  DANGER: 6,
  BODY: {
    ACCELERATION: base.ACCEL * 0.75
  },
  LABEL: "Destroyer",
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [21, 14, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound, g.destroy]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.thermonuke = {
  PARENT: [exports.genericTank],
  DANGER: 6,
  BODY: {
    ACCELERATION: base.ACCEL * 1.3,
    SPEED: base.SPEED * 1.3,
  },
  LABEL: "Desicrater",
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [21, 21, 1.4, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound, g.destroy, g.doubledamage, g.morerecoil]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.anni = {
  PARENT: [exports.genericTank],
  BODY: {
    ACCELERATION: base.ACCEL * 0.75
  },
  LABEL: "Annihilator",
  DANGER: 7,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20.5, 19.5, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound, g.destroy, g.anni]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.beeyeet = {
  PARENT: [exports.genericTank],
  DANGER: 6,
  BODY: {
    ACCELERATION: base.ACCEL * 0.75,
    SPEED: base.speed * 0.8
  },
  LABEL: "Bee Yeeter",
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [14, 14, -1.2, 5, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound, g.destroy, g.hive]),
        TYPE: exports.hive
      }
    },
    {
      POSITION: [15, 12, 1, 5, 0, 0, 0]
    }
  ]
};
exports.hybrid = makeHybrid(exports.destroy, "Hybrid");
exports.shotgun2 = {
  PARENT: [exports.genericTank],
  DANGER: 7,
  LABEL: "Shotgun",
  BODY: {
    ACCELERATION: base.ACCEL * 0.7
  },
  GUNS: [
    /***** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */ {
      POSITION: [4, 3, 1, 11, -3, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [4, 3, 1, 11, 3, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [4, 4, 1, 13, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [1, 4, 1, 12, -1, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [1, 4, 1, 11, 1, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [1, 3, 1, 13, -1, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [1, 3, 1, 13, 1, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [1, 2, 1, 13, 2, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [1, 2, 1, 13, -2, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [15, 14, 1, 6, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun, g.fake]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [8, 14, -1.3, 4, 0, 0, 0]
    }
  ]
};
exports.machshot = {
  PARENT: [exports.genericTank],
  DANGER: 7,
  LABEL: "Spraygun",
  BODY: {
    ACCELERATION: base.ACCEL * 0.7
  },
  GUNS: [
    /***** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */ {
      POSITION: [4, 3, 1, 11, -3, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun, g.doublereload]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [4, 3, 1, 11, 3, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun, g.doublereload]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [4, 4, 1, 13, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun, g.doublereload]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [1, 4, 1, 12, -1, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun, g.doublereload]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [1, 4, 1, 11, 1, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun, g.doublereload]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [1, 3, 1, 13, -1, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun, g.doublereload]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [1, 3, 1, 13, 1, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun, g.doublereload]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [1, 2, 1, 13, 2, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun, g.doublereload]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [1, 2, 1, 13, -2, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun, g.doublereload]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [15, 14, 1.5, 6, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun, g.fake, g.doublereload]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [8, 14, -1.3, 4, 0, 0, 0]
    }
  ]
};
exports.canspray = {
  PARENT: [exports.genericTank],
  DANGER: 7,
  LABEL: "Spraycan",
  BODY: {
    ACCELERATION: base.ACCEL * 0.7
  },
  GUNS: [
    /***** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */ {
      POSITION: [4, 3, 1, 11, -3, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun, g.doublereload]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [4, 3, 1, 11, 3, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun, g.doublereload, g.doublereload, g.doublereload]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [4, 4, 1, 13, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun, g.doublereload, g.doublereload, g.doublereload]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [12, 7, 1.3, 6, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun, g.fake, g.doublereload, g.doublereload, g.doublereload]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [6.5, 7, -1.3, 4, 0, 0, 0]
    }
  ]
};
exports.doomsl = {
  PARENT: [exports.genericTank],
  DANGER: 7,
  LABEL: "Doomslayer",
  BODY: {
    ACCELERATION: base.ACCEL * 0.7,
    SPEED: base.SPEED * 1.5,
    FOV: base.FOV * 1.5
  },
  GUNS: [
    /***** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */ {
      POSITION: [4, 1.5, 1, 11, -7, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [4, 1.5, 1, 11, -1, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [4, 2, 1, 13, -4, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [1, 2, 1, 12, -5, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [1, 2, 1, 11, -3, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [1, 1.5, 1, 13, -5, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [1, 1.5, 1, 13, -3, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [1, 1, 1, 13, -2, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [1, 1, 1, 13, -6, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [15, 7, 1, 6, -5, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun, g.fake]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [8, 7, -1.3, 4, -5, 0, 0]
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 7, 0.8, 0, 5, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.fast, g.fast, g.bigbertha, g.halfdamage, g.halfdamage, g.halfdamage, g.halfdamage, g.mach, g.doublereload, g.doublereload]),
        TYPE: exports.bullet,
        ALT_FIRE: true
      }
    },
    {
      POSITION: [16, 8.25, 0.85, 0, 5, 0, 0]
    }
  ]
};
exports.burstgun = {
  PARENT: [exports.genericTank],
  DANGER: 7,
  LABEL: "Burst Gun",
  BODY: {
    ACCELERATION: base.ACCEL * 0.7
  },
  GUNS: [
    /***** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */ {
      POSITION: [4, 4, 1, 13, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [1, 4, 1, 12, -1, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [1, 4, 1, 11, 1, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [1, 3, 1, 13, -1, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [1, 3, 1, 13, 1, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [1, 2, 1, 13, 2, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [15, 14, 0.7, 6, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun, g.fake]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [8, 14, -1.3, 4, 0, 0, 0]
    }
  ]
};
exports.blastgun = {
  PARENT: [exports.genericTank],
  DANGER: 7,
  LABEL: "Blast Gun",
  BODY: {
    ACCELERATION: base.ACCEL * 0.7
  },
  GUNS: [
    /***** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */ {
      POSITION: [4, 4, 1, 13, 0, 10, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [1, 4, 1, 12, 0, 10, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [1, 4, 1, 11, 0, 10, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [1, 3, 1, 13, 0, -10, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [1, 3, 1, 13, 0, -10, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [1, 2, 1, 13, 0, -10, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [15, 12, 1.3, 4, 0, 10, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun, g.fake]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [15, 12, 1.3, 4, 0, -10, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun, g.fake]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [15, 12, 1.3, 6, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.pound,
          g.morespeed,
          g.morespeed
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [8, 14, -1.3, 4, 0, 0, 0]
    }
  ]
};
exports.eleg = {
  PARENT: [exports.genericTank],
  DANGER: 7,
  LABEL: "Elephant Gun",
  BODY: {
    ACCELERATION: base.ACCEL * 0.7
  },
  GUNS: [
    /***** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */ {
      POSITION: [4, 4, 1, 13, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [1, 4, 1, 12, -1, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [1, 4, 1, 11, 1, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [1, 3, 1, 13, -1, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [1, 3, 1, 13, 1, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [1, 2, 1, 13, 2, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [25, 14, 0.7, 6, 7, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun, g.fake]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [8, 14, -1.3, 4, 7, 0, 0]
    },
    {
      POSITION: [4, 4, 1, 13, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [1, 4, 1, 12, -1, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [1, 4, 1, 11, 1, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [1, 3, 1, 13, -1, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [1, 3, 1, 13, 1, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [1, 2, 1, 13, 2, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [25, 14, 0.7, 6, -7, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.shotgun, g.fake]),
        TYPE: exports.casing
      }
    },
    {
      POSITION: [8, 14, -1.3, 4, -7, 0, 0]
    }
  ]
};
exports.builder = {
  PARENT: [exports.genericTank],
  DANGER: 6,
  LABEL: "Builder",
  STAT_NAMES: statnames.trap,
  BODY: {
    SPEED: base.SPEED * 0.8,
    FOV: base.FOV * 1.15
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 12, 1, 0, 0, 0, 0]
    },
    {
      POSITION: [2, 12, 1.1, 18, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap, g.block]),
        TYPE: exports.block
      }
    }
  ]
};
exports.producer = {
  PARENT: [exports.genericTank],
  DANGER: 7,
  LABEL: "Producer",
  STAT_NAMES: statnames.trap,
  BODY: {
    SPEED: base.SPEED * 0.75,
    FOV: base.FOV * 1.15
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [5, 11, 1, 10.5, 0, 0, 0]
    },
    {
      POSITION: [3, 14, 1, 15.5, 0, 0, 0]
    },
    {
      POSITION: [2, 14, 1.3, 18, 0, 0, 0],
      PROPERTIES: {
        MAX_CHILDREN: 4,
        SHOOT_SETTINGS: combineStats([g.trap, g.block]),
        TYPE: exports.pillbox,
        SYNCS_SKILLS: true
      }
    },
    {
      POSITION: [4, 14, 1, 8, 0, 0, 0]
    }
  ]
};
exports.construct = {
  PARENT: [exports.genericTank],
  LABEL: "Constructor",
  STAT_NAMES: statnames.trap,
  DANGER: 7,
  BODY: {
    ACCELERATION: base.ACCEL * 0.5,
    SPEED: base.SPEED * 0.7,
    FOV: base.FOV * 1.15
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 18, 1, 0, 0, 0, 0]
    },
    {
      POSITION: [2, 18, 1.2, 18, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap, g.block, g.construct]),
        TYPE: exports.block
      }
    }
  ]
};
exports.autobuilder = makeAuto(exports.builder);
exports.conq = {
  PARENT: [exports.genericTank],
  DANGER: 7,
  LABEL: "",
  STAT_NAMES: statnames.trap,
  BODY: {
    SPEED: base.SPEED * 0.8
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [21, 14, 1, 0, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 14, 1, 0, 0, 0, 0]
    },
    {
      POSITION: [2, 14, 1.1, 18, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap, g.block]),
        TYPE: exports.block
      }
    }
  ]
};
exports.bentboomer = {
  PARENT: [exports.genericTank],
  DANGER: 7,
  LABEL: "Boomer",
  STAT_NAMES: statnames.trap,
  BODY: {
    SPEED: base.SPEED * 0.8,
    FOV: base.FOV * 1.15
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [8, 10, 1, 8, -2, -35, 0]
    },
    {
      POSITION: [8, 10, 1, 8, 2, 35, 0]
    },
    {
      POSITION: [2, 10, 1.3, 16, -2, -35, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap, g.block, g.fast, g.twin]),
        TYPE: exports.boomerang
      }
    },
    {
      POSITION: [2, 10, 1.3, 16, 2, 35, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap, g.block, g.fast, g.twin]),
        TYPE: exports.boomerang
      }
    }
  ]
};
exports.boomer = {
  PARENT: [exports.genericTank],
  DANGER: 7,
  LABEL: "Boomer",
  STAT_NAMES: statnames.trap,
  FACING_TYPE: "locksFacing",
  BODY: {
    SPEED: base.SPEED * 0.8,
    FOV: base.FOV * 1.15
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [5, 10, 1, 14, 0, 0, 0]
    },
    {
      POSITION: [6, 10, -1.5, 7, 0, 0, 0]
    },
    {
      //POSITION: [  12,    15,      1,      0,      0,      0,      0,   ],
      //    }, {
      POSITION: [2, 10, 1.3, 18, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap, g.block, g.boomerang]),
        TYPE: exports.boomerang
      }
    }
  ]
};
exports.quadtrapper = {
  PARENT: [exports.genericTank],
  DANGER: 7,
  LABEL: "",
  STAT_NAMES: statnames.trap,
  BODY: {
    SPEED: base.SPEED * 0.8,
    FOV: base.FOV * 1.15
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [14, 6, 1, 0, 0, 45, 0]
    },
    {
      POSITION: [2, 6, 1.1, 14, 0, 45, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap, g.block, g.weak]),
        TYPE: exports.block
      }
    },
    {
      POSITION: [14, 6, 1, 0, 0, 135, 0]
    },
    {
      POSITION: [2, 6, 1.1, 14, 0, 135, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap, g.block, g.weak]),
        TYPE: exports.block
      }
    },
    {
      POSITION: [14, 6, 1, 0, 0, 225, 0]
    },
    {
      POSITION: [2, 6, 1.1, 14, 0, 225, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap, g.block, g.weak]),
        TYPE: exports.block
      }
    },
    {
      POSITION: [14, 6, 1, 0, 0, 315, 0]
    },
    {
      POSITION: [2, 6, 1.1, 14, 0, 315, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap, g.block, g.weak]),
        TYPE: exports.block
      }
    }
  ]
};

exports.artillery = {
  PARENT: [exports.genericTank],
  DANGER: 6,
  LABEL: "Artillery",
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [17, 3, 1, 0, -6, -7, 0.25],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.gunner, g.arty]),
        TYPE: exports.bullet,
        LABEL: "Secondary"
      }
    },
    {
      POSITION: [17, 3, 1, 0, 6, 7, 0.75],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.gunner, g.arty]),
        TYPE: exports.bullet,
        LABEL: "Secondary"
      }
    },
    {
      POSITION: [19, 12, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound, g.arty]),
        TYPE: exports.bullet,
        LABEL: "Heavy"
      }
    }
  ]
};
exports.mortar = {
  PARENT: [exports.genericTank],
  LABEL: "Mortar",
  DANGER: 7,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [13, 3, 1, 0, -8, -7, 0.6],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.gunner, g.arty, g.twin]),
        TYPE: exports.bullet,
        LABEL: "Secondary"
      }
    },
    {
      POSITION: [13, 3, 1, 0, 8, 7, 0.8],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.gunner, g.arty, g.twin]),
        TYPE: exports.bullet,
        LABEL: "Secondary"
      }
    },
    {
      POSITION: [17, 3, 1, 0, -6, -7, 0.2],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.gunner, g.arty, g.twin]),
        TYPE: exports.bullet,
        LABEL: "Secondary"
      }
    },
    {
      POSITION: [17, 3, 1, 0, 6, 7, 0.4],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.gunner, g.arty, g.twin]),
        TYPE: exports.bullet,
        LABEL: "Secondary"
      }
    },
    {
      POSITION: [19, 12, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound, g.arty]),
        TYPE: exports.bullet,
        LABEL: "Heavy"
      }
    }
  ]
};
exports.skimmer = {
  PARENT: [exports.genericTank],
  BODY: {
    FOV: base.FOV * 1.15
  },
  LABEL: "Skimmer",
  DANGER: 7,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [10, 14, -0.5, 9, 0, 0, 0]
    },
    {
      POSITION: [17, 15, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.pound,
          g.arty,
          g.arty,
          g.skim
        ]),
        TYPE: exports.missile,
        STAT_CALCULATOR: gunCalcNames.sustained
      }
    }
  ]
};
exports.spinner = {
  PARENT: [exports.genericTank],
  BODY: {
    FOV: base.FOV * 1.15
  },
  LABEL: "Spinner",
  DANGER: 7,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [10, 14, -0.5, 9, 0, 0, 0]
    },
    {
      POSITION: [17, 15, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.pound,
          g.arty,
          g.arty,
          g.skim
        ]),
        TYPE: exports.spin
      }
    }
  ]
};
exports.launch = {
  PARENT: [exports.genericTank],
  BODY: {
    FOV: base.FOV * 1.075
  },
  LABEL: "Launcher",
  DANGER: 7,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [12, 13, -0.5, 4, 0, 0, 0]
    },
    {
      POSITION: [10, 14, 1, 4, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound, g.skim]),
        TYPE: exports.lilmiss
      }
    }
  ]
};
exports.pentaknox = { 
PARENT: [exports.genericTank], LABEL: 'Pentaknox', DANGER: 7, BODY: { 
FOV: base.FOV * 1.1, SPEED: base.SPEED * 0.9, }, GUNS: [ { /*** LENGTH WIDTH ASPECT X Y ANGLE DELAY */ 
POSITION: [ 19, 17, 1, 0, -2.5, 0, 1.25, ], 
PROPERTIES: { 
SHOOT_SETTINGS: combineStats([g.basic, g.gunner, g.power, g.twin, g.nail]), 
TYPE: exports.pentagon, }, }, { POSITION: [ 19, 17, 1, 0, 2.5, 0, 1.75, ], 
PROPERTIES: { 
SHOOT_SETTINGS: combineStats([g.basic, g.gunner, g.power, g.twin, g.nail]), 
TYPE: exports.pentagon, }, }, { POSITION: [ 20, 17, 1, 0, 0, 0, 2, ], 
PROPERTIES: { 
SHOOT_SETTINGS: combineStats([g.basic, g.gunner, g.power, g.twin, g.nail]), 
TYPE: exports.pentagon, }, }, { POSITION: [ 5.5, 17, -1.8, 6.5, 0, 0, 1.25, ], 
}, ], }; 

exports.fireworks = {
  PARENT: [exports.genericTank],
  BODY: {
    FOV: base.FOV * 1.075
  },
  LABEL: "Fireworks",
  DANGER: 7,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [12, 13, -1.5, 4, 0, 0, 0]
    },
    {
      POSITION: [10, 14, 1, 4, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound, g.skim]),
        TYPE: exports.firework
      }
    }
  ]
};
exports.wingm = {
  PARENT: [exports.genericTank],
  BODY: {
    FOV: base.FOV * 1.5
  },
  LABEL: "Wingman",
  DANGER: 7,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [12, 13, -0.5, 4, 0, 0, 0]
    },
    {
      POSITION: [10, 14, 0.75, 4, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound, g.tempdrone]),
        TYPE: exports.wing,
        MAX_CHILDREN: 1
      }
    }
  ]
}; exports.carriership = {
  PARENT: [exports.genericTank],
  BODY: {
    FOV: base.FOV * 1.5,
  },
  LABEL: "Carrier Ship",
  DANGER: 7,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [12, 13, -0.5, 4, 0, 30, 0]
    },
    {
      POSITION: [10, 14, 0.75, 4, 0, 30, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound, g.tempdrone]),
        TYPE: exports.wing,
        MAX_CHILDREN: 1
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [12, 13, -0.5, 4, 0, -30, 0]
    },
    {
      POSITION: [10, 14, 0.75, 4, 0, -30, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound, g.tempdrone]),
        TYPE: exports.wing,
        MAX_CHILDREN: 1
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [12, 13, -0.5, 4, 0, 0, 0.5]
    },
    {
      POSITION: [10, 14, 0.75, 4, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound, g.tempdrone]),
        TYPE: exports.wing,
        MAX_CHILDREN: 1
      }
    }
  ]
};
exports.apached = {
  PARENT: [exports.genericTank],
  BODY: {
    FOV: base.FOV * 1.5
  },
  LABEL: "Apache Dock",
  DANGER: 10,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [12, 13, 1, 4, 0, 0, 0]
    },
    {
      POSITION: [10, 14, 0.75, 4, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound, g.tempdrone]),
        TYPE: exports.apache,
        MAX_CHILDREN: 1
      }
    }
  ]
};
exports.beastm = {
  PARENT: [exports.genericTank],
  BODY: {
    FOV: base.FOV * 1.5
  },
  LABEL: "Beast Master",
  DANGER: 7,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [12, 13, -0.7, 4, 0, 0, 0]
    },
    {
      POSITION: [10, 14, 0.75, 4, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound, g.tempdrone]),
        TYPE: exports.charger,
        MAX_CHILDREN: 3
      }
    }
  ]
};
exports.airbase = {
  PARENT: [exports.genericTank],
  BODY: {
    FOV: base.FOV * 1.5
  },
  LABEL: "Air Base",
  DANGER: 7,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [12, 13, -0.5, 4, 0, 0, 0]
    },
    {
      POSITION: [10, 14, 0.75, 4, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound, g.tempdrone]),
        TYPE: exports.fjet,
        MAX_CHILDREN: 1
      }
    },
    {
      POSITION: [12, 13, -0.3, 4, 0, 90, 0]
    },
    {
      POSITION: [10, 14, 0.75, 4, 0, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound, g.tempdrone]),
        TYPE: exports.chinook,
        MAX_CHILDREN: 1
      }
    },
    {
      POSITION: [12, 13, -0.5, 4, 0, 180, 0]
    },
    {
      POSITION: [10, 14, 0.75, 4, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound, g.tempdrone]),
        TYPE: exports.fjet,
        MAX_CHILDREN: 1
      }
    },
    {
      POSITION: [12, 13, -0.3, 4, 0, 270, 0]
    },
    {
      POSITION: [10, 14, 0.75, 4, 0, 270, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound, g.tempdrone]),
        TYPE: exports.chinook,
        MAX_CHILDREN: 1
      }
    }
  ]
};
exports.arsenal = {
  PARENT: [exports.genericTank],
  BODY: {
    FOV: base.FOV * 1.075
  },
  LABEL: "Arsenal",
  DANGER: 7,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [12, 6.5, -0.5, 4, 3.25, 0, 0]
    },
    {
      POSITION: [10, 7, 1, 4, 4.5, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound, g.skim]),
        TYPE: exports.lilmiss
      }
    },
    {
      POSITION: [12, 6.5, -0.5, 4, -3.25, 0, 0]
    },
    {
      POSITION: [10, 7, 1, 4, -4.5, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound, g.skim]),
        TYPE: exports.lilmiss
      }
    }
  ]
};
exports.spread = {
  PARENT: [exports.genericTank],
  LABEL: "Spreadshot",
  DANGER: 7,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [13, 4, 1, 0, -0.8, -75, 5 / 6],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.arty,
          g.twin,
          g.spread
        ]),
        TYPE: exports.bullet,
        LABEL: "Spread"
      }
    },
    {
      POSITION: [14.5, 4, 1, 0, -1.0, -60, 4 / 6],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.arty,
          g.twin,
          g.spread
        ]),
        TYPE: exports.bullet,
        LABEL: "Spread"
      }
    },
    {
      POSITION: [16, 4, 1, 0, -1.6, -45, 3 / 6],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.arty,
          g.twin,
          g.spread
        ]),
        TYPE: exports.bullet,
        LABEL: "Spread"
      }
    },
    {
      POSITION: [17.5, 4, 1, 0, -2.4, -30, 2 / 6],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.arty,
          g.twin,
          g.spread
        ]),
        TYPE: exports.bullet,
        LABEL: "Spread"
      }
    },
    {
      POSITION: [19, 4, 1, 0, -3.0, -15, 1 / 6],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.arty,
          g.twin,
          g.spread
        ]),
        TYPE: exports.bullet,
        LABEL: "Spread"
      }
    },
    {
      POSITION: [13, 4, 1, 0, 0.8, 75, 5 / 6],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.arty,
          g.twin,
          g.spread
        ]),
        TYPE: exports.bullet,
        LABEL: "Spread"
      }
    },
    {
      POSITION: [14.5, 4, 1, 0, 1.0, 60, 4 / 6],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.arty,
          g.twin,
          g.spread
        ]),
        TYPE: exports.bullet,
        LABEL: "Spread"
      }
    },
    {
      POSITION: [16, 4, 1, 0, 1.6, 45, 3 / 6],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.arty,
          g.twin,
          g.spread
        ]),
        TYPE: exports.bullet,
        LABEL: "Spread"
      }
    },
    {
      POSITION: [17.5, 4, 1, 0, 2.4, 30, 2 / 6],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.arty,
          g.twin,
          g.spread
        ]),
        TYPE: exports.bullet,
        LABEL: "Spread"
      }
    },
    {
      POSITION: [19, 4, 1, 0, 3.0, 15, 1 / 6],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.arty,
          g.twin,
          g.spread
        ]),
        TYPE: exports.bullet,
        LABEL: "Spread"
      }
    },
    {
      POSITION: [13, 10, 1.3, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.pound,
          g.spreadmain,
          g.spread
        ]),
        TYPE: exports.bullet,
        LABEL: "Pounder"
      }
    }
  ]
};

exports.flank = {
  PARENT: [exports.genericTank],
  LABEL: "Flank Guard",
  BODY: {
    SPEED: base.SPEED * 2.1
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 120, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 240, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.dustdevil = {
  PARENT: [exports.genericTank],
  LABEL: "Dust Devil",
  FACING_TYPE: "autospin",
  BODY: {
    SPEED: base.SPEED * 1.1
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [10, 10, 1, 0, 15, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.spam]),
        TYPE: exports.bullet,
        AUTOFIRE: true
      }
    },
    {
      POSITION: [10, 10, 1, 0, 15, -90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.spam]),
        TYPE: exports.bullet,
        AUTOFIRE: true
      }
    }
  ]
};
exports.dustc = makeAuto(exports.dustdevil, "Dust-ception", {
  type: exports.dustdevilAutoTurret,
  size: 11
});
exports.firewh = {
  PARENT: [exports.genericTank],
  LABEL: "Fire Whirl",
  FACING_TYPE: "autospin",
  BODY: {
    SPEED: base.SPEED * 1.1
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [12, 10, 1.2, 0, 15, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.spam]),
        TYPE: exports.flare1,
        AUTOFIRE: true
      }
    },
    {
      POSITION: [12, 10, 0.5, 0, 15, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.spam,
          g.doublereload,
          g.doublereload
        ]),
        TYPE: exports.flare2,
        AUTOFIRE: true
      }
    },
    {
      POSITION: [12, 10, 1.2, 0, 15, 270, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.spam]),
        TYPE: exports.flare1,
        AUTOFIRE: true
      }
    },
    {
      POSITION: [12, 10, 0.5, 0, 15, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.spam,
          g.doublereload,
          g.doublereload
        ]),
        TYPE: exports.flare2,
        AUTOFIRE: true
      }
    }
  ]
};
exports.angrbee = {
  PARENT: [exports.genericTank],
  LABEL: "Angry Beehive",
  FACING_TYPE: "autospin",
  SHAPE: 6,
  BODY: {
    SPEED: base.SPEED * 1.1
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [10, 5, 0.75, 0, 15, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.spam, g.doublereload]),
        TYPE: exports.autoswarm,
        AUTOFIRE: true
      }
    },
    {
      POSITION: [10, 5, 0.75, 0, 15, -90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.spam, g.doublereload]),
        TYPE: exports.autoswarm,
        AUTOFIRE: true
      }
    }
  ]
};
exports.windstorm = {
  PARENT: [exports.genericTank],
  LABEL: "Windstorm",
  FACING_TYPE: "autospin",
  BODY: {
    SPEED: base.SPEED * 1.6
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [10, 10, 1.5, 0, 15, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.spam]),
        TYPE: exports.bullet,
        AUTOFIRE: true
      }
    },
    {
      POSITION: [10, 10, 1.5, 0, 15, -90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.spam]),
        TYPE: exports.bullet,
        AUTOFIRE: true
      }
    }
  ]
};
exports.tornado = {
  PARENT: [exports.genericTank],
  LABEL: "tornado",
  FACING_TYPE: "autospin",
  BODY: {
    SPEED: base.SPEED * 1.1
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [10, 10, 1, 0, 15, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.spam]),
        TYPE: exports.bullet,
        AUTOFIRE: true
      }
    },
    {
      POSITION: [10, 10, 1, 0, 15, 270, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.spam]),
        TYPE: exports.bullet,
        AUTOFIRE: true
      }
    },
    {
      POSITION: [10, 10, 1, 0, 15, 180, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.spam]),
        TYPE: exports.bullet,
        AUTOFIRE: true
      }
    },
    {
      POSITION: [10, 10, 1, 0, 15, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.spam]),
        TYPE: exports.bullet,
        AUTOFIRE: true
      }
    }
  ]
};
exports.machnado = {
  PARENT: [exports.genericTank],
  LABEL: "Machnado",
  FACING_TYPE: "autospin",
  BODY: {
    SPEED: base.SPEED * 1.1
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [10, 10, 1.5, 0, 15, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.spam]),
        TYPE: exports.bullet,
        AUTOFIRE: true
      }
    },
    {
      POSITION: [10, 10, 1.5, 0, 15, 270, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.spam]),
        TYPE: exports.bullet,
        AUTOFIRE: true
      }
    },
    {
      POSITION: [10, 10, 1.5, 0, 15, 180, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.spam]),
        TYPE: exports.bullet,
        AUTOFIRE: true
      }
    },
    {
      POSITION: [10, 10, 1.5, 0, 15, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.spam]),
        TYPE: exports.bullet,
        AUTOFIRE: true
      }
    }
  ]
};
exports.Monsoon = {
  PARENT: [exports.genericTank],
  LABEL: "Monsoon",
  FACING_TYPE: "autospin",
  BODY: {
    SPEED: base.SPEED * 1.1
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [5, 5, 1, -5, 15, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.spam,
          g.doublereload
        ]),
        TYPE: exports.bullet,
        AUTOFIRE: true
      }
    },
    {
      POSITION: [5, 5, 1, 5, 15, -90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.spam,
          g.doublereload
        ]),
        TYPE: exports.bullet,
        AUTOFIRE: true
      }
    },
    {
      POSITION: [5, 5, 1, 5, 15, 90, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.spam,
          g.doublereload
        ]),
        TYPE: exports.bullet,
        AUTOFIRE: true
      }
    },
    {
      POSITION: [5, 5, 1, -5, 15, -90, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.spam,
          g.doublereload
        ]),
        TYPE: exports.bullet,
        AUTOFIRE: true
      }
    },
    {
      POSITION: [5, 5, 1, -2.5, 20, 90, 0.25],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.spam,
          g.doublereload
        ]),
        TYPE: exports.bullet,
        AUTOFIRE: true
      }
    },
    {
      POSITION: [5, 5, 1, 2.5, 20, -90, 0.25],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.spam,
          g.doublereload
        ]),
        TYPE: exports.bullet,
        AUTOFIRE: true
      }
    },
    {
      POSITION: [5, 5, 1, 2.5, 10, 90, 0.75],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.spam,
          g.doublereload
        ]),
        TYPE: exports.bullet,
        AUTOFIRE: true
      }
    },
    {
      POSITION: [5, 5, 1, -2.5, 10, -90, 0.75],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.spam,
          g.doublereload
        ]),
        TYPE: exports.bullet,
        AUTOFIRE: true
      }
    }
  ]
};
exports.hexa = {
  PARENT: [exports.genericTank],
  LABEL: "Hexa Tank",
  DANGER: 6,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 120, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 240, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 60, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 180, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 300, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.octo = {
  PARENT: [exports.genericTank],
  LABEL: "Octo Tank",
  DANGER: 7,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.spam]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.spam]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.spam]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 270, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.spam]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 45, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.spam]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 135, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.spam]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 225, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.spam]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 315, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.spam]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.ace = {
  PARENT: [exports.genericTank],
  LABEL: "Ace",
  SIZE: 12,
  SPEED: 7.5,
  DANGER: 7,
  SHAPE: [[0.073,0.47],[0.07,1.14],[-0.05,1.31],[-0.287,1.296],[-0.407,1.11],[-0.41,0.38],[-0.927,0.35],[-1.22,0.56],[-1.49,0.596],[-1.5,-0.584],[-1.25,-0.55],[-0.95,-0.344],[-0.43,-0.4],[-0.42,-1.13],[-0.25,-1.28],[-0.03,-1.28],[0.06,-1.09],[0.06,-0.4],[0.99,-0.27],[1.46,0.02],[0.95,0.396]],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.dart
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.dart
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.dart
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 270, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.dart
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 45, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.dart
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 135, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.dart
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 225, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.dart
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 315, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.dart
      }
    }
  ]
};
exports.sace = {
  PARENT: [exports.genericTank],
  LABEL: "Spectre",
  SIZE: 20,
  SPEED: 7.5,
  DANGER: 7,
  SHAPE: [[0.073,0.47],[0.07,1.14],[-0.05,1.31],[-0.287,1.296],[-0.407,1.11],[-0.41,0.38],[-0.927,0.35],[-1.22,0.56],[-1.49,0.596],[-1.5,-0.584],[-1.25,-0.55],[-0.95,-0.344],[-0.43,-0.4],[-0.42,-1.13],[-0.25,-1.28],[-0.03,-1.28],[0.06,-1.09],[0.06,-0.4],[0.99,-0.27],[1.46,0.02],[0.95,0.396]],
  TURRETS: [
    {
      /** SIZE     X       Y     ANGLE    ARC */
      POSITION: [5, 0, 0, 0, 360, 0],
      TYPE: exports.spectregun
    },
  ]
};
exports.flyfort = {
  PARENT: [exports.genericTank],
  LABEL: "Flying Fortress",
  SIZE: 45,
  SPEED: 8.5,
  DANGER: 20,
  HEALTH: 1000,
  FOV: 3,
  SHAPE: [[0.83,0.62],[0.74,2.73],[0.31,2.956],[-0.233,2.94],[-0.55,2.656],[-0.72,0.596],[-1.1,0.63],[-1.45,1.15],[-2.04,1.35],[-2.02,-1.384],[-1.433,-1.044],[-1.06,-0.47],[-0.71,-0.42],[-0.593,-2.75],[-0.27,-2.984],[0.307,-2.98],[0.607,-2.82],[0.89,-0.484],[1.37,-0.47],[1.87,0],[1.36,0.436]],
  TURRETS: [
    {
      /** SIZE     X       Y     ANGLE    ARC */
      POSITION: [5, 0, 0, 0, 360, 0],
      TYPE: exports.flyfortgun
    }, {
      /** SIZE     X       Y     ANGLE    ARC */
      POSITION: [5, 0, 25, 0, 360, 0],
      TYPE: exports.flyfortgun
    }, {
      /** SIZE     X       Y     ANGLE    ARC */
      POSITION: [5, 0, -25, 0, 360, 0],
      TYPE: exports.flyfortgun
    }, 
  ]
};
exports.bace = {
  PARENT: [exports.genericTank],
  LABEL: "Bomber Ace",
  SIZE: 12,
  SPEED: 7.5,
  DANGER: 7,
  SHAPE: [[0.073,0.47],[0.07,1.14],[-0.05,1.31],[-0.287,1.296],[-0.407,1.11],[-0.41,0.38],[-0.927,0.35],[-1.22,0.56],[-1.49,0.596],[-1.5,-0.584],[-1.25,-0.55],[-0.95,-0.344],[-0.43,-0.4],[-0.42,-1.13],[-0.25,-1.28],[-0.03,-1.28],[0.06,-1.09],[0.06,-0.4],[0.99,-0.27],[1.46,0.02],[0.95,0.396]],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.freeze, g.flank, g.halfreload, g.halfreload, g.doublesize]),
        TYPE: exports.acebomb
      }
    }, 
    {
      POSITION: [2, 5, 1, 0, 0, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.dart
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.dart
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 270, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.dart
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 45, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.dart
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 135, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.dart
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 225, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.dart
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 315, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.dart
      }
    }
  ]
};exports.gz = {
  PARENT: [exports.genericTank],
  LABEL: "Ground Zero",
  SIZE: 12,
  SPEED: 7.5,
  DANGER: 7,
  SHAPE: [[0.073,0.47],[0.07,1.14],[-0.05,1.31],[-0.287,1.296],[-0.407,1.11],[-0.41,0.38],[-0.927,0.35],[-1.22,0.56],[-1.49,0.596],[-1.5,-0.584],[-1.25,-0.55],[-0.95,-0.344],[-0.43,-0.4],[-0.42,-1.13],[-0.25,-1.28],[-0.03,-1.28],[0.06,-1.09],[0.06,-0.4],[0.99,-0.27],[1.46,0.02],[0.95,0.396]],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.freeze, g.flank, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload]),
        TYPE: exports.groundz,
        ALT_FIRE: true
      }
    }, 
    {
      POSITION: [2, 5, 1, 0, 0, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.dart
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.dart
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 270, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.dart
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 45, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.dart
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 135, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.dart
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 225, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.dart
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 315, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.dart
      }
    }
  ]
};exports.tsabom = {
  PARENT: [exports.genericTank],
  LABEL: "Tsar Bomba",
  SIZE: 24,
  SPEED: 10,
  DANGER: 7,
  SHAPE: [[0.073,0.47],[0.07,1.14],[-0.05,1.31],[-0.287,1.296],[-0.407,1.11],[-0.41,0.38],[-0.927,0.35],[-1.22,0.56],[-1.49,0.596],[-1.5,-0.584],[-1.25,-0.55],[-0.95,-0.344],[-0.43,-0.4],[-0.42,-1.13],[-0.25,-1.28],[-0.03,-1.28],[0.06,-1.09],[0.06,-0.4],[0.99,-0.27],[1.46,0.02],[0.95,0.396]],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload, g.doubledamage, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, 
    {/*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.freeze, g.flank, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload]),
        TYPE: exports.tsarb,
        ALT_FIRE: true
      }
    }, 
    {
      POSITION: [2, 5, 1, 0, 0, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload, g.doubledamage, g.fast, g.fast]),
        TYPE: exports.dart
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload, g.doubledamage, g.fast, g.fast]),
        TYPE: exports.dart
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 270, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload, g.doubledamage, g.fast, g.fast]),
        TYPE: exports.dart
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 45, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload, g.doubledamage, g.fast, g.fast]),
        TYPE: exports.dart
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 135, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload, g.doubledamage, g.fast, g.fast]),
        TYPE: exports.dart
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 225, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload, g.doubledamage, g.fast, g.fast]),
        TYPE: exports.dart
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 315, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload, g.doubledamage, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }
  ]
};
exports.nace = {
  PARENT: [exports.genericTank],
  LABEL: "Neva-miss Ace",
  SIZE: 12,
  SPEED: 7.5,
  DANGER: 7,
  SHAPE: [[0.073,0.47],[0.07,1.14],[-0.05,1.31],[-0.287,1.296],[-0.407,1.11],[-0.41,0.38],[-0.927,0.35],[-1.22,0.56],[-1.49,0.596],[-1.5,-0.584],[-1.25,-0.55],[-0.95,-0.344],[-0.43,-0.4],[-0.42,-1.13],[-0.25,-1.28],[-0.03,-1.28],[0.06,-1.09],[0.06,-0.4],[0.99,-0.27],[1.46,0.02],[0.95,0.396]],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.exswarm
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.exswarm
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.exswarm
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 270, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.exswarm
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 45, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.exswarm
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 135, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.exswarm
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 225, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.exswarm
      }
    },
    {
      POSITION: [2, 5, 1, 0, 0, 315, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.exswarm
      }
    }
  ]
};
exports.face = {
  PARENT: [exports.genericTank],
  LABEL: "Fighter Plane",
  SIZE: 15,
  SPEED: 10,
  DANGER: 10,
  SHAPE: [[0.073,0.47],[0.07,1.14],[-0.05,1.31],[-0.287,1.296],[-0.407,1.11],[-0.41,0.38],[-0.927,0.35],[-1.22,0.56],[-1.49,0.596],[-1.5,-0.584],[-1.25,-0.55],[-0.95,-0.344],[-0.43,-0.4],[-0.42,-1.13],[-0.25,-1.28],[-0.03,-1.28],[0.06,-1.09],[0.06,-0.4],[0.99,-0.27],[1.46,0.02],[0.95,0.396]],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 5, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload, g.halfreload, g.halfreload]),
        TYPE: exports.autoexswarm
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, -5, 0, 1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload, g.halfreload, g.halfreload]),
        TYPE: exports.autoexswarm
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 30, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 60, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 120, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 150, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 210, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 240, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 270, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 300, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 330, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload]),
        TYPE: exports.dart
      }
    },
  ]
};
exports.ods = {
  PARENT: [exports.genericTank],
  LABEL: "Operation Dartstorm",
  SIZE: 25,
  SPEED: 10,
  DANGER: 10,
  SHAPE: [[0.073,0.47],[0.07,1.14],[-0.05,1.31],[-0.287,1.296],[-0.407,1.11],[-0.41,0.38],[-0.927,0.35],[-1.22,0.56],[-1.49,0.596],[-1.5,-0.584],[-1.25,-0.55],[-0.95,-0.344],[-0.43,-0.4],[-0.42,-1.13],[-0.25,-1.28],[-0.03,-1.28],[0.06,-1.09],[0.06,-0.4],[0.99,-0.27],[1.46,0.02],[0.95,0.396]],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 5, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload, g.halfreload]),
        TYPE: exports.autoexswarm
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, -5, 0, 1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload, g.halfreload]),
        TYPE: exports.autoexswarm
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 22.5, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 45, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 67.5, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 112.5, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 135, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 157.5, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 202.5, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 225, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 247.5, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 270, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 292.5, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 315, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 337.5, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank]),
        TYPE: exports.dart
      }
    },
  ]
};
exports.skyshred= {
  PARENT: [exports.genericTank],
  LABEL: "Sky Shredder",
  SIZE: 30,
  SPEED: 15,
  DANGER: 15,
  SHAPE: [[0.073,0.47],[0.07,1.14],[-0.05,1.31],[-0.287,1.296],[-0.407,1.11],[-0.41,0.38],[-0.927,0.35],[-1.22,0.56],[-1.49,0.596],[-1.5,-0.584],[-1.25,-0.55],[-0.95,-0.344],[-0.43,-0.4],[-0.42,-1.13],[-0.25,-1.28],[-0.03,-1.28],[0.06,-1.09],[0.06,-0.4],[0.99,-0.27],[1.46,0.02],[0.95,0.396]],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 5, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.autoexswarm
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, -5, 0, 1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank, g.halfreload, g.halfreload]),
        TYPE: exports.autoexswarm
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 12, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 24, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 36, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 48, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 60, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 72, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 84, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 96, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 108, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 120, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 132, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 144, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 156, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 168, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 192, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 204, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 216, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 228, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 240, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 252, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 264, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 276, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 288, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 300, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 312, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 324, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 336, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 5, 1, 0, 0, 348, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.doublereload, g.fast, g.fast, g.fast, g.fast, g.fast, g.fast]),
        TYPE: exports.dart
      }
    }, 
  ]
};
exports.pulse = {
  PARENT: [exports.genericTank],
  LABEL: "Pulse",
  DANGER: 7,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.flank, g.spam]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.flank, g.spam]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.flank, g.spam]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 270, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.flank, g.spam]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 45, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.flank, g.spam]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 135, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.flank, g.spam]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 225, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.flank, g.spam]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 315, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.flank, g.spam]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.heptatrap = (() => {
  let a = 360 / 7,
    d = 1 / 7;
  return {
    PARENT: [exports.genericTank],
    LABEL: "Hepta-Trapper",
    DANGER: 7,
    BODY: {
      SPEED: base.SPEED * 0.8
    },
    STAT_NAMES: statnames.trap,
    HAS_NO_RECOIL: true,
    GUNS: [
      {
        /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
        POSITION: [15, 7, 1, 0, 0, 0, 0]
      },
      {
        POSITION: [3, 7, 1.7, 15, 0, 0, 0],
        PROPERTIES: {
          SHOOT_SETTINGS: combineStats([g.trap, g.hexatrap]),
          TYPE: exports.trap,
          STAT_CALCULATOR: gunCalcNames.trap
        }
      },
      {
        POSITION: [15, 7, 1, 0, 0, a, 4 * d]
      },
      {
        POSITION: [3, 7, 1.7, 15, 0, a, 4 * d],
        PROPERTIES: {
          SHOOT_SETTINGS: combineStats([g.trap, g.hexatrap]),
          TYPE: exports.trap,
          STAT_CALCULATOR: gunCalcNames.trap
        }
      },
      {
        POSITION: [15, 7, 1, 0, 0, 2 * a, 1 * d]
      },
      {
        POSITION: [3, 7, 1.7, 15, 0, 2 * a, 1 * d],
        PROPERTIES: {
          SHOOT_SETTINGS: combineStats([g.trap, g.hexatrap]),
          TYPE: exports.trap,
          STAT_CALCULATOR: gunCalcNames.trap
        }
      },
      {
        POSITION: [15, 7, 1, 0, 0, 3 * a, 5 * d]
      },
      {
        POSITION: [3, 7, 1.7, 15, 0, 3 * a, 5 * d],
        PROPERTIES: {
          SHOOT_SETTINGS: combineStats([g.trap, g.hexatrap]),
          TYPE: exports.trap,
          STAT_CALCULATOR: gunCalcNames.trap
        }
      },
      {
        POSITION: [15, 7, 1, 0, 0, 4 * a, 2 * d]
      },
      {
        POSITION: [3, 7, 1.7, 15, 0, 4 * a, 2 * d],
        PROPERTIES: {
          SHOOT_SETTINGS: combineStats([g.trap, g.hexatrap]),
          TYPE: exports.trap,
          STAT_CALCULATOR: gunCalcNames.trap
        }
      },
      {
        POSITION: [15, 7, 1, 0, 0, 5 * a, 6 * d]
      },
      {
        POSITION: [3, 7, 1.7, 15, 0, 5 * a, 6 * d],
        PROPERTIES: {
          SHOOT_SETTINGS: combineStats([g.trap, g.hexatrap]),
          TYPE: exports.trap,
          STAT_CALCULATOR: gunCalcNames.trap
        }
      },
      {
        POSITION: [15, 7, 1, 0, 0, 6 * a, 3 * d]
      },
      {
        POSITION: [3, 7, 1.7, 15, 0, 6 * a, 3 * d],
        PROPERTIES: {
          SHOOT_SETTINGS: combineStats([g.trap, g.hexatrap]),
          TYPE: exports.trap,
          STAT_CALCULATOR: gunCalcNames.trap
        }
      }
    ]
  };
})();
exports.hexatrap = makeAuto({
  PARENT: [exports.genericTank],
  LABEL: "Hexa-Trapper",
  DANGER: 7,
  BODY: {
    SPEED: base.SPEED * 0.8
  },
  STAT_NAMES: statnames.trap,
  HAS_NO_RECOIL: true,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [15, 7, 1, 0, 0, 0, 0]
    },
    {
      POSITION: [3, 7, 1.7, 15, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap, g.hexatrap]),
        TYPE: exports.trap,
        STAT_CALCULATOR: gunCalcNames.trap
      }
    },
    {
      POSITION: [15, 7, 1, 0, 0, 60, 0.5]
    },
    {
      POSITION: [3, 7, 1.7, 15, 0, 60, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap, g.hexatrap]),
        TYPE: exports.trap,
        STAT_CALCULATOR: gunCalcNames.trap
      }
    },
    {
      POSITION: [15, 7, 1, 0, 0, 120, 0]
    },
    {
      POSITION: [3, 7, 1.7, 15, 0, 120, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap, g.hexatrap]),
        TYPE: exports.trap,
        STAT_CALCULATOR: gunCalcNames.trap
      }
    },
    {
      POSITION: [15, 7, 1, 0, 0, 180, 0.5]
    },
    {
      POSITION: [3, 7, 1.7, 15, 0, 180, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap, g.hexatrap]),
        TYPE: exports.trap,
        STAT_CALCULATOR: gunCalcNames.trap
      }
    },
    {
      POSITION: [15, 7, 1, 0, 0, 240, 0]
    },
    {
      POSITION: [3, 7, 1.7, 15, 0, 240, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap, g.hexatrap]),
        TYPE: exports.trap,
        STAT_CALCULATOR: gunCalcNames.trap
      }
    },
    {
      POSITION: [15, 7, 1, 0, 0, 300, 0.5]
    },
    {
      POSITION: [3, 7, 1.7, 15, 0, 300, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap, g.hexatrap]),
        TYPE: exports.trap,
        STAT_CALCULATOR: gunCalcNames.trap
      }
    }
  ]
});

exports.tri = {
  PARENT: [exports.genericTank],
  LABEL: "Tri-Angle",
  BODY: {
    HEALTH: base.HEALTH * 0.8,
    SHIELD: base.SHIELD * 0.8,
    DENSITY: base.DENSITY * 0.6
  },
  DANGER: 6,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.flank,
          g.tri,
          g.trifront,
          g.tonsmorrecoil
        ]),
        TYPE: exports.bullet,
        LABEL: "Front"
      }
    },
    {
      POSITION: [16, 8, 1, 0, 0, 150, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.thruster]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 8, 1, 0, 0, 210, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.thruster]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    }
  ]
};
exports.leak = {
  PARENT: [exports.genericTank],
  LABEL: "Leaker",
  BODY: {
    HEALTH: base.HEALTH * 0.8,
    SHIELD: base.SHIELD * 0.8,
    DENSITY: base.DENSITY * 0.6
  },
  DANGER: 6,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.flank,
          g.tri,
          g.trifront,
          g.tonsmorrecoil
        ]),
        TYPE: exports.bullet,
        LABEL: "Front"
      }
    },
    {
      POSITION: [16, 8, 1, 0, 0, 150, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.thruster]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 8, 1, 0, 0, 210, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.thruster]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [12, 8, 2, 0, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.halfreload,
          g.freeze,
          g.doublerange,
          g.doublesize
        ]),
        TYPE: exports.acidpuddle,
        LABEL: "Poison Puddle"
      }
    },
  ]
};
exports.autotri = makeAuto(exports.tri, "Auto Tri-angle");
exports.goldfish = {
  PARENT: [exports.genericTank],
  LABEL: "Gold Fish",
  BODY: {
    HEALTH: base.HEALTH * 0.8,
    SHIELD: base.SHIELD * 0.8,
    DENSITY: base.DENSITY * 0.6
  },
  DANGER: 6,
  GUNS: [
    {
      POSITION: [16, 4, 1, 0, 0, 150, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.thruster]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 4, 1, 0, 0, 210, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.thruster]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    }
  ]
};
exports.octop = {
  PARENT: [exports.genericTank],
  LABEL: "Octopus",
  BODY: {
    HEALTH: base.HEALTH * 0.8,
    SHIELD: base.SHIELD * 0.8,
    DENSITY: base.DENSITY * 0.6
  },
  DANGER: 6,
  GUNS: [
    {
      POSITION: [16, 10, 1, 0, 0, 150, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.flank,
          g.pound,
          g.thruster,
          g.tonsmorrecoil,
          g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 10, 1, 0, 0, 210, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.flank,
          g.pound,
          g.thruster,
          g.tonsmorrecoil,
          g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 4, 1, 0, 0, 5, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [16, 4, 1, 0, 0, -5, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mini]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [16, 4, 1, 0, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.mach, g.pound]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.squid = {
  PARENT: [exports.genericTank],
  LABEL: "Squid",
  BODY: {
    HEALTH: base.HEALTH * 1.2,
    SHIELD: base.SHIELD * 1,
    DENSITY: base.DENSITY * 0.8
  },
  DANGER: 6,
  GUNS: [
    {
      POSITION: [16, 10, 1, 0, 0, 150, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.flank,
          g.pound,
          g.thruster,
          g.tonsmorrecoil,
          g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 10, 1, 0, 0, 210, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.flank,
          g.pound,
          g.thruster,
          g.tonsmorrecoil,
          g.halfreload,
          g.halfreload
        ]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    }
  ]
};
exports.reefshark = {
  PARENT: [exports.genericTank],
  LABEL: "Reef Shark",
  BODY: {
    HEALTH: base.HEALTH * 0.8,
    SHIELD: base.SHIELD * 0.8,
    DENSITY: base.DENSITY * 0.6
  },
  DANGER: 6,
  GUNS: [
    {
      POSITION: [16, 4, 1, 0, 0, 150, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.thruster]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 4, 1, 0, 0, 210, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.thruster]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 4, 1, 0, 0, 160, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.thruster]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 4, 1, 0, 0, 200, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.thruster]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    }
  ]
};
exports.dolphin = {
  PARENT: [exports.genericTank],
  LABEL: "Dolphin",
  BODY: {
    HEALTH: base.HEALTH * 0.8,
    SHIELD: base.SHIELD * 0.8,
    DENSITY: base.DENSITY * 0.6
  },
  DANGER: 6,
  GUNS: [
    {
      POSITION: [20, 10, 2.5, 0, 0, 180, 1]
    },
    {
      POSITION: [5, 4, 1, 20, 0, 150, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.thruster]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [5, 4, 1, 20, 0, 210, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.thruster]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [5, 4, 1, 20, 0, 160, 0.333],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.thruster]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [5, 4, 1, 20, 0, 200, 0.333],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.thruster]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [5, 4, 1, 20, 0, 170, 0.667],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.thruster]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [5, 4, 1, 20, 0, 190, 0.667],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.thruster]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [5, 7, 1, 20, 0, 180, 1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.flank,
          g.tri,
          g.thruster,
          g.morerecoil
        ]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    }
  ]
};
exports.starfish = {
  PARENT: [exports.genericTank],
  LABEL: "Starfish",
  FACING_TYPE: "autospin",
  BODY: {
    HEALTH: base.HEALTH * 0.8,
    SHIELD: base.SHIELD * 0.8,
    DENSITY: base.DENSITY * 0.6
  },
  DANGER: 6,
  GUNS: [
    {
      POSITION: [16, 4, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.norecoil,
          g.mach,
          g.halfreload
        ]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 4, 1, 0, 0, 72, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.norecoil,
          g.mach,
          g.halfreload
        ]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 4, 1, 0, 0, 144, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.norecoil,
          g.mach,
          g.halfreload
        ]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 4, 1, 0, 0, 216, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.norecoil,
          g.mach,
          g.halfreload
        ]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 4, 1, 0, 0, 288, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.norecoil,
          g.mach,
          g.halfreload
        ]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    }
  ]
};
exports.search = {
  PARENT: [exports.genericTank],
  LABEL: "Searcher",
  BODY: {
    HEALTH: base.HEALTH * 0.8,
    SHIELD: base.SHIELD * 0.8,
    DENSITY: base.DENSITY * 0.6,
    FOV: base.FOV * 1.4
  },
  DANGER: 6,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [25, 8, 1.2, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.sniper,
          g.pound,
          g.tri,
          g.trifront,
          g.tonsmorrecoil
        ]),
        TYPE: exports.bullet,
        LABEL: "Searcher front"
      }
    },
    {
      POSITION: [16, 8, 1, 0, 0, 150, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.thruster]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 8, 1, 0, 0, 210, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.thruster]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    { POSITION: [5, 8.5, -1.6, 8, 0, 0, 0] }
  ]
};
exports.drag = {
  PARENT: [exports.genericTank],
  LABEL: "Dragon",
  COLOR: 11,
  FOV: 2.5,
  SIZE: 30,
  BODY: {
    HEALTH: base.HEALTH * 1.5,
    SHIELD: base.SHIELD * 1.5,
    DENSITY: base.DENSITY * 1
  },
  DANGER: 6,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 12, 1.75, 0, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.fireball]),
        TYPE: exports.bullet,
        LABEL: "Fireball"
      }
    },
    {
      POSITION: [18, 8, 1.5, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.firebreath]),
        TYPE: exports.flare1,
        LABEL: "Fire Breath"
      }
    },
    {
      POSITION: [16, 8, 1, 0, 0, 150, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.flank,
          g.tri,
          g.thruster,
          g.doublereload
        ]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 8, 1, 0, 0, 210, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.flank,
          g.tri,
          g.thruster,
          g.doublereload
        ]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 4, 1.5, 0, 0, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.tri,
          g.doublereload,
          g.doublereload,
          g.slow
        ]),
        TYPE: exports.flare2,
        LABEL: "Fire Wing"
      }
    },
    {
      POSITION: [16, 4, 1.5, 0, 0, -90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.tri,
          g.doublereload,
          g.doublereload,
          g.slow
        ]),
        TYPE: exports.flare2,
        LABEL: "Fire Wing"
      }
    }
  ]
};
exports.yarh = {
  PARENT: [exports.genericTank],
  LABEL: "Yharon",
  COLOR: 12,
  SIZE: 50,
  FOV: 6.5,
  BODY: {
    HEALTH: base.HEALTH * 50,
    SHIELD: base.SHIELD * 25,
    DENSITY: base.DENSITY * 5
  },
  DANGER: 6,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 12, 1.75, 0, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.doublesize, g.halfreload, g.halfreload, g.infihealth]),
        TYPE: exports.fireball,
        LABEL: "Mega Fireball",
        MAX_CHILDREN: 1
      }
    },
    {
      POSITION: [18, 8, 1.5, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.firebreath, g.doubledamage, g.halfreload, g.halfreload, g.halfreload, g.halfdamage, g.halfdamage]),
        TYPE: exports.flare1,
        LABEL: "Fire Breath"
      }
    },
    {
      POSITION: [16, 8, 1, 0, 0, 150, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.flank,
          g.tri,
          g.thruster,
          g.doublereload, g.halfreload, g.halfreload
        ]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 8, 1, 0, 0, 210, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.flank,
          g.tri,
          g.thruster,
          g.doublereload, g.halfreload, g.halfreload
        ]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 4, 1.5, 0, 0, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach
        ]),
        TYPE: exports.fshield,
        LABEL: "Fire Shield",
        MAX_CHILDREN: 1
      }
    },
    {
      POSITION: [16, 4, 1.5, 0, 0, -90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach, g.halfreload
        ]),
        TYPE: exports.fshield,
        LABEL: "Fire Shield",
        MAX_CHILDREN: 1
      }
    }
  ]
};
exports.falldrag = {
  PARENT: [exports.genericTank],
  LABEL: "The Fallen One",
  COLOR: 16,
  SIZE: 250,
  FOV: 6.5,
  BODY: {
    HEALTH: base.HEALTH * 1000,
    SHIELD: base.SHIELD * 100,
    DENSITY: base.DENSITY * 50
  },
  DANGER: 6,
  GUNS: [
     
    {
      POSITION: [1, 1, 1.7, 8, 0, 120, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.halfreload, g.halfreload, g.halfreload, g.doublesize]),
        TYPE: exports.fireball
      }
    }, {
      POSITION: [1, 1, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.halfreload, g.halfreload, g.halfreload, g.freeze, g.norecoil, g.doubledamage, g.doubledamage, g.doubledamage, g.doubledamage, g.doubledamage, g.doubledamage, g.doubledamage, g.doubledamage, g.doubledamage, g.doublesize, g.doublesize]),
        TYPE: exports.ringoffire,
        LABEL: "Ring of Fire"
      }
    }, 
    {
      POSITION: [1, 1, 1.7, 8, 0, 240, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.halfreload, g.halfreload, g.halfreload, g.doublesize]),
        TYPE: exports.fireball
      }
    }, 
    {
      POSITION: [1, 1, 1.7, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.halfreload, g.halfreload, g.halfreload, g.doublesize]),
        TYPE: exports.fireball
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 12, 1.75, 0, 0, 0, 1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.mach, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload]),
        TYPE: exports.trueinfernado,
        LABEL: "True Infernado",
        MAX_CHILDREN: 1
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 12, 1.75, 0, 0, 0, 1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.mach, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload]),
        TYPE: exports.trueinfernado,
        LABEL: "True Infernado",
        MAX_CHILDREN: 1
      }
    }, {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 12, 1.75, 0, 0, 0, 1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.mach, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload, g.halfreload]),
        TYPE: exports.trueinfernado,
        LABEL: "True Infernado",
        MAX_CHILDREN: 1
      }
    },
    {
      POSITION: [18, 8, 1.5, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.firebreath, g.mach]),
        TYPE: exports.flare1,
        LABEL: "Fire Breath"
      }
    },
    {
      POSITION: [16, 8, 1, 0, 0, 150, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.flank,
          g.tri,
          g.thruster,
          g.doublereload
        ]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 8, 1, 0, 0, 210, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.flank,
          g.tri,
          g.thruster,
          g.doublereload
        ]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 4, 1.5, 0, 0, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach
        ]),
        TYPE: exports.fshield,
        LABEL: "Fire Shield",
        MAX_CHILDREN: 1
      }
    },
    {
      POSITION: [16, 4, 1.5, 0, 0, -90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach, g.halfreload
        ]),
        TYPE: exports.fshield,
        LABEL: "Fire Shield",
        MAX_CHILDREN: 1
      }
    }
  ]
};
exports.bdrag = {
  PARENT: [exports.genericTank],
  LABEL: "Baby Dragon",
  FOV: 1.5,
  BODY: {
    HEALTH: base.HEALTH * 1.5,
    SHIELD: base.SHIELD * 1.5,
    DENSITY: base.DENSITY * 1
  },
  DANGER: 6,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 12, 1.75, 0, 0, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.weakfireball]),
        TYPE: exports.bullet,
        LABEL: "Fireball"
      }
    },
    {
      POSITION: [18, 8, 1.5, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.weakfirebreath]),
        TYPE: exports.bullet,
        LABEL: "Fire Breath"
      }
    },
    {
      POSITION: [16, 8, 1, 0, 0, 150, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.thruster]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 8, 1, 0, 0, 210, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.thruster]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 4, 1.5, 0, 0, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.tri,
          g.doublereload
        ]),
        TYPE: exports.bullet,
        LABEL: "Fire Wing"
      }
    },
    {
      POSITION: [16, 4, 1.5, 0, 0, -90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.gunner,
          g.tri,
          g.doublereload
        ]),
        TYPE: exports.bullet,
        LABEL: "Fire Wing"
      }
    }
  ]
};
exports.booster = {
  PARENT: [exports.genericTank],
  LABEL: "Booster",
  BODY: {
    HEALTH: base.HEALTH * 0.6,
    SHIELD: base.SHIELD * 0.6,
    DENSITY: base.DENSITY * 0.2
  },
  DANGER: 7,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.flank,
          g.tri,
          g.trifront,
          g.muchmorerecoil
        ]),
        TYPE: exports.bullet,
        LABEL: "Front"
      }
    },
    {
      POSITION: [13, 8, 1, 0, -1, 135, 0.6],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.flank,
          g.tri,
          g.thruster,
          g.halfrecoil
        ]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [13, 8, 1, 0, 1, 225, 0.6],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.flank,
          g.tri,
          g.thruster,
          g.halfrecoil
        ]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 8, 1, 0, 0, 145, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.thruster]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 8, 1, 0, 0, 215, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.thruster]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    }
  ]
};
exports.form1 = {
  PARENT: [exports.genericTank],
  LABEL: "Formula One",
  BODY: {
    HEALTH: base.HEALTH * 2,
    SHIELD: base.SHIELD * 2,
    DENSITY: base.DENSITY * 0.2
  },
  DANGER: 10,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [2, 1, 0.0001, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.lancerblade,
        ]),
        TYPE: exports.bullet,
        LABEL: "nose"
      }
    },
    {
      POSITION: [30, 15, 0.05, 0, 0, 0, 0], },
    {
      POSITION: [13, 8, 1.5, 0, -5, 165, 0.6],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.tri,
          g.tonsmorrecoil,
          g.tonsmorrecoil,
        ]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [13, 8, 1.5, 0, 5, 200, 0.6],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.mach,
          g.tri,
          g.tonsmorrecoil,
          g.tonsmorrecoil,
        ]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 8, 1.5, 0, 0, 145, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.tri]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 8, 1.5, 0, 0, 215, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.mach, g.tri]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    }
  ]
};
exports.fighter = {
  PARENT: [exports.genericTank],
  LABEL: "Fighter",
  BODY: {
    DENSITY: base.DENSITY * 0.6
  },
  DANGER: 7,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.trifront]),
        TYPE: exports.bullet,
        LABEL: "Front"
      }
    },
    {
      POSITION: [16, 8, 1, 0, -1, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.trifront]),
        TYPE: exports.bullet,
        LABEL: "Side"
      }
    },
    {
      POSITION: [16, 8, 1, 0, 1, -90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.trifront]),
        TYPE: exports.bullet,
        LABEL: "Side"
      }
    },
    {
      POSITION: [16, 8, 1, 0, 0, 150, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.thruster]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 8, 1, 0, 0, 210, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.thruster]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    }
  ]
};
exports.brutalizer = {
  PARENT: [exports.genericTank],
  LABEL: "Surfer",
  BODY: {
    DENSITY: base.DENSITY * 0.6
  },
  DANGER: 7,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [18, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.trifront]),
        TYPE: exports.bullet,
        LABEL: "Front"
      }
    },
    {
      POSITION: [7, 7.5, 0.6, 7, -1, 90, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm]),
        TYPE: [exports.autoswarm],
        STAT_CALCULATOR: gunCalcNames.swarm
      }
    },
    {
      POSITION: [7, 7.5, 0.6, 7, 1, -90, 9],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm]),
        TYPE: [exports.autoswarm],
        STAT_CALCULATOR: gunCalcNames.swarm
      }
    },
    {
      POSITION: [16, 8, 1, 0, 0, 150, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.thruster]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 8, 1, 0, 0, 210, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.thruster]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    }
  ]
};
exports.bomber = {
  PARENT: [exports.genericTank],
  LABEL: "Bomber",
  BODY: {
    DENSITY: base.DENSITY * 0.6
  },
  DANGER: 7,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri, g.trifront]),
        TYPE: exports.bullet,
        LABEL: "Front"
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 130, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri]),
        TYPE: exports.bullet,
        LABEL: "Wing"
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 230, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.tri]),
        TYPE: exports.bullet,
        LABEL: "Wing"
      }
    },
    {
      POSITION: [14, 8, 1, 0, 0, 180, 0]
    },
    {
      POSITION: [4, 8, 1.5, 14, 0, 180, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap, g.morerecoil]),
        TYPE: exports.trap,
        STAT_CALCULATOR: gunCalcNames.trap
      }
    }
  ]
};
exports.autotri = makeAuto(exports.tri);
exports.autotri.BODY = {
  SPEED: base.SPEED
};
exports.falcon = {
  PARENT: [exports.genericTank],
  LABEL: "Falcon",
  DANGER: 7,
  BODY: {
    ACCELERATION: base.ACCEL * 0.8,
    FOV: base.FOV * 1.2
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [27, 8.5, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.sniper,
          g.assass,
          g.lessreload
        ]),
        TYPE: exports.bullet,
        LABEL: "Assassin",
        ALT_FIRE: true
      }
    },
    {
      POSITION: [5, 8.5, -1.6, 8, 0, 0, 0]
    },
    {
      POSITION: [16, 8, 1, 0, 0, 150, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.flank,
          g.tri,
          g.thruster,
          g.halfrecoil
        ]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [16, 8, 1, 0, 0, 210, 0.1],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.flank,
          g.tri,
          g.thruster,
          g.halfrecoil
        ]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    },
    {
      POSITION: [18, 8, 1, 0, 0, 180, 0.6],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.flank,
          g.tri,
          g.thruster,
          g.halfrecoil
        ]),
        TYPE: exports.bullet,
        LABEL: gunCalcNames.thruster
      }
    }
  ]
};

exports.auto3 = {
  PARENT: [exports.genericTank],
  LABEL: "Auto-3",
  DANGER: 6,
  FACING_TYPE: "autospin",
  TURRETS: [
    {
      /*  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, 8, 0, 0, 190, 0],
      TYPE: exports.auto3gun
    },
    {
      POSITION: [11, 8, 0, 120, 190, 0],
      TYPE: exports.auto3gun
    },
    {
      POSITION: [11, 8, 0, 240, 190, 0],
      TYPE: exports.auto3gun
    }
  ]
};
exports.auto3turret = {
  PARENT: [exports.genericTank],
  LABEL: "Auto-3",
  DANGER: 6,
  COLOR: 16,
  FACING_TYPE: "autospin",
  BODY: {
   FOV: 2 
  },
  TURRETS: [
    {
      /*  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, 8, 0, 0, 190, 0],
      TYPE: exports.auto5gun
    },
    {
      POSITION: [11, 8, 0, 120, 190, 0],
      TYPE: exports.auto5gun
    },
    {
      POSITION: [11, 8, 0, 240, 190, 0],
      TYPE: exports.auto5gun
    }
  ]
};
exports.mace = {
  PARENT: [exports.genericTank],
  LABEL: "mace",
  DANGER: 6,
  BODY: {
    SPEED: base.SPEED * 1.5,
},
  TURRETS: [
    {
      /*  SIZE     X       Y     ANGLE    ARC */
      POSITION: [4, 5, 0, 0, 190, 0],
      TYPE: exports.chain
    },
    {
      POSITION: [4, 10, 0, 0, 190, 0],
      TYPE: exports.chain
       },
    {
      POSITION: [4, 15, 0, 0, 190, 0],
      TYPE: exports.chain
       },
    {
      POSITION: [4, 20, 0, 0, 190, 0],
      TYPE: exports.chain
       },
    {
      POSITION: [4, 25, 0, 0, 190, 0],
      TYPE: exports.chain
       },
    {
      POSITION: [4, 30, 0, 0, 190, 0],
      TYPE: exports.chain
       },
    {
      POSITION: [4, 35, 0, 0, 190, 0],
      TYPE: exports.chain
       },
    {
      POSITION: [4, 40, 0, 0, 190, 0],
      TYPE: exports.chain
       },
    {
      POSITION: [4, 45, 0, 0, 190, 0],
      TYPE: exports.chain
       },
    {
      POSITION: [4, 50, 0, 0, 190, 0],
      TYPE: exports.chain
    },
    {
      POSITION: [4, 50, 0, 0, 190, 0],
      TYPE: exports.chain
    },
    {POSITION: [15, 60, 0, 0, 0, 0],
      TYPE: exports.macemain
    }
  ],


};
exports.trooper = {
  PARENT: [exports.genericTank],
  LABEL: "Trooper",
  DANGER: 6,
  BODY: {
    SPEED: base.SPEED * 1.5,
},
  TURRETS: [
    {
      /*  SIZE     X       Y     ANGLE    ARC */
      POSITION: [4, 5, 0, 0, 190, 0],
      TYPE: exports.chain
    },
    {
      POSITION: [4, 10, 0, 0, 190, 0],
      TYPE: exports.chain
       },
    {
      POSITION: [4, 15, 0, 0, 190, 0],
      TYPE: exports.chain
       },
    {
      POSITION: [4, 20, 0, 0, 190, 0],
      TYPE: exports.chain
       },
    {
      POSITION: [4, 25, 0, 0, 190, 0],
      TYPE: exports.chain
       },
    {
      POSITION: [4, 30, 0, 0, 190, 0],
      TYPE: exports.chain
       },
    {
      POSITION: [4, 35, 0, 0, 190, 0],
      TYPE: exports.chain
       },
    {
      POSITION: [4, 40, 0, 0, 190, 0],
      TYPE: exports.chain
       },
    {
      POSITION: [4, 45, 0, 0, 190, 0],
      TYPE: exports.chain
       },
    {
      POSITION: [4, 50, 0, 0, 190, 0],
      TYPE: exports.chain
    },
    {
      POSITION: [4, 50, 0, 0, 190, 0],
      TYPE: exports.chain
    },
    {POSITION: [15, 60, 0, 0, 0, 0],
      TYPE: exports.macemain
    }
  ],
    GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 8, 1, 0, 5.5, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.lowpower]),
        TYPE: exports.bullet
      }
    },
    {
      /* LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 8, 1, 0, -5.5, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.twin, g.lowpower]),
        TYPE: exports.bullet
      }
    }
  ]
};
exports.invincity = {
  PARENT: [exports.genericTank],
  LABEL: "Invincible City",
  SHAPE: -6,
  SIZE: 100,
  BODY: {
    SHIELD: 0,
    SPEED: 0,
    HEALTH: 10000,
    FOV: 2.5
  },
  DANGER: 20,
  FACING_TYPE: "autospin",
  TURRETS: [
    {
      /*  SIZE     X       Y     ANGLE    ARC */
      POSITION: [6, 8, 0, 150, 15, 0],
      TYPE: exports.fortautoTurret
    },
    {
      POSITION: [6, 8, 0, 30, 15, 0],
      TYPE: exports.fortautoTurret
    },
    {
      POSITION: [6, 8, 0, 270, 15, 0],
      TYPE: exports.fortautoTurret
    }, {
      /*  SIZE     X       Y     ANGLE    ARC */
      POSITION: [6, 8, 0, 210, 15, 0],
      TYPE: exports.fortmachgunTurret
    },
    {
      POSITION: [6, 8, 0, 90, 15, 0],
      TYPE: exports.fortmachgunTurret
    },
    {
      POSITION: [6, 8, 0, 330, 15, 0],
      TYPE: exports.fortmachgunTurret
    }, {
      /*  SIZE     X       Y     ANGLE    ARC */
      POSITION: [6, 8, 0, 120, 15, 0],
      TYPE: exports.fortshotTurret
    },
    {
      POSITION: [6, 8, 0, 0, 15, 0],
      TYPE: exports.fortshotTurret
    },
    {
      POSITION: [6, 8, 0, 240, 15, 0],
      TYPE: exports.fortshotTurret
    }, {
      /*  SIZE     X       Y     ANGLE    ARC */
      POSITION: [6, 8, 0, 180, 15, 0],
      TYPE: exports.forttrappTurret
    },
    {
      POSITION: [6, 8, 0, 60, 15, 0],
      TYPE: exports.forttrappTurret
    },
    {
      POSITION: [6, 8, 0, 300, 15, 0],
      TYPE: exports.forttrappTurret
    }, {
      POSITION: [6, 0, 0, 0, 360, 1],
      TYPE: exports.fortcenterTurret
    }, 
  ]
};
exports.auto5 = {
  PARENT: [exports.genericTank],
  LABEL: "Auto-5",
  DANGER: 7,
  FACING_TYPE: "autospin",
  TURRETS: [
    {
      /*  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, 8, 0, 0, 190, 0],
      TYPE: exports.auto5gun
    },
    {
      POSITION: [11, 8, 0, 72, 190, 0],
      TYPE: exports.auto5gun
    },
    {
      POSITION: [11, 8, 0, 144, 190, 0],
      TYPE: exports.auto5gun
    },
    {
      POSITION: [11, 8, 0, 216, 190, 0],
      TYPE: exports.auto5gun
    },
    {
      POSITION: [11, 8, 0, 288, 190, 0],
      TYPE: exports.auto5gun
    }
  ]
};
exports.heavy3 = {
  BODY: {
    SPEED: base.SPEED * 0.95
  },
  PARENT: [exports.genericTank],
  LABEL: "Mega-3",
  DANGER: 7,
  FACING_TYPE: "autospin",
  TURRETS: [
    {
      /*  SIZE     X       Y     ANGLE    ARC */
      POSITION: [14, 8, 0, 0, 190, 0],
      TYPE: exports.heavy3gun
    },
    {
      POSITION: [14, 8, 0, 120, 190, 0],
      TYPE: exports.heavy3gun
    },
    {
      POSITION: [14, 8, 0, 240, 190, 0],
      TYPE: exports.heavy3gun
    }
  ]
};
exports.tritrap = {
  LABEL: "tri-trapper",
  BODY: {
    SPEED: base.SPEED * 1.1
  },
  PARENT: [exports.genericTank],
  DANGER: 6,
  FACING_TYPE: "autospin",
  TURRETS: [
    {
      /*  SIZE     X       Y     ANGLE    ARC */
      POSITION: [12, 8, 0, 0, 190, 0],
      TYPE: exports.tritrapgun
    },
    {
      POSITION: [12, 8, 0, 120, 190, 0],
      TYPE: exports.tritrapgun
    },
    {
      POSITION: [12, 8, 0, 240, 190, 0],
      TYPE: exports.tritrapgun
    }
  ]
};
exports.sniper3 = {
  PARENT: [exports.genericTank],
  DANGER: 7,
  LABEL: "sniper-3",
  BODY: {
    ACCELERATION: base.ACCEL * 0.6,
    SPEED: base.SPEED * 0.8,
    FOV: base.FOV * 1.25
  },
  FACING_TYPE: "autospin",
  TURRETS: [
    {
      /*  SIZE     X       Y     ANGLE    ARC */
      POSITION: [13, 8, 0, 0, 170, 0],
      TYPE: exports.sniper3gun
    },
    {
      POSITION: [13, 8, 0, 120, 170, 0],
      TYPE: exports.sniper3gun
    },
    {
      POSITION: [13, 8, 0, 240, 170, 0],
      TYPE: exports.sniper3gun
    }
  ]
};
exports.auto4 = {
  PARENT: [exports.genericTank],
  DANGER: 5,
  LABEL: "Auto-4",
  FACING_TYPE: "autospin",
  TURRETS: [
    {
      /*  SIZE     X       Y     ANGLE    ARC */
      POSITION: [13, 6, 0, 45, 160, 0],
      TYPE: exports.auto4gun
    },
    {
      POSITION: [13, 6, 0, 135, 160, 0],
      TYPE: exports.auto4gun
    },
    {
      POSITION: [13, 6, 0, 225, 160, 0],
      TYPE: exports.auto4gun
    },
    {
      POSITION: [13, 6, 0, 315, 160, 0],
      TYPE: exports.auto4gun
    }
  ]
};

exports.flanktrap = {
  PARENT: [exports.genericTank],
  LABEL: "Trap Guard",
  STAT_NAMES: statnames.generic,
  DANGER: 6,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [20, 8, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.flank, g.flank]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [13, 8, 1, 0, 0, 180, 0]
    },
    {
      POSITION: [4, 8, 1.7, 13, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap]),
        TYPE: exports.trap,
        STAT_CALCULATOR: gunCalcNames.trap
      }
    }
  ]
};
exports.guntrap = {
  PARENT: [exports.genericTank],
  LABEL: "Gunner Trapper",
  DANGER: 7,
  STAT_NAMES: statnames.generic,
  BODY: {
    FOV: base.FOV * 1.25
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [19, 2, 1, 0, -2.5, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.power,
          g.twin,
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [19, 2, 1, 0, 2.5, 0, 0.5],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.basic,
          g.power,
          g.twin,
        ]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [12, 11, 1, 0, 0, 0, 0]
    },
    {
      POSITION: [13, 11, 1, 0, 0, 180, 0]
    },
    {
      POSITION: [4, 11, 1.7, 13, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap, g.fast, g.halfrecoil]),
        TYPE: exports.trap,
        STAT_CALCULATOR: gunCalcNames.trap
      }
    }
  ]
};
exports.bushwhack = {
  PARENT: [exports.genericTank],
  LABEL: "Bush Whacker",
  BODY: {
    ACCELERATION: base.ACCEL * 0.7,
    FOV: base.FOV * 1.2
  },
  DANGER: 7,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [24, 8.5, 1, 0, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.sniper, g.morerecoil]),
        TYPE: exports.bullet
      }
    },
    {
      POSITION: [13, 8.5, 1, 0, 0, 180, 0]
    },
    {
      POSITION: [4, 8.5, 1.7, 13, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap]),
        TYPE: exports.trap,
        STAT_CALCULATOR: gunCalcNames.trap
      }
    }
  ]
};

// UPGRADE PATHS
exports.testbed.UPGRADES_TIER_1 = [
  exports.basic,
  exports.bosses,
  exports.misc,
  exports.freetbtanks,
  exports.champ,
  exports.pentaknox
];
exports.bosses.UPGRADES_TIER_1 = [exports.yarh, exports.drag, exports.falldrag];
exports.misc.UPGRADES_TIER_1 = [
  exports.tastetherainbow,
  exports.airbase,
  exports.fireballspam,
  exports.tsarbomba,
  exports.trueinfernadoshooter,
  exports.invincity,
  exports.laztest,
  
];
exports.freetbtanks.UPGRADES_TIER_1 = [
  exports.smoke,
  exports.eleg,
  exports.aspect,
  exports.hamm,
  exports.smol,
  exports.nailgun
];
exports.champ.UPGRADES_TIER_1 = [exports.form1, exports.thermonuke, exports.firestorm, exports.torrentialdownpour, exports.electro, exports.apacheprheli,
  exports.comcom,
  exports.flyfort,
  exports.skyshred, exports.tsabom, exports.specoper, exports.hexmas
]
exports.basic.UPGRADES_TIER_1 = [
  exports.twin,
  exports.sniper,
  exports.machine,
  exports.flank,
  exports.director,
  exports.dustdevil,
  exports.pellet,
  exports.pound,
  exports.uzi,
  exports.static,
  exports.Sawshot,
  exports.lance,
  exports.smash,
  exports.pg2
];
exports.basic.UPGRADES_TIER_3 = [exports.single];
exports.lance.UPGRADES_TIER_2 = [exports.sword, exports.lancef, exports.VPNlance, exports.slance, exports.dagger];
exports.sword.UPGRADES_TIER_3 = [exports.longsword]
exports.mace.UPGRADES_TIER_3 = [exports.trooper]
exports.dagger.UPGRADES_TIER_3 = [exports.shortsword, exports.jlance]
exports.slance.UPGRADES_TIER_3 = [exports.jlance]
exports.smash.UPGRADES_TIER_3 = [
  exports.megasmash,
  exports.spike,
  exports.autosmash,
  exports.microsmash,
  exports.jumpsmash
];
exports.dustdevil.UPGRADES_TIER_2 = [exports.windstorm, exports.tornado];
exports.windstorm.UPGRADES_TIER_3 = [exports.Monsoon, exports.machnado];
exports.dustdevil.UPGRADES_TIER_3 = [
  exports.angrbee,
  exports.firewh,
  exports.superstorm,
  exports.dustc
];
exports.tornado.UPGRADES_TIER_3 = [exports.machnado, exports.Monsoon];
exports.pbasic.UPGRADES_TIER_4 = [exports.hexmas]
exports.plbasic.UPGRADES_TIER_4 = [exports.hexmas]
exports.pbasic.UPGRADES_TIER_3 = [exports.fbbasic, exports.bbasic]
exports.fbbasic.UPGRADES_TIER_3 = [exports.frbrbasic]
exports.fbasic.UPGRADES_TIER_3 = [exports.fbbasic]
exports.bbasic.UPGRADES_TIER_4 = [exports.hexmas]
exports.twin.UPGRADES_TIER_2 = [
  exports.double,
  exports.bent,
  exports.gunner,
  exports.hexa,
  exports.caval
];
exports.twin.UPGRADES_TIER_3 = [exports.triple];
exports.double.UPGRADES_TIER_3 = [
  exports.tripletwin,
  exports.split,
  exports.autodouble,
  exports.bentdouble
];
exports.bent.UPGRADES_TIER_3 = [
  exports.penta,
  exports.spread,
  exports.benthybrid,
  exports.bentdouble,
  exports.triple,
  exports.atomi
];
exports.gunner.UPGRADES_TIER_3 = [
  exports.autogunner,
  exports.nailgun,
  exports.auto4,
  exports.machinegunner,
  exports.battery,
  exports.Monsoon
];

exports.sniper.UPGRADES_TIER_2 = [
  exports.assassin,
  exports.hunter,
  exports.mini,
  exports.builder,
  exports.rifle,
  exports.fbeam,
  exports.gatl,
  exports.incog
];
exports.rifle.UPGRADES_TIER_3 = [
  exports.archer,
  exports.musket,
  exports.asrifle
];
exports.asrifle.UPGRADES_TIER_3 = [exports.awp]
exports.sniper.UPGRADES_TIER_3 = [exports.bushwhack, exports.search];
exports.assassin.UPGRADES_TIER_3 = [exports.ranger, exports.falcon, exports.autoass, exports.m24, exports.ggatl];
exports.m24.UPGRADES_TIER_3 = [exports.m48];
exports.hunter.UPGRADES_TIER_3 = [
  exports.preda,
  exports.poach,
  exports.sidewind,
  exports.dual
];
exports.builder.UPGRADES_TIER_3 = [
  exports.construct,
  exports.autobuilder,
  exports.producer,
  exports.boomer
];

exports.machine.UPGRADES_TIER_2 = [
  exports.destroy,
  exports.artillery,
  exports.mini,
  exports.gunner,
  exports.infer,
  exports.rain,
  exports.windstorm,
  exports.gatl
];
exports.gatl.UPGRADES_TIER_3 = [exports.ggatl]
exports.machine.UPGRADES_TIER_3 = [exports.spray];
exports.infer.UPGRADES_TIER_3 = [
  exports.blastf,
  exports.bunsen,
  exports.bonfire,
  exports.blaze,
  exports.rof,
  exports.smoker,
  exports.infernadoshooter,
  exports.infersheild,
  exports.fireballer
];
exports.blastf.UPGRADES_TIER_3 = [exports.heatwav]
exports.bunsen.UPGRADES_TIER_3 = [exports.btorch]
exports.infernadoshooter.UPGRADES_TIER_3 = [exports.trueinfernadoshooter]
exports.blaze.UPGRADES_TIER_3 = [exports.sflare]
exports.fireballer.UPGRADES_TIER_3 = [exports.gfireballer, exports.incann, exports.volc, exports.poisonballer]
exports.infer.UPGRADES_TIER_4 = [exports.firestorm]
exports.smoker.UPGRADES_TIER_3 = [exports.fireb];
exports.destroy.UPGRADES_TIER_3 = [
  exports.anni,
  exports.hybrid,
  exports.construct,
  exports.shotgun2,
  exports.beeyeet,
  exports.bigberth
];
exports.artillery.UPGRADES_TIER_3 = [
  exports.mortar,
  exports.spread,
  exports.skimmer,
  exports.spinner
];
exports.mini.UPGRADES_TIER_3 = [
  exports.stream,
  exports.battery,
  exports.hybridmini,
  exports.downpour,
  exports.archer
];
exports.shotgun2.UPGRADES_TIER_3 = [exports.doomsl, exports.machshot]
exports.machshot.UPGRADES_TIER_3 = [exports.canspray]
exports.bigberth.UPGRADES_TIER_3 = [exports.doomsl]
exports.flank.UPGRADES_TIER_2 = [
  exports.hexa,
  exports.tri,
  exports.auto3,
  exports.flanktrap,
  exports.ace
];
exports.ace.UPGRADES_TIER_3 = [exports.face, exports.nace, exports.bace]
exports.ace.UPGRADES_TIER_4 = [exports.skyshred, exports.flyfort, exports.tsabom]
exports.nace.UPGRADES_TIER_3 = [exports.sace]
exports.face.UPGRADES_TIER_3 = [exports.ods]
exports.bace.UPGRADES_TIER_3 = [exports.gz]
exports.flank.UPGRADES_TIER_3 = [];
exports.tri.UPGRADES_TIER_3 = [
  exports.fighter,
  exports.booster,
  exports.falcon,
  exports.bomber,
  exports.autotri,
  exports.brutalizer,
  exports.search,
  exports.bdrag,
  exports.leak
];
exports.tri.UPGRADES_TIER_4 = [exports.form1];
exports.hexa.UPGRADES_TIER_3 = [exports.octo, exports.hexatrap];
exports.octo.UPGRADES_TIER_3 = [exports.pulse]
exports.auto3.UPGRADES_TIER_3 = [exports.auto5, exports.heavy3, exports.auto4, exports.sniper3];
exports.flanktrap.UPGRADES_TIER_3 = [
  exports.bushwhack,
  exports.guntrap,
  exports.fortress,
  exports.bomber
];

exports.director.UPGRADES_TIER_2 = [
  exports.overseer,
  exports.cruiser,
  exports.underseer,
  exports.lilfact,
  exports.lightning
];
exports.lightning.UPGRADES_TIER_3 = [exports.zeus, exports.lightspeed];
exports.lilfact.UPGRADES_TIER_3 = [
  exports.factory,
  exports.brigade,
  exports.crusade,
  exports.superstorm
];
exports.director.UPGRADES_TIER_3 = [exports.factory];
exports.overseer.UPGRADES_TIER_3 = [
  exports.overlord,
  exports.overtrap,
  exports.overgunner,
  exports.master
];
exports.underseer.UPGRADES_TIER_3 = [exports.necromancer];
exports.cruiser.UPGRADES_TIER_3 = [
  exports.carrier,
  exports.battleship,
  exports.fortress,
  exports.autocruiser
];
exports.pellet.UPGRADES_TIER_2 = [exports.gunner, exports.submach, exports.heli];
exports.heli.UPGRADES_TIER_3 = [exports.downh, exports.aheli, exports.dguns];
exports.heli.UPGRADES_TIER_4 = [exports.specoper, exports.apacheprheli, exports.comcom];
exports.aheli.UPGRADES_TIER_3 = [exports.apacheheli];
exports.downh.UPGRADES_TIER_3 = [exports.chinheli];
exports.dguns.UPGRADES_TIER_3 = [exports.cheli];
exports.submach.UPGRADES_TIER_3 = [exports.vulcan];
exports.vulcan.UPGRADES_TIER_3 = [exports.heavy];
exports.uzi.UPGRADES_TIER_2 = [exports.mini, exports.railgun, exports.rain];
exports.railgun.UPGRADES_TIER_3 = [exports.duobarrel, exports.blastg];
exports.rain.UPGRADES_TIER_3 = [exports.downpour];
exports.downpour.UPGRADES_TIER_3 = [exports.torrent];
exports.downpour.UPGRADES_TIER_4 = [exports.torrentialdownpour];
exports.pound.UPGRADES_TIER_2 = [
  exports.destroy,
  exports.burstgun,
  exports.launch
];
exports.burstgun.UPGRADES_TIER_3 = [exports.shotgun2, exports.blastgun];
exports.launch.UPGRADES_TIER_3 = [
  exports.skimmer,
  exports.arsenal,
  exports.wingm,
  exports.beastm,
  exports.spinner,
  exports.fireworks
];
exports.wingm.UPGRADES_TIER_3 = [exports.apached, exports.carriership];
exports.static.UPGRADES_TIER_2 = [exports.kenetic, exports.thunder, exports.resistor, exports.uv];
exports.uv.UPGRADES_TIER_3 = [exports.infa, exports.solwin]
exports.infa.UPGRADES_TIER_3 = [exports.gamray]
exports.solwin.UPGRADES_TIER_3 = [exports.gamray]
exports.thunder.UPGRADES_TIER_3 = [exports.thunderb]
exports.kenetic.UPGRADES_TIER_3 = [exports.discharge];
exports.kenetic.UPGRADES_TIER_4 = [exports.electro];
exports.Sawshot.UPGRADES_TIER_2 = [exports.buzcan, exports.bruisers, exports.burners, exports.autosaw, exports.sawblaz, exports.discgun];
exports.bruisers.UPGRADES_TIER_3 = [exports.bashers]
exports.buzcan.UPGRADES_TIER_3 = [exports.shredd]
exports.goldfish.UPGRADES_TIER_2 = [
  exports.starfish,
  exports.reefshark,
  exports.squid
];
exports.reefshark.UPGRADES_TIER_3 = [exports.dolphin];
exports.squid.UPGRADES_TIER_3 = [exports.octop];
exports.observer.UPGRADES_TIER_2 = [exports.basic];
exports.lazera.UPGRADES_TIER_2 = [exports.fbeam, exports.caval];
exports.fbeam.UPGRADES_TIER_3 = [exports.bigberth, exports.twinbeam];
exports.caval.UPGRADES_TIER_3 = [exports.atomi, exports.twinbeam];
exports.pg2.UPGRADES_TIER_1 = [exports.observer, exports.lazera, exports.mag, exports.explode, exports.shout, exports.freetbtanks];
exports.pg2.UPGRADES_TIER_2 = [exports.pbasic, exports.fbasic, exports.plbasic]
exports.mag.UPGRADES_TIER_3 = [exports.knock]
exports.knock.UPGRADES_TIER_3 = [exports.ko]
exports.shout.UPGRADES_TIER_3 = [exports.microphone, exports.yell]
exports.microphone.UPGRADES_TIER_3 = [exports.Singer, exports.megaphone]
exports.yell.UPGRADES_TIER_3 = [exports.Singer, exports.scream]
exports.incog.UPGRADES_TIER_3 = [exports.vpn]
/*exports.smash.UPGRADES_TIER_3 = [exports.megasmash, exports.spike, exports.autosmash];
            
    exports.twin.UPGRADES_TIER_2 = [exports.double, exports.bent, exports.triple, exports.hexa];
        exports.double.UPGRADES_TIER_3 = [exports.tripletwin, exports.autodouble];
        exports.bent.UPGRADES_TIER_3 = [exports.penta, exports.benthybrid];
        exports.triple.UPGRADES_TIER_3 = [exports.quint];

    exports.sniper.UPGRADES_TIER_2 = [exports.assassin, exports.overseer, exports.hunter, exports.builder];
        exports.assassin.UPGRADES_TIER_3 = [exports.ranger];
        exports.overseer.UPGRADES_TIER_3 = [exports.overlord, exports.battleship
            , exports.overtrap, exports.necromancer, exports.factory, exports.fortress];
        exports.hunter.UPGRADES_TIER_3 = [exports.preda, exports.poach];
        exports.builder.UPGRADES_TIER_3 = [exports.construct, exports.autobuilder];
        
    exports.machine.UPGRADES_TIER_2 = [exports.destroy, exports.gunner, exports.artillery];
        exports.destroy.UPGRADES_TIER_3 = [exports.anni, exports.hybrid];
        exports.gunner.UPGRADES_TIER_3 = [exports.autogunner, exports.mortar, exports.stream];
        exports.artillery.UPGRADES_TIER_3 = [exports.mortar, exports.spread, exports.skimmer];
        exports.machine.UPGRADES_TIER_3 = [exports.spray];

    exports.flank.UPGRADES_TIER_2 = [exports.hexa, exports.tri, exports.auto3, exports.flanktrap];
        exports.hexa.UPGRADES_TIER_3 = [exports.octo];
        exports.tri.UPGRADES_TIER_3 = [exports.booster, exports.fighter, exports.bomber, exports.autotri];
        exports.auto3.UPGRADES_TIER_3 = [exports.auto5, exports.heavy3];
        exports.flanktrap.UPGRADES_TIER_3 = [exports.guntrap, exports.fortress, exports.bomber];*/

// NPCS:
exports.crasher = {
  TYPE: "crasher",
  LABEL: "Crasher",
  COLOR: 5,
  SHAPE: 3,
  SIZE: 5,  
  VARIES_IN_SIZE: true,
  CONTROLLERS: ["nearestDifferentMaster", "mapTargetToGoal"],
  AI: { NO_LEAD: true },
  BODY: {
    SPEED: 5,
    ACCEL: 0.01,
    HEALTH: 0.5,
    DAMAGE: 5,
    PENETRATION: 2,
    PUSHABILITY: 0.5,
    DENSITY: 10,
    RESIST: 2
  },
  MOTION_TYPE: "motor",
  FACING_TYPE: "smoothWithMotion",
  HITS_OWN_TYPE: "hard",
  HAS_NO_MASTER: true,
  DRAW_HEALTH: true
};
exports.basherw = {
  TYPE: "crasher",
  LABEL: "Basher",
  COLOR: 2,
  SHAPE: [[-1,-1.773],[0.5,-1],[1,-0.5],[1,0.5],[0.5,1],[-0.98,1.7]],
  SIZE: 15,
  VARIES_IN_SIZE: true,
  CONTROLLERS: ["nearestDifferentMaster", "mapTargetToGoal"],
  AI: { NO_LEAD: true },
  BODY: {
    SPEED: 2.5,
    ACCEL: 0.5,
    HEALTH: 5,
    DAMAGE: 50,
    PENETRATION: 5,
    PUSHABILITY: 0.5,
    DENSITY: 20,
    RESIST: 2
  },
  MOTION_TYPE: "motor",
  FACING_TYPE: "smoothWithMotion",
  HITS_OWN_TYPE: "hard",
  HAS_NO_MASTER: true,
  DRAW_HEALTH: true
};
exports.sentry = {
  PARENT: [exports.genericTank],
  TYPE: "crasher",
  LABEL: "Sentry",
  DANGER: 3,
  COLOR: 5,
  SHAPE: 3,
  SIZE: 10,
  SKILL: skillSet({
    rld: 0.5,
    dam: 0.8,
    pen: 0.8,
    str: 0.1,
    spd: 1,
    atk: 0.5,
    hlt: 0,
    shi: 0,
    rgn: 0.7,
    mob: 0
  }),
  VALUE: 1500,
  VARIES_IN_SIZE: true,
  CONTROLLERS: ["nearestDifferentMaster", "mapTargetToGoal"],
  AI: { NO_LEAD: true },
  BODY: {
    FOV: 0.5,
    ACCEL: 0.006,
    DAMAGE: base.DAMAGE * 2,
    SPEED: base.SPEED * 0.5
  },
  MOTION_TYPE: "motor",
  FACING_TYPE: "smoothToTarget",
  HITS_OWN_TYPE: "hard",
  HAS_NO_MASTER: true,
  DRAW_HEALTH: true,
  GIVE_KILL_MESSAGE: true
};
exports.trapTurret = {
  PARENT: [exports.genericTank],
  LABEL: "Turret",
  BODY: {
    FOV: 0.5
  },
  INDEPENDENT: true,
  CONTROLLERS: ["nearestDifferentMaster"],
  COLOR: 16,
  AI: {
    SKYNET: true,
    FULL_VIEW: true
  },
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [16, 14, 1, 0, 0, 0, 0]
    },
    {
      POSITION: [4, 14, 1.8, 16, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([
          g.trap,
          g.lowpower,
          g.fast,
          g.halfreload
        ]),
        TYPE: exports.trap,
        STAT_CALCULATOR: gunCalcNames.trap
      }
    }
  ]
};
exports.sentrySwarm = {
  PARENT: [exports.sentry],
  DANGER: 3,
  GUNS: [
    {
      POSITION: [7, 14, 0.6, 7, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm, g.morerecoil]),
        TYPE: exports.swarm,
        STAT_CALCULATOR: gunCalcNames.swarm
      }
    }
  ]
};
exports.sentryGun = makeAuto(exports.sentry, "Sentry", {
  type: exports.heavy3gun,
  size: 12
});
exports.sentryTrap = makeAuto(exports.sentry, "Sentry", {
  type: exports.trapTurret,
  size: 12
});

exports.miniboss = {
  PARENT: [exports.genericTank],
  TYPE: "miniboss",
  DANGER: 6,
  SKILL: skillSet({
    rld: 0.7,
    dam: 0.5,
    pen: 0.8,
    str: 0.8,
    spd: 0.2,
    atk: 0.3,
    hlt: 1,
    shi: 0.7,
    rgn: 0.7,
    mob: 0
  }),
  LEVEL: 45,
  AI: { NO_LEAD: false },
  FACING_TYPE: "autospin",
  HITS_OWN_TYPE: "hard",
  BROADCAST_MESSAGE: "A visitor has left!"
};
exports.crasherSpawner = {
  PARENT: [exports.genericTank],
  LABEL: "Spawned",
  STAT_NAMES: statnames.drone,
  CONTROLLERS: ["nearestDifferentMaster"],
  COLOR: 5,
  INDEPENDENT: true,
  AI: { chase: true },
  MAX_CHILDREN: 4,
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [6, 12, 1.2, 8, 0, 0, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.drone, g.weak, g.weak]),
        TYPE: [
          exports.drone,
          { LABEL: "Crasher", VARIES_IN_SIZE: true, DRAW_HEALTH: true }
        ],
        SYNCS_SKILLS: true,
        AUTOFIRE: true,
        STAT_CALCULATOR: gunCalcNames.drone
      }
    }
  ]
};
exports.elite = {
  PARENT: [exports.miniboss],
  LABEL: "Elite Crasher",
  COLOR: 5,
  SHAPE: 3,
  SIZE: 20,
  VARIES_IN_SIZE: true,
  VALUE: 150000,
  BODY: {
    FOV: 1.3,
    SPEED: base.SPEED * 0.25,
    HEALTH: base.HEALTH * 1.5,
    SHIELD: base.SHIELD * 1.25,
    REGEN: base.REGEN,
    DAMAGE: base.DAMAGE * 2.5
  }
};
exports.elite_destroyer = {
  PARENT: [exports.elite],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [5, 16, 1, 6, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound, g.pound, g.destroy]),
        TYPE: exports.bullet,
        LABEL: "Devastator"
      }
    },
    {
      POSITION: [5, 16, 1, 6, 0, 60, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound, g.pound, g.destroy]),
        TYPE: exports.bullet,
        LABEL: "Devastator"
      }
    },
    {
      POSITION: [5, 16, 1, 6, 0, -60, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.basic, g.pound, g.pound, g.destroy]),
        TYPE: exports.bullet,
        LABEL: "Devastator"
      }
    }
  ],
  TURRETS: [
    {
      /*********  SIZE     X       Y     ANGLE    ARC */
      POSITION: [11, 0, 0, 180, 360, 0],
      TYPE: [exports.crasherSpawner]
    },
    {
      POSITION: [11, 0, 0, 60, 360, 0],
      TYPE: [exports.crasherSpawner]
    },
    {
      POSITION: [11, 0, 0, -60, 360, 0],
      TYPE: [exports.crasherSpawner]
    },
    {
      POSITION: [11, 0, 0, 0, 360, 1],
      TYPE: [exports.bigauto4gun, { INDEPENDENT: true, COLOR: 5 }]
    }
  ]
};
exports.elite_gunner = {
  PARENT: [exports.elite],
  GUNS: [
    {
      /*** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
      POSITION: [14, 16, 1, 0, 0, 180, 0]
    },
    {
      POSITION: [4, 16, 1.5, 14, 0, 180, 0],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.trap, g.hexatrap]),
        TYPE: [exports.pillbox, { INDEPENDENT: true }]
      }
    },
    {
      POSITION: [6, 14, -2, 2, 0, 60, 0]
    },
    {
      POSITION: [6, 14, -2, 2, 0, 300, 0]
    }
  ],
  AI: { NO_LEAD: false },
  TURRETS: [
    {
      /*********  SIZE     X       Y     ANGLE    ARC */
      POSITION: [14, 8, 0, 60, 180, 0],
      TYPE: [exports.auto4gun]
    },
    {
      POSITION: [14, 8, 0, 300, 180, 0],
      TYPE: [exports.auto4gun]
    }
  ]
};
exports.elite_sprayer = {
  PARENT: [exports.elite],
  AI: { NO_LEAD: false },
  TURRETS: [
    {
      /*  SIZE     X       Y     ANGLE    ARC */
      POSITION: [14, 6, 0, 180, 190, 0],
      TYPE: [exports.spray, { COLOR: 5 }]
    },
    {
      POSITION: [14, 6, 0, 60, 190, 0],
      TYPE: [exports.spray, { COLOR: 5 }]
    },
    {
      POSITION: [14, 6, 0, -60, 190, 0],
      TYPE: [exports.spray, { COLOR: 5 }]
    }
  ]
};

exports.palisade = (() => {
  let props = {
    SHOOT_SETTINGS: combineStats([
      g.factory,
      g.pound,
      g.halfreload,
      g.halfreload
    ]),
    TYPE: exports.minion,
    STAT_CALCULATOR: gunCalcNames.drone,
    AUTOFIRE: true,
    MAX_CHILDREN: 1,
    SYNCS_SKILLS: true,
    WAIT_TO_CYCLE: true
  };
  return {
    PARENT: [exports.miniboss],
    LABEL: "Rogue Palisade",
    COLOR: 17,
    SHAPE: 6,
    SIZE: 28,
    VALUE: 500000,
    BODY: {
      FOV: 1.3,
      SPEED: base.SPEED * 0.1,
      HEALTH: base.HEALTH * 2,
      SHIELD: base.SHIELD * 2,
      REGEN: base.REGEN,
      DAMAGE: base.DAMAGE * 3
    },
    GUNS: [
      {
        /**** LENGTH  WIDTH   ASPECT    X       Y     ANGLE   DELAY */
        POSITION: [4, 6, -1.6, 8, 0, 0, 0],
        PROPERTIES: props
      },
      {
        POSITION: [4, 6, -1.6, 8, 0, 60, 0],
        PROPERTIES: props
      },
      {
        POSITION: [4, 6, -1.6, 8, 0, 120, 0],
        PROPERTIES: props
      },
      {
        POSITION: [4, 6, -1.6, 8, 0, 180, 0],
        PROPERTIES: {
          SHOOT_SETTINGS: combineStats([g.factory, g.pound]),
          TYPE: exports.minion,
          STAT_CALCULATOR: gunCalcNames.drone,
          AUTOFIRE: true,
          MAX_CHILDREN: 1,
          SYNCS_SKILLS: true,
          WAIT_TO_CYCLE: true
        }
      },
      {
        POSITION: [4, 6, -1.6, 8, 0, 240, 0],
        PROPERTIES: props
      },
      {
        POSITION: [4, 6, -1.6, 8, 0, 300, 0],
        PROPERTIES: props
      }
    ],
    TURRETS: [
      {
        /*  SIZE     X       Y     ANGLE    ARC */
        POSITION: [5, 10, 0, 30, 110, 0],
        TYPE: exports.trapTurret
      },
      {
        POSITION: [5, 10, 0, 90, 110, 0],
        TYPE: exports.trapTurret
      },
      {
        POSITION: [5, 10, 0, 150, 110, 0],
        TYPE: exports.trapTurret
      },
      {
        POSITION: [5, 10, 0, 210, 110, 0],
        TYPE: exports.trapTurret
      },
      {
        POSITION: [5, 10, 0, 270, 110, 0],
        TYPE: exports.trapTurret
      },
      {
        POSITION: [5, 10, 0, 330, 110, 0],
        TYPE: exports.trapTurret
      }
    ]
  };
})();

exports.bot = {
  AUTO_UPGRADE: "random",
  FACING_TYPE: "looseToTarget",
  LEVEL: 70,
  DANGER: 2000,
  BODY: {
    SIZE: 18,
    SPEED: base.SPEED * 0.4,
    FOV: base.FOV * 1.1,
  },
 SKILL: skillSet({
    rld: 0.7, // reload
    dam: 0.8, // bullet damage
    pen: 0.8, // bullet penetration
    str: 0.8, // bullet health
    spd: 0.2, // bullet speed
    atk: 0.0, // body damage
    hlt: 0.0, // max health
    shi: 0.0, // shield
    rgn: 0.0, // shield regen
    mob: 0.0 // movement speed
  }),
  COLOR: 17,
  CONTROLLERS: [
    "nearestDifferentMaster",
    "avoid",
    "alwaysFire",
    "mapAltToFire",
    "minion",
    "fleeAtLowHealth"
  ],
  AI: { STRAFE: true }
};

exports.elite_swarmer = {
  PARENT: [exports.elite],
  LABEL: 'Supreme Swarmer',
  MAX_CHILDREN: 20,
  SIZE: 18,
  COLOR: 5,
  SHAPE: 8,
  GUNS: [ {
      POSITION: [ 5, 12, 0.4, 7, 0, 0, 0, ],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm,  g.morereload, g.small]),
        TYPE: exports.swarm, STAT_CALCULATOR: gunCalcNames.swarm,
      }, }, {
      POSITION: [ 5, 12, 0.4, 7, 0, 90, 0, ],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm, g.morereload, g.small]),
        TYPE: exports.swarm, STAT_CALCULATOR: gunCalcNames.swarm,
      }, }, {
      POSITION: [ 5, 12, 0.4, 7, 0, 180, 0, ],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm, g.morereload, g.small]),
        TYPE: exports.swarm, STAT_CALCULATOR: gunCalcNames.swarm,
      }, }, {
      POSITION: [ 5, 12, 0.4, 7, 0, 270, 0, ],
      PROPERTIES: {
        SHOOT_SETTINGS: combineStats([g.swarm,  g.morereload, g.small]),
        TYPE: exports.swarm, STAT_CALCULATOR: gunCalcNames.swarm,
      },
    },
  ],
  TURRETS: [{
    POSITION: [7, 0, 0, 0, 360, 1, ],
    TYPE: [exports.auto5gun],
  },
           {
             POSITION: [3, 6, 0, 120, 360, 1],
  TYPE: [exports.auto5gun]
           },
            {
              POSITION: [3, 6, 0, 0, 360, 1,],
              TYPE: [exports.auto5gun]
            },
            {
              POSITION: [3, 6, 0, -120,  360, 1],
              TYPE: [exports.auto5gun]
            }
              
            
           ],
};
exports.testbed.UPGRADES_TIER_1.push(exports.elite_sprayer);
exports.testbed.UPGRADES_TIER_1.push(exports.elite_swarmer);
